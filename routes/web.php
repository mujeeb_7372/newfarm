<?php
use Illuminate\Support\Facades\Auth;
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('send_request_registration', function ()
{

});
Route::middleware(['admin'])->group(function()
{
 
	 Route::get('/', function () 
	{
		
		if(Auth::check())
		{
			
		    if(Auth::check())
			{
			  if(Auth::user()->user_type=='Admin' || Auth::user()->user_type=='admin_user')
			  {
			    return redirect('/home');	
			  }
			  else if(Auth::user()->user_type=='Partner' || Auth::user()->user_type=='partner_user')
			  {
			  	return redirect('/parnter_home');
			  }
			  
			}
			else
			{
				return redirect('/login');
			}
	    }
	    else
		{
			return redirect('/login');
		}
	});
 	/*Users*/
 	Route::get('users','HomeController@users_page');
 	Route::post('getUser','users@get_user_list');
 	Route::get('user_add_page','users@add_users_page');
	Route::post('ajax_save_user','users@add_users');
 	Route::get('edit_user_page/{id}','users@add_users_page');
 	Route::get('useraction/{status}/{id}','users@user_action');

 	
 	/*End Users*/


 	/*Requirement*/
 	Route::get('requirement/{page}/{type}/{id}','requirement@requirement_page');
 	Route::post('ajax_add_requirement','requirement@add_requirement');
 	Route::post('ajax_list_requirement','requirement@get_requirement_list');
 	Route::post('ajax_shortlist_requirement','requirement@get_short_list');
 	Route::get('view_requirement_page/{id}','requirement@requirement_detail_page');
 	Route::get('requirement_change_status/{action}/{id}','requirement@update_requirement_status');
 	Route::get('requirment_edit_page/{id}','requirement@requirement_detail_page');
 	Route::post('movetocheckout','requirement@update_shortlist');
 	Route::get('checkout_details/{id}','requirement@checkout_details');
 	
 	/*End Requirement*/

 	/*Capacity*/
 	Route::get('capacity/{page}/{type}/{id}','capacity@capacity_page');
 	Route::post('ajax_add_capacity','capacity@add');
 	Route::post('ajax_list_capacity','capacity@get_capacity_list');
 	Route::get('capacity_change_status/{action}/{id}','capacity@update_capacity_status');
 	Route::post('ajax_shortlist_capacity','capacity@get_short_list');
 	Route::post('moveto_capacity_checkout','capacity@update_shortlist');
 	Route::get('checkout_capacity_details/{id}','capacity@checkout_details');
 	Route::post('moveto_capacity_checkout','capacity@update_shortlist');

 	
 	/*End Capacity*/

 	/*Partner*/
 	Route::get('partner','partner@partner_list_page');
 	Route::get('add_partner','partner@add_partner_page');
 	Route::post('ajax_save_company','partner@add_company');
 	Route::post('getPartner','partner@get_partner_list');
 	Route::get('saved_partner','partner@partner_saved_page');
 	Route::get('edit_partner_page/{id}','partner@add_partner_page');
 	Route::post('ajax_update_company','partner@update_partner_page');
 	Route::get('partner_view_page/{id}','partner@view_details_page');
 	Route::get('partner_graph_page/{id}','partner@view_grap_report');
 	Route::get('part_action/{status}/{id}','partner@part_action');
 	Route::get('partner_reset_page/{id}/{email}','partner@reset_partner_pwd');
 	Route::post('update_partner_pwd','partner@update_partner_pwd');
 	
 	/*End Partner*/

 	/*Export Section*/
 	Route::get('export_user','admin_export@downloadUser_Data');
 	Route::get('export_partner/{status}','admin_export@download_Partner');
 	Route::get('export_requirement/{status}','admin_export@downloadRequirement_data');
 	Route::get('export_capacity/{status}','admin_export@downloadCapacity_data');
 	/*End Export Section*/
 	Route::get('email_page','HomeController@email_page');
 	Route::post('getEmail','HomeController@getEmail');
 	Route::post('save_email','HomeController@addEmail');
 	Route::post('update_read_email','HomeController@updateRead');
 	
 	Route::get('alter_query','HomeController@alter_query');

	Auth::routes();

	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/help', 'HomeController@help');
	Route::get('/doc_help', 'HomeController@doc_help');
	Route::get('/video_help', 'HomeController@video_help');

}); 

Auth::routes();

