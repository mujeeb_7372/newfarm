<form id="user_form_search">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-xs-1">
        </div>
       <div class="col-xs-3">
            <h2 class="card-inside-title">Requirment ID</h2>
            <div class="input-group date" >
              
                <div class="form-line">
                    <input type="text" class="form-control" name="req_no" id="req_no">
                </div>
            </div>
        </div>

        <div class="col-xs-3">
            <h2 class="card-inside-title">Titile</h2>
            <div class="input-group date" >
                <div class="form-line">
                    <input type="text" class="form-control" name="req_title" id="req_title">
                </div>
            </div>
        </div>
        <div class="col-xs-2">
            <h2 class="card-inside-title">Skills</h2>
            <div class="input-group date" >
                <div class="form-line">
                    <input type="text" id="req_skill" name="req_skill" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <h2 class="card-inside-title">Location</h2>
            <div class="input-group date" >
                <div class="form-line">
                     <input type="text" class="form-control" name="req_location" id="req_location">
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 text-center">
        <div class="row clearfix">
            <div class="col-xs-12">
              <button type="submit" class="btn bg-purple waves-effect">
                    <i class="material-icons">search</i>
                    <span>SEARCH</span>
               </button>
               <a class="btn bg-indigo waves-effect" id="req_export_btn">
                  <i class="material-icons">import_export</i>
                  <span>Export</span>
               </a>
               <a class="btn btn-success waves-effect" href="{{url('requirement/page/add/0')}}">
                    <i class="material-icons">person_add</i>
                    <span>Add</span>
                </a>
            </div>  
       </div>
     </div>
    </div>
  </form>