<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <title>Farm Digitohub</title>
      <!-- Favicon-->
      <link rel="icon" href="favicon.ico" type="image/x-icon">
      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
      <!-- Bootstrap Core Css -->
      <link href="{{URL::asset('public/src/')}}/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
      <!-- Waves Effect Css -->
      <link href="{{URL::asset('public/src/')}}/plugins/node-waves/waves.css" rel="stylesheet" />
      <!-- Animati{{URL::asset('public/src/')}}/on Css -->
      <link href="{{URL::asset('public/src/')}}/plugins/animate-css/animate.css" rel="stylesheet" />
      <!-- Morris Chart Css-->
      <link href="{{URL::asset('public/src/')}}/plugins/morrisjs/morris.css" rel="stylesheet" />
      <!-- Custom Css -->
      <link href="{{URL::asset('public/src/')}}/css/style.css" rel="stylesheet">
      <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
      <link href="{{URL::asset('public/src/')}}/css/themes/all-themes.css" rel="stylesheet" />

      <link href="{{URL::asset('public/src/')}}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

      <link href="{{URL::asset('public/src/')}}/plugins/dropzone/dropzone.css" rel="stylesheet">

      <link href="{{URL::asset('public/src/')}}/plugins/multi-select/css/multi-select.css" rel="stylesheet">

      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

      <link href="{{URL::asset('public/src/')}}/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

      <link href="{{URL::asset('public/src/')}}/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
      
      <link href="{{URL::asset('public/src/')}}/css/common.css" rel="stylesheet" />     

      <link href="{{URL::asset('public/src/')}}/plugins/morrisjs/morris.css" rel="stylesheet" />

      <link href="{{URL::asset('public/src/')}}/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

      <link href="{{URL::asset('public/css/hlep_doc.css')}}" rel="stylesheet">
      <!-- <link rel="stylesheet" type="text/css" href="https://unpkg.com/jquery-easy-loading@2.0.0-rc.2/dist/jquery.loading.min.js"> -->

      @include('flash::message')
<style type="text/css">
.sidebars
{
      width: 193px;
}
</style>
   </head>
