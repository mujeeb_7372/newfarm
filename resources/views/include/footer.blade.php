<div id="myModal" class="modal ">
    <div class="modal-dialog custome-model">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Modal Header</h4>
          
          <button type="button" class="close btn btn-danger" style="width:40px; height:25px;" data-dismiss="modal">Close</button>

          <a href="" class="pull-right" id="download_link">Download</a>&nbsp;&nbsp;
        </div>
          <div id="caption"></div>
          <embed class="modal-content" id="img01" >
        
        <div class="modal-footer">
          
        </div>
      </div>
  
</div>
</div>


<!-- Jquery Core Js -->
  <script src="{{URL::asset('public/src/')}}/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap Core Js -->
  <script src="{{URL::asset('public/src/')}}/plugins/bootstrap/js/bootstrap.js"></script>
  <!-- Select Plugin Js -->
  <script src="{{URL::asset('public/src/')}}/plugins/bootstrap-select/js/bootstrap-select.js"></script>
  <!-- Slimscroll Plugin Js -->
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
  <!-- Waves Effect Plugin Js -->
  <script src="{{URL::asset('public/src/')}}/plugins/node-waves/waves.js"></script>
  <!-- Jquery CountTo Plugin Js -->
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-countto/jquery.countTo.js"></script>
  <!-- Morris Plugin Js -->
  <script src="{{URL::asset('public/src/')}}/plugins/raphael/raphael.min.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/morrisjs/morris.js"></script>
  <!-- ChartJs -->
  <script src="{{URL::asset('public/src/')}}/plugins/chartjs/Chart.bundle.js"></script>
  <!-- Flot Charts Plugin Js -->
  <script src="{{URL::asset('public/src/')}}/plugins/flot-charts/jquery.flot.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/flot-charts/jquery.flot.resize.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/flot-charts/jquery.flot.pie.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/flot-charts/jquery.flot.categories.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/flot-charts/jquery.flot.time.js"></script>
  <!-- Sparkline Chart Plugin Js -->
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-sparkline/jquery.sparkline.js"></script>
  <!-- Custom Js -->

  <script src="{{URL::asset('public/src/')}}/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

  <script src="{{URL::asset('public/src/')}}/js/admin.js"></script>

  <!-- <script src="{{URL::asset('public/src/')}}/js/pages/index.js"></script> -->
  
  

  <script src="{{URL::asset('public/src/')}}/plugins/multi-select/js/jquery.multi-select.js"></script>

  <script src="{{URL::asset('public/src/')}}/js/demo.js"></script>

  <script src="{{URL::asset('public/src/')}}/plugins/jquery-datatable/jquery.dataTables.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
  <script src="{{URL::asset('public/src/')}}/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

<script src="{{URL::asset('public/src/')}}/plugins/momentjs/moment.js"></script>

  <script src="{{URL::asset('public/src/')}}/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

  
  <script src="{{URL::asset('public/src/')}}/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

   <script src="{{URL::asset('public/src/')}}/js/pages/forms/basic-form-elements.js"></script>

   <script src="{{URL::asset('public/src/')}}/plugins/sweetalert/sweetalert.min.js"></script>

   
    <script src="{{URL::asset('public/src/')}}/plugins/chartjs/Chart.bundle.js"></script>
   
   <script src="{{URL::asset('public/src/')}}/plugins/ckeditor/ckeditor.js"></script>
   
   <script src="{{URL::asset('public/src/')}}/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

   <script src="{{URL::asset('public/src/')}}/plugins/tinymce/tinymce.js"></script>

   <script src="{{URL::asset('public/src/')}}/plugins/bootstrap-notify/bootstrap-notify.js"></script>

   <script src="{{URL::asset('public/src/')}}/js/jquery.loading.min.js"></script>
   
   </body>
    @if ($message = Session::get('success'))
  <script type="text/javascript">
     $.notify({
          title: 'Message',
          message: '{{ $message }}'
        },{
          type: 'success'
        });
 </script>
  @endif
  @if ($message = Session::get('error'))
  <script type="text/javascript">
    $.notify({
          title: 'Message',
          message: '{{ $message }}'
        },{
          type: 'warning'
        });
  </script>
  @endif
<script>
  $(document).ready(function()
  {
    $('.profile_btn').on('click', function () {
        var color = $(this).data('color');
        $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
        $('#mdModal').modal('show');
        $('.modal-backdrop').hide()
    });
  })
  $('.user-info').css('background-image',"url({{URL::asset('public/src/images/user-img-background.jpg')}})") 
  var localhost="http://localhost/NewFarm/";
  
  var modal = document.getElementById("myModal");
  var modalImg = document.getElementById("img01");
  var captionText = document.getElementById("caption");
  var downloadlin=document.getElementById('download_link');
  $('.upld_img').on('click',function()
  {
    modal.style.display = "block";
    modalImg.src = $(this).attr('srcc');
    downloadlin.href=localhost+$(this).attr('srcc');
    captionText.innerHTML = this.alt;
  });
  var span = document.getElementsByClassName("close")[0];
  span.onclick = function() 
  { 
    modal.style.display = "none";
  }

function closeNav() 
{
 $('.mobile-phone-number').inputmask('9999999999', { placeholder: '__________' });
  document.getElementById("mySidebar").style.width = "0";
  $(".custome-content").css("margin-left", "10px");
}
$('#openbtn').click(function()
  {
  if($("#mySidebar").width()==0)
  {
    document.getElementById("mySidebar").style.width = "200px";
    $(".custome-content").css("margin-left", "188px");
  }
  else
  {
    document.getElementById("mySidebar").style.width = "0";
    $(".custome-content").css("margin-left", "10px");
  }
  })
</script>
</html>