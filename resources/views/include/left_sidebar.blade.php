<aside id="mySidebar" class="sidebars">
   <!-- User Info -->
<!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a> -->
   <div class="user-info">
      <div class="image">
         <img src="{{URL::asset('public/src/')}}/images/user.png" width="48" height="48" alt="User" />
      </div>
      <div class="info-container">
         <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{login_details('uiname')}}</div>
         <div class="email">{{login_details('uiemail')}}</div>
         <div class="btn-group user-helper-dropdown">
            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
            <ul class="dropdown-menu pull-right">
               <li>
                  <a href="javascript:void(0);" data-color="deep-purple" class="profile_btn"><i class="material-icons">person</i> Profile</a>
               </li>
               <li role="separator" class="divider"></li>
               <li role="separator" class="divider"></li>
               <li><a href="{{url('logout')}}"><i class="material-icons">input</i>Sign Out</a></li>
            </ul>
         </div>
      </div>
   </div>
   <!-- #User Info -->
   <!-- Menu -->
  @if($usertype=='Admin') 
   <div class="menu">
      <ul class="list">
         <li class="active">
            <a href="{{url('home')}}">
            <i class="material-icons">home</i>
            <span>Home</span>
            </a>
         </li>
         <li class="active">
            <a href="{{url('users')}}">
            <i class="material-icons">face</i>
            <span>Users</span>
            </a>
         </li>
         <li>
            <a href="javascript:void(0);" class="menu-toggle">
            <i class="material-icons">group</i>
            <span>Partner</span>
            </a>
            <ul class="ml-menu">
               <li>
                  <a href="{{url('add_partner')}}">
                     <i class="material-icons">add</i>
                     <span>Add Partner</span>
                  </a>
               </li>
               <li>
                  <a href="{{url('partner')}}">
                     <i class="material-icons">list</i>
                     <span>Partner List</span>
                  </a>
               </li>
                <li>
                  <a href="{{url('saved_partner')}}">
                     <i class="material-icons">list</i>
                     <span>Saved Partner</span>
                  </a>
               </li>
            </ul>
         </li>
         <li>
            <a href="javascript:void(0);" class="menu-toggle">
            <i class="material-icons">assignment</i>
            <span>Requirement</span>
            </a>
            <ul class="ml-menu">
              <li>
                  <a href="javascript:void(0);" class="menu-toggle">
                  <span>My Requirement</span>
                  </a>
                  <ul class="ml-menu">
                     <li>
                        <a href="{{url('requirement/page/add/0')}}">
                           <i class="material-icons">add</i><span>Add Requirement</span></a>
                     </li>
                     <li>
                        <a href="{{url('requirement/page/saved_list/0')}}">
                           <i class="material-icons">list</i><span>Saved Requirement</span></a>
                     </li>
                     <li>
                        <a href="{{url('requirement/page/submit_list/0')}}">
                           <i class="material-icons">list</i><span>Submitted</span></a>
                     </li>
                     <li>
                        <a href="{{url('requirement/page/shortlist/0')}}">
                           <i class="material-icons">list</i><span>My Shortlisted</span></a>
                     </li>
                     <li>
                        <a href="{{url('requirement/page/deactive_list/0')}}">
                           <i class="material-icons">cancel</i><span>Deactive List</span></a>
                     </li>
                     <li>
                        <a href="{{url('requirement/page/checkout_list/0')}}">
                           <i class="material-icons">check</i><span>My Checkout</span></a>
                     </li>
                     
                  </ul>
               </li>
               <li>
                  <a href="javascript:void(0);" class="menu-toggle">
                  <span>Partner Requirement</span>
                  </a>
                  <ul class="ml-menu">
                     <li>
                        <a href="{{url('requirement/page/part_submit/0')}}">
                           <i class="material-icons"></i><span>To Publish</span></a>
                     </li>
                     <li>
                        <a href="{{url('requirement/page/part_checkout/0')}}">
                           <i class="material-icons">check</i><span>Checkout</span></a>
                     </li>
                  </ul>
               </li>
               <li>
                  <a href="{{url('requirement/page/publish/0')}}">
                     <i class="material-icons">assignment_turned_in</i>
                     <span>Publish</span>
                  </a>
               </li>
            </ul>
         </li>
         <li>
            <a href="javascript:void(0);" class="menu-toggle">
            <i class="material-icons">description</i>
            <span>Capacity</span>
            </a>
            <ul class="ml-menu">
              <li>
                  <a href="javascript:void(0);" class="menu-toggle">
                  <span>My Capacity</span>
                  </a>
                  <ul class="ml-menu">
                     <li>
                        <a href="{{url('capacity/page/add/0')}}">
                           <i class="material-icons">add</i><span>Add</span></a>
                     </li>
                     <li>
                        <a href="{{url('capacity/page/saved/0')}}">
                           <i class="material-icons">list</i><span>Saved</span></a>
                     </li>
                     <li>
                        <a href="{{url('capacity/page/submit/0')}}">
                           <i class="material-icons">list</i><span>Submitted</span></a>
                     </li>
                     <li>
                        <a href="{{url('capacity/page/shortlist/0')}}">
                           <i class="material-icons">list</i><span>Shortlisted</span></a>
                     </li>
                     <li>
                        <a href="{{url('capacity/page/deactive/0')}}">
                           <i class="material-icons">cancel</i><span>Deactive List</span></a>
                     </li>
                     <li>
                        <a href="{{url('capacity/page/checkout/0')}}">
                           <i class="material-icons">check</i><span>Checkout List</span></a>
                     </li>
                     
                  </ul>
               </li>
               <li>
                  <a href="javascript:void(0);" class="menu-toggle">
                  <span>Partner Capacity</span>
                  </a>
                  <ul class="ml-menu">
                     <li>
                        <a href="{{url('capacity/page/partner_capacity_publish/0')}}">
                           <i class="material-icons"></i><span>To Publish</span></a>
                     </li>
                     <li>
                        <a href="{{url('capacity/page/partner_capacity_checkout/0')}}">
                           <i class="material-icons">check</i><span>Checkout</span></a>
                     </li>
                  </ul>
               </li>
               <li>
                  <a href="{{url('capacity/page/publish/0')}}">
                     <i class="material-icons">assignment_turned_in</i>
                     <span>Published</span>
                  </a>
               </li>
            </ul>
         </li>
         <!-- <li class="active">
            <a href="index.html">
            <i class="material-icons">gavel</i>
            <span>Permission</span>
            </a>
         </li> -->
         <li class="active">
            <a href="{{url('email_page')}}">
            <i class="material-icons">email</i>
            <span>Email</span>
            </a>
         </li>
         <li class="active">
            <a href="{{url('help')}}">
            <i class="material-icons">help</i>
            <span>Help</span>
            </a>
         </li>
         <li class="active">
            <a href="{{url('logout')}}">
            <i class="material-icons">power_settings_new</i>
            <span>Logout</span>
            </a>
         </li>
      </ul>
   </div>
   @endif

   @if($usertype=='Partner' || $usertype=='partner_user')
     <div class="menu">
         <ul class="list">
            <li class="active">
               <a href="{{url('home')}}">
               <i class="material-icons">home</i>
               <span>Home</span>
               </a>
            </li>
            @if($usertype=='Partner')
            <li class="active">
               <a href="{{url('users')}}">
               <i class="material-icons">face</i>
               <span>Users</span>
               </a>
            </li>
            @endif
            <li>
               <a href="javascript:void(0);" class="menu-toggle">
               <i class="material-icons">assignment</i>
               <span>Requirement</span>
               </a>
               <ul class="ml-menu">
                 <li>
                     <a href="javascript:void(0);" class="menu-toggle">
                     <span>My Requirement</span>
                     </a>
                     <ul class="ml-menu">
                        <li>
                           <a href="{{url('requirement/page/add/0')}}">
                              Add</span></a>
                        </li>
                        <li>
                           <a href="{{url('requirement/page/saved_list/0')}}">
                              Saved</span></a>
                        </li>
                        <li>
                           <a href="{{url('requirement/page/submit_list/0')}}">
                              Submitted</span></a>
                        </li>
                        <li>
                           <a href="{{url('requirement/page/shortlist/0')}}">
                              My Shortlisted</span></a>
                        </li>
                        <li>
                           <a href="{{url('requirement/page/checkout_list/0')}}">
                              My Checkout</span></a>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <a href="{{url('requirement/page/publish/0')}}">
                        <i class="material-icons">assignment_turned_in</i>
                        <span>Publish</span>
                     </a>
                  </li>
               </ul>
            </li>
            <li>
               <a href="javascript:void(0);" class="menu-toggle">
               <i class="material-icons">description</i>
               <span>Capacity</span>
               </a>
               <ul class="ml-menu">
                 <li>
                     <a href="javascript:void(0);" class="menu-toggle">
                     <span>My Capacity</span>
                     </a>
                     <ul class="ml-menu">
                        <li>
                           <a href="{{url('capacity/page/add/0')}}">
                              <span>Add</span></a>
                        </li>
                        <li>
                           <a href="{{url('capacity/page/saved/0')}}">
                              <span>Saved</span></a>
                        </li>
                        <li>
                           <a href="{{url('capacity/page/submit/0')}}">
                              <span>Submitted</span></a>
                        </li>
                        <li>
                           <a href="{{url('capacity/page/shortlist/0')}}">
                              <span>Shortlisted</span></a>
                        </li>
                        <li>
                           <a href="{{url('capacity/page/checkout/0')}}">
                              <span>Checkout List</span></a>
                        </li>
                        
                     </ul>
                  </li>
                  <li>
                     <a href="{{url('capacity/page/publish/0')}}">
                        <i class="material-icons">assignment_turned_in</i>
                        <span>Published</span>
                     </a>
                  </li>
               </ul>
            </li>
            <li class="active">
               <a href="{{url('email_page')}}">
               <i class="material-icons">email</i>
               <span>Email</span>
               </a>
            </li>
            <li class="active">
               <a href="index.html">
               <i class="material-icons">help</i>
               <span>Help</span>
               </a>
            </li>
            <li class="active">
               <a href="{{url('logout')}}">
               <i class="material-icons">power_settings_new</i>
               <span>Logout</span>
               </a>
            </li>
         </ul>
      </div>
   @endif
   <!-- #Menu -->
   <!-- Footer -->
   
   <!-- #Footer -->
</aside>