@include('include.header')
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Requirement Details
                            </h2>
                            <ul class="header-dropdown m-r--5">
                              
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                      

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                   
                                    <tbody>
                                      <tr>
                                        <th>ID</th> 
                                        <td>{{$data->requirement_id}}</td>
                                      </tr>
                                      <tr>
                                        <th>Title</th>
                                        <td>{{$data->title}}</td>
                                      </tr>
                                      <tr>
                                        <th>Description</th>
                                        <td>{!!base64_decode($data->description)!!}</td>
                                      </tr>
                                      <tr>
                                        <th>Type</th>
                                        <td>{{$data->requirement_type}}</td>
                                      </tr>
                                      <tr>
                                        <th>Primary  Skills</th> 
                                        <td>{{$data->primary_skills}}</td>
                                      </tr>
                                      <tr>
                                        <th>Secondary Skills</th> 
                                        <td>{{$data->secondary_skills}}</td>
                                      </tr>
                                      <tr>
                                        <th>Minimum Exp</th> 
                                        <td>{{$data->min_experience}}</td>
                                      </tr>
                                      <tr>
                                        <th>Maximum Exp</th> 
                                        <td>{{$data->max_experience}}</td>
                                      </tr>
                                      <!-- <tr>
                                        <th>Requirement Number</th> 
                                        <td></td>
                                      </tr> -->
                                      <tr>
                                        <th>Work Location</th> 
                                        <td>{{$data->work_location}}</td>
                                      </tr>
                                      <tr>
                                        <th>Place</th> 
                                        <td>{{$data->place}}</td>
                                      </tr>
                                      <tr>
                                        <th>Project Type</th> 
                                        <tdx>{{$data->project_type}}</td>
                                      </tr>
                                      <tr>
                                        <th>Industry</th> 
                                        <td>{{$data->industry}}</td>
                                      </tr>
                                      <tr>
                                        <th>Duration (months)</th> 
                                        <td>{{$data->duration}}</td>
                                      </tr>
                                      <tr>
                                        <th>Start Date</th> 
                                        <td>{{$data->start_date}}</td>
                                      </tr>
                                      @if($usertype=='Admin' || $usertype=='admin_user')
                                        <tr>
                                        <th><b>Added By User</b></th> 
                                        <td>{{$data->name}}</td>
                                      </tr>
                                      <tr>
                                        <th><b>Added By Company<b></th> 
                                        <td>{{$data->company_name}}</td>
                                      </tr>
                                      <tr>
                                        <th><b>Qty<b></th> 
                                        <td>{{$data->qty}}</td>
                                      </tr>
                                      @endif
                                    </tbody>
                                </table>
                           </div>
                           <div class="row">
                             <div class="text-center">
                              @if(isset($data))
                                 @if($data->admin_status=='publish')
                                    @if($company!=$data->company_id)
                                      <a href="{{url('requirement_change_status/shortlisted').'/'.$data->id}}" class="btn btn-primary">Add to Shortlist</a>
                                    @endif

                                    
                                  @endif
                                  @if($usertype=='Admin' || $usertype=='admin_user')
                                     @if($data->admin_status=='inactive')
                                          <a href="{{url('requirement_change_status/publish').'/'.$data->id}}" class="btn btn-info" style="width:140px">Active</a>
                                         @else
                                          <a href="{{url('requirement_change_status/inactive').'/'.$data->id}}" class="btn btn-danger" style="width:140px">Deactive</a>
                                     @endif
                                    @endif
                              @endif 
                               
                               <a href="{{url()->previous()}}" class="btn btn-danger">Close</a>
                             </div>
                          </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            
            <!-- #END# Exportable Table -->
        </div>
    </section>
@include('include.footer')

<script type="text/javascript">
$(document).ready(function()
{
  
}) 

</script>