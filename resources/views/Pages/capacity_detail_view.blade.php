@include('include.header')

<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="table-responsive">
                                <div class="col-lg-14 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                                Capacity Details
                                            </h2>
                                            
                                        </div>
                                        <div class="body">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active">
                                                    <a href="#General" data-toggle="tab">
                                                         General
                                                    </a>
                                                </li>
                                               @if(!isset($isPublish))
                                                 <li role="presentation">
                                                      <a href="#Personal" data-toggle="tab">
                                                           Personal
                                                      </a>
                                                  </li>
                                                @endif
                                                <li role="presentation">
                                                    <a href="#Edcucation" data-toggle="tab">
                                                         Edcucation
                                                    </a>
                                                </li>
                                                <li role="presentation">
                                                    <a href="#Skills" data-toggle="tab">
                                                         Skills
                                                    </a>
                                                </li>
                                                @if(isset($data)) 
                                                  @if(!empty($data))
                                                    @if($data->isFresher=='1')
                                                <li role="presentation">
                                                    <a href="#Experiences" data-toggle="tab">
                                                         Work Experiences
                                                    </a>
                                                </li>
                                                    @endif
                                                  @endif
                                                @endif

                                                @if($usertype=='Admin' || $usertype=='admin_user')
                                                <li role="presentation">
                                                    <a href="#Official" data-toggle="tab">
                                                        Official Details
                                                    </a>
                                                </li>
                                                @endif
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade in active" id="General">
                                                    <table class="table-bordered center_tbl">
                                                      <tr>
                                                        <td>Capacity Title</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->capacity_no}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Description</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){!!base64_decode($data->description)!!}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Type</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->capacity_type}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Industry</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->industry}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Preferred Location 1</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->preferred_location}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Preferred Location 2</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->preferred_location1}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Duration of Availability</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->duration_of_availability}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Comments</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->comments}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Resource</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->source}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Partners Name</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->part_name}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Resource Detail</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->source_desc}}@endif @endif</td>
                                                      </tr>
                                                    </table>
                                                </div>
                                                @if(!isset($isPublish))
                                                <div role="tabpanel" class="tab-pane fade" id="Personal">
                                                    <table class="table-bordered center_tbl">
                                                      <tr>
                                                        <td>Name</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->name}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Phone</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->mobile}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Email</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->email}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>DOB</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->dob}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Address</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->address}}@endif @endif</td>
                                                      </tr>
                                                      
                                                    </table>
                                                </div>
                                                @endif
                                                <div role="tabpanel" class="tab-pane fade" id="Edcucation">
                                                    <table class="table-bordered">
                                                      <thead>
                                                        <tr>
                                                          <th>Height Qualification</th>
                                                          <th>Course</th>
                                                          <th>Specialization</th>
                                                          <th>University/College</th>
                                                          <th>Course Type</th>
                                                          <th>Passing Year</th>
                                                          <th>Marks Type</th>
                                                          <th>Marks Description</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        @if(isset($education))
                                                          @if(!empty($education))
                                                           @foreach($education as $row)
                                                            <tr>
                                                              <td>{{$row->ce_qualification}}</td>
                                                              <td>{{$row->ce_course}}</td>
                                                              <td>{{$row->ce_specilization}}</td>
                                                              <td>{{$row->ce_university}}</td>
                                                              <td>{{$row->ce_course_type}}</td>
                                                              <td>{{$row->ce_passyear}}</td>
                                                              <td>{{$row->ce_marktype}}</td>
                                                              <td>{{$row->ce_markdesc}}</td>
                                                            </tr>
                                                          @endforeach
                                                         @endif
                                                        @endif
                                                      </tbody>
                                                      
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="Skills">
                                                    <table class="table-bordered">
                                                      <thead>
                                                        <tr>
                                                          <th>Primary Skills</th>
                                                          <th>Secondry Skill</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        <tr>
                                                          <td>{{$data->primary_skills}}</td>
                                                          <td>{{$data->secondary_skills}}</td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                </div>
                                                @if(isset($data)) 
                                                  @if(!empty($data))
                                                    @if($data->isFresher=='1')
                                                    <div role="tabpanel" class="tab-pane fade" id="Experiences">
                                                        <table class="table-bordered center_tbl">
                                                      <tr>
                                                        <td>Total Experience</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->experience}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Company Name</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->comapany_name}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Current Designation</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->designation}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Annaul Salary</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->annaulsalary}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Working Since</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->workingsince}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Current Location</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->curr_location}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Functional Area</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->func_area}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Onsite Experience</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->onsite_experience}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Technology Track</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->tech_track}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Type of Engadgement</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->typeofengae}}@endif @endif</td>
                                                      </tr>
                                                      
                                                    </table>
                                                    </div>
                                                    @endif
                                                  @endif 
                                                @endif

                                                @if($usertype=='Admin' || $usertype=='admin_user')
                                                <div role="tabpanel" class="tab-pane fade" id="Official">
                                                    <table class="table-bordered center_tbl">
                                                      <tr>
                                                        <td>Posted Company</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->comp_name}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Posted User</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->user_name}}@endif @endif</td>
                                                      </tr>
                                                      <tr>
                                                        <td>Posted Date</td>
                                                        <td>@if(isset($data)) @if(!empty($data)){{$data->created_at}}@endif @endif</td>
                                                      </tr>
                                                    </table>
                                                </div>
                                               @endif 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           <div class="row">
                             <div class="text-center">
                              @if(isset($data))
                               @if(!empty($data))
                               @if($data->status_admin =='publish')
                                @if($company==$data->cap_companyid)
                                  <a href="{{url('capacity_change_status/shortlisted').'/'.$data->id}}" class="btn btn-primary">Add to Shortlist</a>
                                @endif
                                @endif
                                @if($usertype=='Admin' || $usertype=='admin_user')
                                 @if($data->status_admin=='inactive')
                                      <a href="{{url('capacity_change_status/publish').'/'.$data->id}}" class="btn btn-info" style="width:140px">Active</a>
                                     @else
                                      <a href="{{url('capacity_change_status/inactive').'/'.$data->id}}" class="btn btn-danger" style="width:140px">Deactive</a>
                                 @endif
                                @endif
                               @endif 
                              @endif 
                               <a href="{{url()->previous()}}" class="btn btn-danger">Close</a>
                             </div> 
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            
            <!-- #END# Exportable Table -->
        </div>
    </section>
@include('include.footer')
