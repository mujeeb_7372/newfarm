@include('include.header')
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
     @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Capacity Submitted List 
                            </h2>
                        </div>
                        <div class="row">
                           <div class="col-md-10">
                            <div class="row clearfix">
                              @include('include.capacity.capacity_search_blog')
                           </div> 
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="example1">
                                    @include('include.capacity.capacity_table_header')
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            
            <!-- #END# Exportable Table -->
        </div>
    </section>
@include('include.footer')

<script type="text/javascript">
$(document).ready(function()
{
  $('.mobile-phone-number').inputmask('9999999999', { placeholder: '__________' });
  $('.show-tick').selectpicker();
  cap_list();
  $('#cap_form_search').submit(function(e) 
  {
        e.preventDefault();
        //var formData = new FormData($(this)[0]);
        cap_list();
  })
  $('#cap_export_btn').attr('href',"{{url('export_capacity/submit')}}")
}) 
function cap_list()
{

  var _token = $('input[name="_token"]').val();
  $('#example1').DataTable(
    {
        initComplete: function() 
        {
            var api = this.api();
            $('#myTable_filter input').off('.DT').on('input.DT', function() 
            {
                api.search(this.value).draw();
            });
        },
        oLanguage: 
        {
            sProcessing: "loading..."
        },
        order: [[ 0, "desc" ]],
        
        processing: true,
        serverSide: true,
        searching: false,
        bDestroy:true,
        lengthChange: false,
        ajax: {
            "url": "{{url('ajax_list_capacity')}}",
            "type": "POST",
            "headers": 
            {
              'X-CSRF-TOKEN': _token
            },
            "data": function(d) 
            {
               var frm_data = $('#cap_form_search').serializeArray();
               $.each(frm_data, function(key, val) {
                 d[val.name] = val.value;
                 d['list_type']='submit';
                 d['user']='partner';
                 
               });
               

             },
            "error": function (xhr, error, code)
            {
                alert(error)
            }
        },
        columns: [
            
            {
              render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            {
                "data": "capacity_no"
            },
            {
                "data": "title"
            },
            {
                "data": "primary_skills"
            },
            {
                "data": "experience"
            },
            {
                "data": "company_name",
                "render":function( data, type, row, meta ) 
                {
                   return data;
                }
            },
            {
                "data": "created_at"
            },
            {
                "data": "isFresher",
                "render": function(data, type, row, meta)
                {
                   if(data=='0')
                   {
                     return 'Entry Level';
                   }
                   if(data=='1')
                   {
                    return 'Experience'
                   }
                }
            },
            {
                "render": function(data, type, row, meta)
                {
                  
                   return row.status_partner
                }
            },
            {
                "render": function(data, type, row, meta)
                {
                     var str= `<div class="btn-group">
                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                  <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">`;

                                 str+=`
                                  <li><a href="{{url('capacity/page/viewpage')}}/`+row.id+`">View</a></li>
                                  <li><a href="{{url('capacity/page/edit')}}/`+row.id+`">Edit</a></li>
                                </ul>
                              </div>`;
                      return str;
                }
            },
        ],
        
    });
}
</script>