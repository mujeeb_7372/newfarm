@include('include.header')
<style type="text/css">
.farm_custome_div
{
    
    
    border: 3px solid #DD4B39;
    padding: 10px;
}
</style>
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
         <div class="row clearfix">
           <div class="col-lg-12 d-flex ">
                    <div class="card">
                        <div class="header">
                            <h2>
                               @if($data!=NULL)
                                  Edit Requirement
                                @else
                                  Add Requirement
                               @endif
                            </h2>
                           
                        </div>
                        <div class="body">
                          <div class="farm_custome_div">
                            <form id="partner_frm">
                              <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                              <input type="hidden" id="isSaved" name="isSaved" value="false">
                              <input type="hidden" name="isEdit" value="@if($data!=NULL) {{$data->id}} @else false @endif" >
                               
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Requirement Title</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <input type="text"  @if(!empty($data)) value="{{$data->title}}" @endif  id="req_title" name="req_title" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Requirement Type</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="req_type" id="req_type" required="">
                                                    <option value="">-- Please select --</option>
                                                    <option value="Functional">Functional</option>
                                                    <option value="Technical">Technical</option>
                                                    <option value="Techno Functional">Techno Functional</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Primary Skills</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" data-role="tagsinput"  @if(!empty($data)) value="{{$data->primary_skills}}" @endif id="primary_skill" name="primary_skill">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Secondary Skills</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" data-role="tagsinput"  @if(!empty($data)) value="{{$data->secondary_skills}}" @endif id="secondry_skill" name="secondry_skill">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Requirement Description</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <textarea id="ckeditor" name="requirment_description" > @if(!empty($data)) {!!base64_decode($data->description)!!} @endif
                                                </textarea>  
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Requirement Quantity</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <input type="text" id="requr_qty" class="form-control mobile-phone-number" @if(!empty($data)) value="{{$data->qty}}" @endif name="requr_qty" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Industry</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="industry" id="industry" required="">
                                                    <option value="">-- Please select --</option>
                                                    <option value="IT Service">IT Service</option>
                                                    <option value="Telecome">Telecome</option>
                                                    <option value="IT Product Development">IT Product Development</option>
                                                    <option value="IT Engineering Service">IT Engineering Service</option>
                                                    <option value="IT Embedded Service">IT Embedded Service</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Project Type</label>
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="prj_type" id="prj_type">
                                                    <option value="">-- Please select --</option>
                                                   <option value="Requirement Gathering">Requirement Gathering</option>
                                                    <option value="Design">Design</option>
                                                    <option value="Development">Development</option>
                                                    <option value="Testing">Testing</option>
                                                    <option value="Support">Support</option>
                                                    <option value="Project Management">Project Management</option>
                                                    <option value="Architect">Architect</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Work Location</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="location" id="location" required="">
                                                    <option value="">-- Please select --</option>
                                                    <option value="INDIA">INDIA</option>
                                                    <option value="BOTH">BOTH</option>
                                                    <option value="OVERSEAS">OVERSEAS</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">City Name</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <input name="city_name" id="city_name" type="text" class="form-control" @if(!empty($data)) value="{{$data->place}}" @endif  required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Project Duration (months)</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <input type="text" name="prj_duration" id="prj_duration" class="form-control mobile-phone-number" @if(!empty($data)) value="{{$data->duration}}" @endif required="">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Min Exp.</label>
                                          <span style="color:red;">*</span>
                                          
                                            <div class="form-line">
                                                <input type="text" id="min_exp" name="min_exp" class="form-control mobile-phone-number" @if(!empty($data)) value="{{$data->min_experience}}" @endif required="">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Max Exp.</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <input type="text" id="max_exp" name="max_exp" class="form-control mobile-phone-number" @if(!empty($data)) value="{{$data->max_experience}}" @endif required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Start Date</label>
                                          <span style="color:red;">*</span>
                                            <div class="form-line">
                                                <input type="text" name="start_date" id="start_date" class="datepicker form-control" placeholder="Please choose a date..." data-dtp="dtp_lxGaC" @if(!empty($data)) value="{{$data->start_date}}" @endif required="">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Upload Job Description</label>
                                            <div class="form-line">
                                                <input type="file" id="job_doc" name="job_doc" class="form-control" accept=".doc,.docx,.pdf">
                                                @if(!empty($data)) 
                                                  <img class="upld_img" style="width:40px; height:40px;" src="https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/Documents_files_pictogram_symbol_icon_folder.png" srcc="{{$data->document_path}}">
                                                @endif 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Source</label>
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="sourcename" id="sourcename" >
                                                    <option value="">-- Please select --</option>
                                                    <option value="Registered Partner">Registered Partner</option>
                                                    <option value="Referral">Referral</option>
                                                    <option value="Job Portal">Job Portal</option>
                                                    <option value="Walkins">Walkins</option>
                                                    <option value="Digitohub Website">Digitohub Website</option>
                                                    <option value="Non Registered Partner">Non Registered Partner</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                       <div id="gendiv" style="display: none;">
                                        <label class="form-label">Source Description</label>
                                         <input type="text" placeholder="Enter Source" name="resource_val" id="resource_val"
                                         @if(isset($data))
                                            
                                                value="{{$data->source_desc}}"  
                                            
                                          @endif class="form-control">
                                         </div>
                                         <div id="gendiv1" style="display: none;">
                                          <label class="form-label">Partner</label>
                                           <select class="form-control" name="partner" id="partner">
                                             <option value="">Select</option>
                                             
                                             @foreach($partner as $val)
                                             <option value="{{$val->recruiter_id}}">{{$val->name}}</option>
                                             @endforeach
                                             
                                           </select>
                                         </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                  <div class="col-lg-12 col-md-12 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                          <label class="form-label">Comments</label>
                                            <div class="form-line">
                                               <textarea name="comments" id="comments" rows="4" class="form-control no-resize" placeholder="Please type what you want..." > @if(!empty($data)) {{$data->comment}} @endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                <div class="text-center">
                                  <a id="save_btn" class="btn btn-primary m-t-15 waves-effect">Save</a>
                                  <button type="submit" class="btn btn-success m-t-15 waves-effect">Submit</button>
                                  <a href="{{ url()->previous()}}" class="btn btn-danger m-t-15 waves-effect">Cancel</a>
                                </div>
                                
                            </form>
                          </div>
                        </div>
                    </div>
                </div>
         </div>
       </div> 
    </section>
@include('include.footer')

<script type="text/javascript">
$(document).ready(function()
{ 
  /*CKEDITOR.replace('ckeditor');
  CKEDITOR.config.height = 160;*/


  tinymce.init({
        selector: "textarea#ckeditor",
        theme: "modern",
        height: 300,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
    });
    tinymce.suffix = ".min";
    tinyMCE.baseURL = '{{URL::asset("public/src/")}}/plugins/tinymce';


   @if(!empty($data))
     $('#req_type').val('{{$data->requirement_type}}');
     $('#industry').val('{{$data->industry}}');
     $('#prj_type').val('{{$data->project_type}}');
     $('#location').val('{{$data->work_location}}');
     $('#sourcename').val('{{$data->source}}');
     $('.show-tick').selectpicker('refresh')
   @endif
  $('.mobile-phone-number').inputmask('9999999999', { placeholder: '__________' });
   $('.show-tick').selectpicker();
    
    $('#partner_frm').submit(function(e) 
     {
        
      $(".farm_custome_div").loading();

        e.preventDefault();
        var url="";
        var editor='ckeditor';
        var content =tinyMCE.activeEditor.getContent();
        
        var formData = new FormData($(this)[0]);
        formData.append('requirment_description',content);
        url="{{url('ajax_add_requirement')}}";

         $.ajax({
           type: 'POST',
           cache: false,
           headers: 
           {
             'X-CSRF-TOKEN': $('#_token').val()
           },
           url: url,
           data:formData,
           processData : false,
           contentType: false,
           dataType:'json',
           success: function(data)
           {
             $('.farm_custome_div').loading('stop');
               if(data.type=='success')
               {
                    swal({
                        title:"Response",
                        text:data.msg,
                        type:data.type,
                        //imageUrl: "{{URL::asset('public/src/images/loader.gif')}}",
                        showCancelButton:false,
                         showConfirmButton: true
                     },function()
                     { 
                       if($('#isSaved').val()=='true')
                       {
                          window.location.href="{{url('requirement/page/saved_list/0')}}";
                       }
                       if($('#isSaved').val()=='false')
                       {
                          window.location.href="{{url('requirement/page/submit_list/0')}}";
                       }
                       
                     })
               }
               
           },
           error: function (request, status, error) 
           {

            $('.farm_custome_div').loading('stop');
                 var str="<ul style='color:red;'>";
                 $.each(request.responseJSON.errors, function(key, value)
                 {
                    
                    if(key!='type')
                    {
                      $('#'+key).css({ "border": '#FF0000 1px solid'});
                      str+='<li>'+value+'</li>';                        
                    }                
                 });
                
                 str+='</ul>';
                 
                 swal({
                 title:"The following fields were filled incorrectly",
                  html:true,
                  type:'error',
                  text:str,
                 })
                 //$(".splerror").html(str);
                 
             }
       });
        
     })

    $('#sourcename').change(function() 
    {

      var val = $(this).val();

      if (val == '') {
          $('#gendiv').hide()
          $('#gendiv1').hide()
          return false;
      }
      if (val == 'Registered Partner') {
          $('#gendiv').hide()
          $('#gendiv1').show()

      } else { 

          $('#gendiv').show()
          $('#gendiv1').hide()
      }
    })
   $('#save_btn').click(function()
   {
    $('#isSaved').val('true');
    $('#partner_frm').submit();
   }) 
}) 


</script>