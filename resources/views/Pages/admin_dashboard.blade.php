@include('include.header')


<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')

   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
      <div class="container-fluid">
         <div class="block-header">
            <h2>DASHBOARD</h2>
         </div>
         <!-- Widgets -->
         <div class="row clearfix">
           @if($usertype=='Admin' || $usertype=='admin_user') 
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <div class="info-box bg-pink hover-expand-effect">
                  <div class="icon">
                     <i class="material-icons">people</i>
                  </div>
                  <div class="content">
                     <div class="text">Total Active Partners</div>
                     <div class="number count-to" data-from="0" data-to="{{$active_part}}" data-speed="15" data-fresh-interval="20"></div>
                  </div>
               </div>
            </div>
            @endif
            <div class="@if($usertype=='Admin' || $usertype=='admin_user') col-lg-col-md-6 @else col-lg-col-md-6 @endif">
               <div class="info-box bg-cyan hover-expand-effect">
                  <div class="icon">
                     <i class="material-icons">face</i>
                  </div>
                  <div class="content">
                     <div class="text">Total Active Users</div>
                     <div class="number count-to" data-from="0" data-to="{{$active_user}}" data-speed="1000" data-fresh-interval="20"></div>
                  </div>
               </div>
            </div>
            
         </div>
         
         <!-- #END# Widgets -->
         
         <!-- #END# CPU Usage -->
         <div class="row clearfix">
            <!-- Visitors -->
            
            <!-- #END# Visitors -->
            <!-- Latest Social Trends -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
               <div class="card">
                  <div class="body bg-cyan">
                     <div class="m-b--35 font-bold text-center h5">Requirement</div>
                     <ul class="dashboard-stat-list">
                        <li>
                           <a href="{{url('requirement/page/add/0')}}" class="btn btn-success waves-effect">
                              <i class="material-icons">home</i>
                              <span>Add</span>
                          </a>
                           <span class="pull-right">
                             <a href="{{url('requirement/page/global_list/0')}}" class="btn bg-purple waves-effect">
                                    <i class="material-icons">list</i>
                                    <span>Listing</span>
                             </a>
                           </span>
                        </li>
                       
                     </ul>
                     <div id="pie_chart" class="flot-chart"></div>
                  </div>
               </div>
            </div>
            <!-- #END# Latest Social Trends -->
            <!-- Answered Tickets -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
               <div class="card">
                  <div class="body bg-teal">
                     <div class="font-bold m-b--35 text-center h5">Capacity</div>
                     <ul class="dashboard-stat-list">
                        <li>
                           <a href="{{url('capacity/page/add/0')}}" class="btn btn-success waves-effect">
                              <i class="material-icons">home</i>
                              <span>Add</span>
                          </a>
                           <span class="pull-right">
                             <a href="{{url('capacity/page/global_list/0')}}" class="btn bg-purple waves-effect">
                                    <i class="material-icons">list</i>
                                    <span>Listing</span>
                             </a>
                           </span>
                        </li>
                        
                     </ul>
                     <div id="pie_chart_1" class="flot-chart"></div>
                  </div>
               </div>
            </div>
            <!-- #END# Answered Tickets -->
         </div>
         
            <!-- #END# Task Info -->
            <!-- Browser Usage -->
            
            <!-- #END# Browser Usage -->
         </div>
      </div>
   </section>

   <div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="defaultModalLabel">User Profile</h4>
              </div>
              <form>
              <div class="modal-body">
                  <div class="form-group">
                     <label for="pwd">Email:</label>
                     <input type="text" class="form-control" value="test@gmail.com" readonly="">
                   </div>
                   <div class="form-group">
                     <label for="pwd">New Password:</label>
                     <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
                   </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button>
                  <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
           </form>
          </div>
      </div>
</div>
   @include('include.footer')
   <script src="{{URL::asset('public/src/')}}/js/pages/index.js"></script>
   <script src="https://envato.stammtec.de/themeforest/melon/plugins/flot/jquery.flot.tooltip.min.js"></script>
   <script type="text/javascript">

    var data = [
        {label: "Saved", data:{{$saved}}},
        {label: "Submit", data: {{$submit}}},
        {label: "Short List", data: {{$short_list}}},
        {label: "Checkout", data: {{$checkout}}},
        
        @if($usertype=='Admin' || $usertype=='admin_user') 

        {label: "Deactive", data: {{$deactive}}},
        {label: "Publish", data: {{$publish}}},
        {label: "Part Submi", data:{{$part_submit}}},
        {label: "Part Checkout", data:{{$part_checkout}}}

        @endif
    ];
    
    var data2 = [
        {label: "Saved", data:{{$cap_saved}}},
        {label: "Submit", data: {{$cap_submit}}},
        {label: "Short List", data: {{$cap_short_list}}},
        {label: "Checkout", data: {{$cap_checkout}}},
        @if($usertype=='Admin' || $usertype=='admin_user') 

        {label: "Deactive", data: {{$cap_deactive}}},
        {label: "Publish", data: {{$cap_publish}}},
        {label: "Part Submi", data:{{$cap_part_submit}}},
        {label: "Part Checkout", data:{{$cap_part_checkout}}}

        @endif
    ];
    pie_chart('#pie_chart',data);
    pie_chart('#pie_chart_1',data2);

   function pie_chart(element,pieChartData)
   {
         $.plot(element, pieChartData, 
         {
           series: 
           {
            pie: 
            {
              show: true,
              radius: 1,
              label: 
              {
                show: true,
                radius: 3 / 4,
                formatter: labelFormatter,
                /*background: 
                {
                  opacity: 0.5
                }*/
              }
            }
           },
           legend: 
           {
               show: true
           },
           grid: 
           {
            hoverable: true
          },
          tooltip: true,
          tooltipOpts: 
          {
              cssClass: "flotTip",
              content: "%p.0%, %s",
              shifts: {
                  x: 20,
                  y: 0
              },
              defaultTheme: false
          }

       });
   }
   function labelFormatter(label, series) 
   {
      return '<div style="font-size:8pt; text-align:center; padding:2px; color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
   }
</script>