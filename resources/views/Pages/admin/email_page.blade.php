@include('include.header')
<style type="text/css">
   .mail-box {
   border-collapse: collapse;
   border-spacing: 0;
   display: table;
   table-layout: fixed;
   width: 100%;
   }
   .mail-box aside {
   display: table-cell;
   float: none;
   height: 100%;
   padding: 0;
   vertical-align: top;
   }
   .mail-box .sm-side {
   width: 25%;
   background: #ecf0f1;
   border-radius: 4px 0 0 4px;
   -webkit-border-radius: 4px 0 0 4px;
   }
   .mail-box .lg-side {
   width: 75%;
   background: #fff;
   border-radius: 0px 4px 4px 0;
   -webkit-border-radius: 0px 4px 4px 0;
   }
   .mail-box .sm-side .user-head {
   background: #2980b9;
   border-radius: 4px 0px 0px 0;
   -webkit-border-radius: 4px 0px 0px 0;
   padding: 10px;
   color: #fff;
   min-height: 80px;
   }
   .user-head .inbox-avatar {
   width: 65px;
   float: left;
   }
   .user-head .inbox-avatar img {
   border-radius: 4px;
   -webkit-border-radius: 4px;
   }
   .user-head .user-name {
   display: inline-block;
   margin:0 0 0 10px;
   }
   .user-head .user-name h5 {
   font-size: 14px;
   margin-top: 15px;
   margin-bottom: 0;
   font-weight: 300;
   }
   .user-head .user-name h5 a {
   color: #fff;
   }
   .user-head .user-name span a {
   font-size: 12px;
   color: #87e2e7;
   }
   a.mail-dropdown {
   background: #1abc9c;
   padding:3px 5px;
   font-size: 10px;
   color: #ddd;
   border-radius: 2px;
   margin-top: 20px;
   }
   .inbox-body {
   padding: 20px;
   }
   .btn-compose {
   background: #9b59b6;
   padding: 12px 0;
   text-align: center;
   width: 100%;
   color: #fff;
   }
   .btn-compose:hover {
   background: #8e44ad;
   color: #fff;
   }
   ul.inbox-nav  {
   display: inline-block;
   width: 100%;
   margin: 0;
   padding: 0;
   }
   .inbox-divider {
   border-bottom: 1px solid #d5d8df;
   }
   ul.inbox-nav li {
   display: inline-block;
   line-height: 45px;
   width: 100%;
   }
   ul.inbox-nav li a  {
   color: #6a6a6a;
   line-height: 45px;
   width: 100%;
   display: inline-block;
   padding: 0 20px;
   }
   ul.inbox-nav li a:hover, ul.inbox-nav li.active a, ul.inbox-nav li a:focus  {
   color: #6a6a6a;
   background: #d5d7de;
   }
   ul.inbox-nav li a i {
   padding-right: 10px;
   font-size: 16px;
   color: #6a6a6a;
   }
   ul.inbox-nav li a span.label {
   margin-top: 13px;
   }
   ul.labels-info li h4 {
   padding-left:15px;
   padding-right:15px;
   padding-top: 5px;
   color: #5c5c5e;
   font-size: 13px;
   text-transform: uppercase;
   }
   ul.labels-info li  {
   margin: 0;
   }
   ul.labels-info li a {
   color: #6a6a6a;
   border-radius: 0;
   }
   ul.labels-info li a:hover, ul.labels-info li a:focus {
   color: #6a6a6a;
   background: #d5d7de;
   }
   ul.labels-info li a i {
   padding-right: 10px;
   }
   .nav.nav-pills.nav-stacked.labels-info p {
   margin-bottom: 0;
   padding: 0 22px;
   color: #9d9f9e;
   font-size: 11px;
   }
   .inbox-head {
   padding:20px;
   background: #3498db;
   color: #fff;
   border-radius: 0 4px 0 0;
   -webkit-border-radius: 0 4px 0 0;
   min-height: 80px;
   }
   .inbox-head  h3 {
   margin: 0;
   display: inline-block;
   padding-top: 6px;
   font-weight: 300;
   }
   .inbox-head  .sr-input {
   height: 40px;
   border: none;
   box-shadow: none;
   padding: 0 10px;
   float: left;
   border-radius: 4px 0 0 4px;
   color: #8a8a8a;
   }
   .inbox-head  .sr-btn {
   height: 40px;
   border: none;
   background: #2980b9;
   color: #fff;
   padding: 0 20px;
   border-radius: 0 4px 4px 0;
   -webkit-border-radius: 0 4px 4px 0;
   }
   .table-inbox {
   border: 1px solid #d3d3d3;
   margin-bottom: 0;
   }
   .table-inbox tr td{
   padding: 12px !important;
   }
   .table-inbox tr td:hover{
   cursor: pointer;
   }
   .table-inbox tr td .fa-star.inbox-started ,.table-inbox tr td .fa-star:hover{
   color: #f78a09;
   }
   .table-inbox tr td .fa-star{
   color: #d5d5d5;
   }
   .table-inbox tr.unread td {
   font-weight: 600;
   background: #f7f7f7;
   }
   ul.inbox-pagination  {
   float: right;
   list-style: none;
   }
   ul.inbox-pagination li {
   float: left;
   }
   .mail-option {
   display: inline-block;
   margin-bottom: 10px;
   width: 100%;
   }
   .mail-option .chk-all, .mail-option .btn-group {
   margin-right: 5px;
   }
   .mail-option .chk-all, .mail-option .btn-group a.btn {
   border: 1px solid #e7e7e7;
   padding: 5px 10px;
   display: inline-block;
   background: #fcfcfc;
   color: #afafaf;
   border-radius: 3px !important;
   -webkit-border-radius: 3px !important;
   }
   .inbox-pagination a.np-btn  {
   border: 1px solid #e7e7e7;
   padding: 5px 15px;
   display: inline-block;
   background: #fcfcfc;
   color: #afafaf;
   border-radius: 3px !important;
   -webkit-border-radius: 3px !important;
   }
   .mail-option .chk-all input[type=checkbox] {
   margin-top: 0;
   }
   .mail-option .btn-group a.all {
   padding: 0;
   border: none;
   }
   .inbox-pagination a.np-btn {
   margin-left: 5px;
   }
   .inbox-pagination li span {
   display: inline-block;
   margin-top: 7px;
   margin-right: 5px;
   }
   .fileinput-button {
   border: 1px solid #e6e6e6;
   background: #eeeeee;
   }
   .inbox-body .modal .modal-body input, .inbox-body .modal .modal-body textarea{
   border: 1px solid #e6e6e6;
   box-shadow: none;
   }
   .btn-send, .btn-send:hover {
   background: #00A8B3;
   color: #fff;
   }
   .btn-send:hover {
   background: #009da7;
   }
   .modal-header h4.modal-title {
   font-weight: 300;
   font-family: 'Open Sans', sans-serif;
   }
   .modal-body label {
   font-weight: 400;
   font-family: 'Open Sans', sans-serif;
   }
   .heading-inbox h4{
   font-size: 18px;
   color: #444;
   border-bottom: 1px solid #ddd;
   padding-bottom: 10px;
   margin-top: 20px;
   }
   .sender-info {
   margin-bottom: 20px;
   }
   .sender-info img {
   width: 30px;
   height: 30px;
   }
   .sender-dropdown {
   background: #eaeaea;
   padding:0 3px;
   color: #777;
   font-size: 10px;
   }
   .view-mail a {
   color: #FF6C60;
   }
   .attachment-mail {
   margin-top: 30px;
   }
   .attachment-mail ul {
   width: 100%;
   display: inline-block;
   margin-bottom: 30px;
   }
   .attachment-mail ul li {
   float: left;
   width: 150px;
   margin-right: 10px;
   margin-bottom: 10px;
   }
   .attachment-mail ul li img {
   width: 100%;
   }
   .attachment-mail ul li span {
   float: right;
   }
   .attachment-mail .file-name {
   float: left;
   }
   .attachment-mail .links {
   width: 100%;
   display: inline-block;
   }
</style>
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
     @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
      <div class="container bootstrap snippets">
         <div class="row">
            <div class="col-lg-12">
               <div class="box">
                  <!--mail inbox start-->
                  <div class="mail-box">
                     <aside class="sm-side">
                        <div class="user-head">
                           <a href="javascript:;" class="inbox-avatar">
                           <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="" class="img-responsive">
                           </a>
                           <div class="user-name">
                              <h5><a href="#">@if(isset($data)) {{$data->name}} @endif</a></h5>
                              <span><a href="#">@if(isset($data)) {{$data->email}} @endif</a></span>
                           </div>
                           
                           <i class="fa fa-chevron-down"></i>
                           </a>
                        </div>
                        <div class="inbox-body">
                           <a class="btn btn-compose" >
                           Compose
                           </a>
                           <!-- Modal -->
                           
                           <!-- /.modal -->
                        </div>
                        <ul class="inbox-nav inbox-divider">
                           <li class="active" href="#">
                              <a class="tab_btn" data-tab="inbox" id="inbox"><i class="material-icons">forum</i> Inbox </a>
                           </li>
                           <li>
                              <a class="tab_btn" data-tab="sentmail" id="sentmail" href="#"><i class="material-icons">flight_takeoff</i> Sent Mail</a>
                           </li>
                           <li>
                              <a class="tab_btn" data-tab="trash" id="trash" href="#"><i class="material-icons">list</i> ALL Mail</a>
                           </li>
                        </ul>
                     </aside>
                     <aside class="lg-side">
                        <div class="inbox-head">
                           <h3>Inbox</h3>
                           <form class="pull-right position" id="serch_email_frm">
                            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="status" id="status" value="inbox"> 
                           </form>
                        </div>
                        <div class="inbox-body">
                           <table class="table table-inbox table-hover" id="example1" >
                              <tbody>
                                
                              </tbody>
                           </table>
                        </div>
                     </aside>
                  </div>
                  <!--mail inbox end-->
               </div>
            </div>
         </div>
      </div>
   </section>

   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
       <div class="modal-content" style="background-color:aquamarine;">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
             <h4 class="modal-title">Compose</h4>
          </div>
          <div class="modal-body">
             <form class="form-horizontal" enctype="multipart/form-data" method="post" role="form" id="email_frm">
                <div class="form-group">
                   <label class="col-lg-2 control-label">To</label>
                   <div class="col-lg-10">
                      <select class="form-control select2" id="partner" name="partner" required>
                        <option value="">Select</option>
                         @if($usertype=='Admin' || $usertype=='admin_user') 
                          @if(isset($partner))
                            @foreach($partner as $row)
                            <option value="{{$row->company_id}}">{{$row->name}}</option>
                            @endforeach
                          @endif
                           @else if($usertype=='Partner' || $usertype=='partner_user')
                            <option value="1">Admin</option>
                         @endif 
                      </select>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-2 control-label">Subject</label>
                   <div class="col-lg-10">
                      <input type="text" class="form-control" id="subject" name="subject" placeholder="">
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-2 control-label">Message</label>
                   <div class="col-lg-10">
                      <textarea name="msg" id="msg" class="form-control" cols="30" rows="10"></textarea>
                   </div>
                </div>
                <br>
                <div class="form-group">
                   <div class="col-lg-offset-2 col-lg-10">
                      <span class="btn green fileinput-button" data-original-title="" title="">
                      <i class="fa fa-plus fa fa-white"></i>
                      <span>Attachment</span>
                      <input type="file" name="emial_file" id="emial_file">
                      </span>
                      
                   </div>
                </div>
                <br>
                <div class="form-group text-center">
                  <label class="col-lg-2 control-label"></label>
                  <button type="submit" class="btn btn-send">Send</button>
                </div>
             </form>
          </div>
       </div>
       <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
 </div>

<div id="email_model" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Email Message</h4>
      </div>
      <div class="modal-body">
        <p>Subject :</p> <b><p id="em_sub"></p></b><br>
        <p>Message :</p> <b><p id="em_msg"></p></b><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="email_file" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <a href="" class="pull-right" id="download_link">Download</a>&nbsp;&nbsp;
        <h4 class="modal-title">Email File</h4>
      </div>
      
      <div class="modal-body">
        <embed class="modal-content" id="img01" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
 @include('include.footer')
<script type="text/javascript">
$(document).ready(function()
{
    $('#openbtn').click();
    $('.btn-compose').click(function()
    {  
       $("#myModal").modal();
       $('.modal-backdrop').hide()
       
    });
    $('.show-tick').selectpicker();
    user_list();
    $('#user_form_search').submit(function(e) 
    {
        e.preventDefault();
        //var formData = new FormData($(this)[0]);
        user_list();
    });
    $('.tab_btn').click(function()
    { 
       var x=$(this).data('tab')
       $('#status').val(x)
       user_list()
    })
    
    $("#example1").on("click", ".open_msg", function (event) 
    {
       var subject=$(this).data('subject');
       var message=$(this).data('message');
       var send_id=$(this).data('sendid');

       $('#em_sub').text(subject)
       $('#em_msg').text(message)

       $('#email_model').modal('show');
       $('.modal-backdrop').hide()
       read_update(send_id,'read','inbox');
    });
    $('#example1').on('click','.open_file',function(event)
    {

        var modal = document.getElementById("email_file");
        var modalImg = document.getElementById("img01");
        
        var downloadlin=document.getElementById('download_link')

        
        modal.style.display = "block";
        modalImg.src = $(this).data('path');
        downloadlin.href=$(this).data('path');
        captionText.innerHTML = this.alt;

        $("#email_file").modal();
       $('.modal-backdrop').hide()

    });
    $('#example1').on('click','.mail_delete',function(event)
    {
      var email_id=$(this).data('id');
      var es_id=$(this).data('esid');
      if($('#status').val()=='inbox')
      {
          read_update(es_id,'delete','inbox');
      }
      if($('#status').val()=='sentmail')
      {
         read_update(es_id,'delete','sent');
      }
      if($('#status').val()=='trash')
      {
         read_update(es_id,'delete','trash');
      }
      
    });

    $('#email_frm').submit(function(e) 
    {
        e.preventDefault();


        var formData = new FormData($(this)[0]);
         $.ajax({
           type: 'POST',
           headers: 
           {
             'X-CSRF-TOKEN': $('#_token').val()
           },
           url:"{{url('save_email')}}",
           processData : false,
           contentType: false,
           cache: false,
           data:formData,
           dataType:'json',
           success: function(data)
           {
              swal({
                title:"Response",
                text:data.msg,
                type:data.type,
                imageUrl: "https://gurayyarar.github.io/AdminBSBMaterialDesign/images/thumbs-up.padding",
                showCancelButton:false,
                showConfirmButton: true
              },function()
              {
                  window.location.href="";
                  
              }) 
              
           },
         });      
    });


}) 
  function read_update(id,action,from_list)
  {
    $.ajax({

           type:'POST',
           headers: 
           {
             'X-CSRF-TOKEN': $('#_token').val()
           },
           url:"{{url('update_read_email')}}",

           data:{'id':id,'action':action,'from_list':from_list},

           success:function(data)
           {
             user_list();
           }

        });
  } 
  function user_list()
  {
  
    var _token = $('input[name="_token"]').val();
    $('#example1').DataTable(
      {
          initComplete: function() 
          {
              var api = this.api();
              $('#myTable_filter input').off('.DT').on('input.DT', function() 
              {
                  api.search(this.value).draw();
              });
          },
          oLanguage: 
          {
              sProcessing: "loading..."
          },
         processing: true,
          serverSide: true,
          searching: true,
          bDestroy:true,
          ordering: false,
          lengthChange: false,
          ajax: {
              "url": "{{url('getEmail')}}",
              "type": "POST",
              "headers": 
              {
                'X-CSRF-TOKEN': _token
              },
               "data": function(d) 
              {
                 var frm_data = $('#serch_email_frm').serializeArray();
                 $.each(frm_data, function(key, val) {
                   d[val.name] = val.value;
                   
                 });
                 

               },
              "error": function (xhr, error, code)
              {
                  alert(error)
              }
          },
          columns: [
              {
                  "data": "name"
              },
              {
                  "data": "em_subject"
              },
              {
                  "data": "em_date"
              },
              {
                  "render": function ( data, type, row, meta ) 
                  {
                    var strs=`<button data-id="`+row.em_id+`" type="button" class="btn btn-success waves-effect mail_open open_msg" id="open_msg" data-subject="`+row.em_subject+`" data-message="`+row.em_message+`" data-sendid="`+row.es_id+`">
                                <i class="material-icons">folder_open</i> 
                            </button>
                           <!-- <button data-id="`+row.em_id+`" data-esid="`+row.es_id+`" type="button" class="btn btn-danger waves-effect mail_delete">
                                <i class="material-icons">delete</i> 
                            </button> --> `;

                     if(row.em_file!='')
                     {
                       strs+=`<button id="open_file" data-id="`+row.em_id+`" data-path="`+row.em_file+`" type="button" class="btn btn-primary waves-effect  open_file">
                                <i class="material-icons">find_in_page</i> 
                            </button>`;
                     }
                     return strs;
                  }
              },

          ],
          "createdRow": function(row, data, dataIndex) 
          { 
            if(data['er_isread']=='0')
            {
               $(row).addClass("unread");
            }
          },
          
      });
  }
   </script>