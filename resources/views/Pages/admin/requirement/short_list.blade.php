@include('include.header')
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Requirement Short List 
                            </h2>
                            <ul class="header-dropdown m-r--5">
                              
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                           <div class="col-md-10">
                            <div class="row clearfix">
                              @include('include.requirement.requirement_search_blog')
                           </div> 
                        </div>

                        <form id="shortlist_frm">
                          <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="example1">
                                    <thead>
                                      <tr>
                                        <th>Sr No.</th>
                                        <th>Partner</th>
                                        <th>Req No.</th>
                                        <th>Title</th>
                                        <th>Primary Skill</th>
                                        <th>Type</th>
                                        <th>

                                          <input type="checkbox" id="md_checkbox_0" class="filled-in chk-col-brown" name="select_all">
                                          <label for="md_checkbox_0">DEEP ORANGE</label>
                                        </th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="row text-center">
                              <br>
                              <div class="col-md-10">
                                <button type="submit" class="btn bg-deep-purple waves-effect">
                                 <i class="material-icons">assignment</i>
                                 <span>Submit</span>
                               </button>
                              </div>
                           </div>
                        </div>
                       </form> 
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            
            <!-- #END# Exportable Table -->
        </div>
    </section>
@include('include.footer')
<script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript">
   var table;
   var rows_selected = [];

$(document).ready(function()
{
   $('.mobile-phone-number').inputmask('9999999999', { placeholder: '__________' });
   $('.show-tick').selectpicker();

   user_list();

   $('#user_form_search').submit(function(e) 
   {
      e.preventDefault();
        //var formData = new FormData($(this)[0]);
      user_list();
   })

   $('#req_export_btn').attr('href',"{{url('export_requirement/shortlist')}}")
})
  

function user_list()
{

  var _token = $('input[name="_token"]').val();
  window.table=$('#example1').DataTable(
    {
        initComplete: function() 
        {
            var api = this.api();
            $('#myTable_filter input').off('.DT').on('input.DT', function() 
            {
                api.search(this.value).draw();
            });
        },
        oLanguage: 
        {
            sProcessing: "loading..."
        },
        order: [[ 0, "desc" ]],
        processing: true,
        serverSide: true,
        bDestroy:true,
        lengthChange: true,
        searchable: false,
        orderable: false,
        
        ajax: {
            "url": "{{url('ajax_shortlist_requirement')}}",
            "type": "POST",
            "headers": 
            {
              'X-CSRF-TOKEN': _token
            },
            "data": function(d) 
            {
               var frm_data = $('#reqFrm').serializeArray();
               $.each(frm_data, function(key, val) 
               {
                 d[val.name] = val.value;
               });

               d['list_type']='shortlist';
               d['user']='admin';
               d['short_status']='0';
             },
            "error": function (xhr, error, code)
            {
                alert(error)
            }
        },
        'columnDefs': [{
         'targets': 6,
         'searchable':false,
         'orderable':false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
             return '<input type="checkbox" name="id[]" value="' 
                + $('<div/>').text(data).html() + '">';
         }
      }],
         columns: [
            
            {
              render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            {
                "data": "company_name"
            },
            {
                "data": "requirement_id"
            },
            {
                "data": "title"
            },
            {
                "data": "primary_skills"
            },
            {
                "data": "project_type"
            },
            
            {
                "render":function( data, type, row, meta ) 
                {
                   return `
                         <input type="checkbox" id="md_checkbox_`+row.id+`" class="filled-in chk-col-brown">
                         <label for="md_checkbox_`+row.id+`">DEEP ORANGE</label>`;
                }
            },
            {
                "render": function(data, type, row, meta)
                {
                  var str= `<a href="{{url('view_requirement_page')}}/`+row.id+`"  class="btn btn-info btn-circle waves-effect waves-circle waves-float">
                              <i class="material-icons">remove_red_eye</i>
                            </a>`;
                  return str;        
                }
            },
        ],
        'rowCallback': function(row, data, dataIndex){
         // Get row ID
         var rowId = data.short_id;

         // If row ID is in the list of selected row IDs
         if($.inArray(rowId, rows_selected) !== -1){
            $(row).find('input[type="checkbox"]').prop('checked', true);
            $(row).addClass('selected');
         }
      }
       
        
    });

}
$(document).ready(function()
{
   $('#example1 tbody').on('click', 'input[type="checkbox"]', function(e){
      var $row = $(this).closest('tr');

      // Get row data
      var data = table.row($row).data();

      // Get row ID
      var rowId = data.short_id;

      // Determine whether row ID is in the list of selected row IDs
      var index = $.inArray(rowId, rows_selected);

      // If checkbox is checked and row ID is not in list of selected row IDs
      if(this.checked && index === -1){
         rows_selected.push(rowId);

      // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
      } else if (!this.checked && index !== -1){
         rows_selected.splice(index, 1);
      }

      if(this.checked){
         $row.addClass('selected');
      } else {
         $row.removeClass('selected');
      }

      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle click on table cells with checkboxes
   $('#example1').on('click', 'tbody td, thead th:first-child', function(e){
      $(this).parent().find('input[type="checkbox"]').trigger('click');
   });

   // Handle click on "Select all" control
   $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
      if(this.checked){
         $('#example1 tbody input[type="checkbox"]:not(:checked)').trigger('click');
      } else {
         $('#example1 tbody input[type="checkbox"]:checked').trigger('click');
      }

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle table draw event
   table.on('draw', function(){
      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);
   });

   $('#shortlist_frm').on('submit', function(e)
   {
      var arr=[];
      $.each(rows_selected, function(index, rowId)
      {
          arr.push(rowId)
          
      });
      if(arr.length==0)
      {
        
        swal({
                title: "Error",
                text:'Please Select Requirment',
                type:'error',
                
              })
        return false;
      }

      e.preventDefault();
      
      $.ajax({
          type: 'POST',
          cache: false,
          headers: 
          {
              'X-CSRF-TOKEN': $('#_token').val()
          },
          url:"{{url('movetocheckout')}}",
          data:JSON.stringify(arr),
          processData : false,
          contentType: false,
          dataType:'json',
          success: function(data)
          {
              swal({
                  title: "Response",
                  text: data,
                  type:'success',
               },function()
               {
                 window.location.href="{{url('checkoutrequirment')}}";
               })
              
          },
          error: function (request, status, error) 
          {
                json = $.parseJSON(request.responseText);
                 //printErrorMsg(json.error);
                $('.splerror').show();
                var str="<ul style='color:red;'>";
                $.each(request.responseJSON, function(key, value)
                {
                   str+='<li>'+value+'</li>';                        
                   
                });
                str+='</ul>';
                swal({
                      title:"Required Fields",
                      html:true,
                      text:str,
                      
                    })
                /*$(".splerror").html(str);*/
                
            }
      });
   });
  
})
function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}
</script>