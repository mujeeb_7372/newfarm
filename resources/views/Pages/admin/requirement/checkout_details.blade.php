@include('include.header')
<style type="text/css">
  .popover{
    max-width: 100%;
    width: <width>;
}
</style>
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
               <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6" style="padding-left: 1px !important; padding-right: 1px !important;">
                  <div class="card">
                     <div class="body bg-cyan">
                        <div class="m-b--35 font-bold">Requirment Details</div>
                        <ul class="dashboard-stat-list">
                           <li>
                              Requirement Id
                              <span class="pull-right">
                              {{$data->requirement_id}}
                              </span>
                           </li>
                           <li>
                              Title
                              <span class="pull-right">
                              {{$data->title}}
                              </span>
                           </li>
                           <li>
                              Description
                              <span class="pull-right">
                                <a data-toggle="popover" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?">
                                   
                                </a>
                             <button type="button" class="btn btn-primary btn-block waves-effect" data-trigger="focus" data-container="body" data-toggle="popover" data-placement="right" title="" data-content="{!!base64_decode($data->description)!!}" data-original-title="Description">
                                    Click
                                </button>
                              </span>
                           </li>
                           <li>
                              Type
                              <span class="pull-right">
                              {{$data->requirement_type}}
                              </span>
                           </li>
                           <li>
                              Industry
                              <span class="pull-right">
                              {{$data->industry}}
                              </span>
                           </li>
                           <li>
                              Duration
                              <span class="pull-right">
                              {{$data->duration}}
                              </span>
                           </li>
                           <li>
                             Start Date
                              <span class="pull-right">
                              {{$data->start_date}}
                              </span>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6" style="padding-left: 1px !important; padding-right: 1px !important;">
                  <div class="card">
                     <div class="body bg-cyan">
                        <div class="m-b--35 font-bold">Requirment Details</div>
                        <ul class="dashboard-stat-list">
                           <li>
                              Primary Skills  
                              <span class="pull-right">
                              {{$data->primary_skills}}
                              </span>
                           </li>
                           <li>
                              Secondary Skills
                              <span class="pull-right">
                              {{$data->secondary_skills}}
                              </span>
                           </li>
                           <li>
                              Min Experience
                              <span class="pull-right">
                              {{$data->min_experience}}
                              </span>
                           </li>
                           <li>
                              Max Experience
                              <span class="pull-right">
                              {{$data->max_experience}}
                              </span>
                           </li>
                           <li>
                              Requirement Number
                              <span class="pull-right">
                              1
                              </span>
                           </li>
                           <li>
                             Work Location  
                              <span class="pull-right">
                              {{$data->work_location}}
                              </span>
                           </li>
                           <li>
                             Place  
                              <span class="pull-right">
                              {{$data->place}}
                              </span>
                           </li>
                           

                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
                  <div class="card">
                     <div class="body bg-teal">
                        <div class="m-b--35 font-bold">Posted Partner Details</div>
                        <ul class="dashboard-stat-list">
                           <li>
                              Company Name
                              <span class="pull-right">
                              {{$data->posted_company}}
                              </span>
                           </li>
                           <li>
                              Primary Contact Name
                              <span class="pull-right">
                              {{$data->posted_contactname}}
                              </span>
                           </li>
                           
                           <li>
                              Contact Number
                              <span class="pull-right">
                              {{$data->posted_contact}}
                              </span>
                           </li>
                           <li>
                              Email ID
                              <span class="pull-right">
                              {{$data->posted_email}}
                              </span>
                           </li>
                           
                           
                        </ul>
                     </div>
                  </div>
               </div>    
               <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
                  <div class="card">
                     <div class="body bg-pink">
                        <div class="m-b--35 font-bold">Checkedout Partner Details</div>
                        <ul class="dashboard-stat-list">
                           <li>
                              Company Name
                              <span class="pull-right">
                              {{$data->checkout_company}}
                              </span>
                           </li>
                           <li>
                              Primary Contact Name
                              <span class="pull-right">
                              {{$data->checkout_contactname}}
                              </span>
                           </li>
                           
                           <li>
                              Contact Number
                              <span class="pull-right">
                              {{$data->checkout_contact}}
                              </span>
                           </li>
                           <li>
                              Email ID
                              <span class="pull-right">
                              {{$data->checkout_email}}
                              </span>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
                 
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
              <div class="card">
                <div class="body bg-green">
                  <div class="m-b--35 font-bold">Admin Action</div>
                  <ul class="dashboard-stat-list">
                    <form class="form-horizontal" id="checkout_frm">
                        <input type="hidden" name="trs_id" id="trs_id" value="{{$data->short_id}}">
                        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="page" value="true">
                        <div class="box-body">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Work Status</label>

                            <div class="col-sm-10">
                              <select class="form-control show-tick" id="workstatus" name="workstatus">
                                <option value="0">Select</option>
                                <option value="Waiting for Validation ">Waiting for Validation </option>
                                <option value="Validation Completed">Validation Completed</option>
                                <option value="Checking with Publisher">Checking with Publisher</option>
                                <option value="Checking Completed">Checking Completed</option>
                                <option value="Matching">Matching</option>
                                <option value="Not Matching">Not Matching</option>
                                <option value="Negotiating">Negotiating</option>
                                <option value="SOW Sent">SOW Sent</option>
                                <option value="SOW Signed">SOW Signed</option>
                                <option value="Close">Close</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Priority</label>

                            <div class="col-sm-10">
                              <select class="form-control show-tick" id="priority" name="priority">
                                <option value="0">Select</option>
                                <option value="Hot">Hot</option>
                                <option value="High">High</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                                <option value="Regular">Regular</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Close Reason</label>

                            <div class="col-sm-10">
                              <select class="form-control show-tick" id="closereason" name="closereason">
                                <option value="0">Select</option>
                                <option value="SOW Signed and Resource Onboarded">SOW Signed &amp; Resource Onboarded</option>
                                <option value="Price Not acceptable by Publisher">Price Not acceptable by Publisher</option>
                                <option value="Price Not Acceptable by Checkout">Price Not Acceptable by Checkout</option>
                                <option value="Resource Quality Below Par">Resource Quality Below Par</option>
                                <option value="Resource Quality Above Par ">Resource Quality Above Par </option>
                                <option value="Other resource already onboarded">Other resource already onboarded</option>
                                <option value="Requirement put on hold">Requirement put on hold</option>
                                <option value="Waiting for Customer Response">Waiting for Customer Response</option>
                                <option value="Billing Terms not acceptable">Billing Terms not acceptable</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
                              <div class="col-sm-10">
                                <select class="form-control show-tick" id="final_status" name="final_status">
                                  <option>Select</option>
                                  <option value="Close">Close</option>
                                  <option value="In Process">In Process</option>
                                  <option value="Active">Active</option>
                                  <option value="Abandoned">Abandoned</option>

                                </select>
                              </div>
                          </div>
                          <div class="box-footer text-center">
                          <button type="submit" class="btn btn-info">Submit</button>
                          <a class="btn btn-danger" href="{{url()->previous()}}">Cancel</a>
                        </div>
                        </div>
                        <!-- /.box-body -->
                        
                      
                        <!-- /.box-footer -->
                      </form>
                  </ul>
                         
                </div>
              </div>
            </div>
      </div>
    </section>
@include('include.footer')

<script type="text/javascript">
$(document).ready(function()
{
  $('[data-toggle="popover"]').popover()
  $('.show-tick').selectpicker('refresh');

  @if(isset($data))
    $('#workstatus').val('{{$data->short_admin_status}}').prop('selected', true).change();
    $('#priority').val('{{$data->short_admin_priority}}').prop('selected', true).change();
    $('#closereason').val('{{$data->short_admin_close_reason}}').prop('selected', true).change();
    $('#final_status').val('{{$data->short_final_status}}').prop('selected', true).change();
  @endif

  $('#checkout_frm').submit(function(e) 
   {
      e.preventDefault();
      var formData = new FormData($(this)[0]);
       $.ajax({
           type: 'POST',
           cache: false,
           headers: 
           {
             'X-CSRF-TOKEN': $('#_token').val()
           },
           url:"{{url('movetocheckout')}}",
           data: formData,
           processData: false,
           contentType: false,
           dataType: 'json',
           success: function(data) 
           {
               if (data.type=='success') 
               {
                swal({
                    title: "Response",
                    text: data.msg,
                    type: data.type,
                 },function()
                 {
                    window.location.href="{{url('requirement/page/checkout_list/0')}}";
                 })
                  
               } 
               else 
               {
                 swal(
                 {
                   title:"Response",
                   text: data.msg,
                   type:data.type,
                 })
               }
           },
           error: function(request, status, error) 
           {
               json = $.parseJSON(request.responseText);
               //printErrorMsg(json.error);
               //$('.splerror').show();
               var str = "<ul style='color:red;'>";
               $.each(request.responseJSON, function(key, value) {
                   str += '<li>' + value + '</li>';
                  //$('input[name='+key+']').css({ "border": '#FF0000 1px solid'});
                  $('#'+key).css({ "border": '1px solid #e82626'});
               });
               str += '</ul>';
               swal({
                    title:"Required Fields",
                    html:true,
                    text:str,
                  
                  })
               //$(".splerror").html(str);

           }
          
       });
     })
}) 

</script>