@include('include.header')
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Requirement Checkout List 
                            </h2>
                            <ul class="header-dropdown m-r--5">
                              
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                           <div class="col-md-10">
                            <div class="row clearfix">
                              @include('include.requirement.requirement_search_blog')
                           </div> 
                        </div>

                        
                        
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="example1">
                                    <thead>
                                      <tr>
                                        <th>Sr No.</th>
                                        <th>Posted Company</th>
                                        <th>Posted User</th>
                                        <th>Checkout Company</th>
                                        <th>Checkout User</th>
                                        <th>Checkout Date</th>
                                        <th>Req No.</th>
                                        <th>Title</th>
                                        <th>Primary Skill</th>
                                        <th>Work Status</th>
                                        <th>Priority</th>
                                        <th>Close Reason</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                        
                                      </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                          
                        </div>
                       
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            
            <!-- #END# Exportable Table -->
        </div>
    </section>
@include('include.footer')
<script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript">
   var table;
   var rows_selected = [];

$(document).ready(function()
{
   $('.mobile-phone-number').inputmask('9999999999', { placeholder: '__________' });
   $('.show-tick').selectpicker();

   user_list();

   $('#user_form_search').submit(function(e) 
   {
      e.preventDefault();
        //var formData = new FormData($(this)[0]);
      user_list();
   })

   $('#req_export_btn').attr('href',"{{url('export_requirement/checkout')}}")
})
  

function user_list()
{

  var _token = $('input[name="_token"]').val();
  window.table=$('#example1').DataTable(
    {
        initComplete: function() 
        {
            var api = this.api();
            $('#myTable_filter input').off('.DT').on('input.DT', function() 
            {
                api.search(this.value).draw();
            });
        },
        oLanguage: 
        {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        bDestroy:true,
        lengthChange: true,
        targets: 0,
        searchable: false,
        orderable: false,
        
        ajax: {
            "url": "{{url('ajax_shortlist_requirement')}}",
            "type": "POST",
            "headers": 
            {
              'X-CSRF-TOKEN': _token
            },
            "data": function(d) 
            {
               var frm_data = $('#reqFrm').serializeArray();
               $.each(frm_data, function(key, val) 
               {
                 d[val.name] = val.value;
               });

               d['list_type']='shortlist';
               d['user']='admin';
               d['short_status']='1';
             },
            "error": function (xhr, error, code)
            {
                alert(error)
            }
        },
        'columnDefs': [],
         columns: [
            
            {
              render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            {
                "data": "company_name"
            },
            {
                "data": "posted_user"
            },
            {
                "data": "checkout_company"
            },
            {
                "data": "checkout_user"
            },
            {
                "data": "checkout_date"
            },
            {
                "data": "requirement_id"
            },
            {
                "data": "title"
            },
            {
                "data": "primary_skills"
            },
            {
                "data": "short_admin_status"
            },
            {
                "data": "short_admin_priority"
            },
            {
                "data": "short_admin_close_reason"
            },
            {
                "data": "short_final_status"
            },
            {
                "render": function(data, type, row, meta)
                {

                  var str= `<a href="{{url('checkout_details')}}/`+row.short_id+`"  class="btn btn-info btn-circle waves-effect waves-circle waves-float">
                              <i class="material-icons">remove_red_eye</i>
                            </a>`;
                  return str;        
                }
            },
        ]
       
        
    });

}
$(document).ready(function()
{
   
   $('#shortlist_frm').on('submit', function(e)
   {
      var arr=[];
      $.each(rows_selected, function(index, rowId)
      {
          arr.push(rowId)
          
      });
      if(arr.length==0)
      {
        
        swal({
                title: "Error",
                text:'Please Select Requirment',
                type:'error',
                
              })
        return false;
      }

      e.preventDefault();
      
      $.ajax({
          type: 'POST',
          cache: false,
          headers: 
          {
              'X-CSRF-TOKEN': $('#_token').val()
          },
          url:"{{url('movetocheckout')}}",
          data:JSON.stringify(arr),
          processData : false,
          contentType: false,
          dataType:'json',
          success: function(data)
          {
              swal({
                  title: "Response",
                  text: data,
                  type:'success',
               },function()
               {
                 window.location.href="{{url('checkoutrequirment')}}";
               })
              
          },
          error: function (request, status, error) 
          {
                json = $.parseJSON(request.responseText);
                 //printErrorMsg(json.error);
                $('.splerror').show();
                var str="<ul style='color:red;'>";
                $.each(request.responseJSON, function(key, value)
                {
                   str+='<li>'+value+'</li>';                        
                   
                });
                str+='</ul>';
                swal({
                      title:"Required Fields",
                      html:true,
                      text:str,
                      
                    })
                /*$(".splerror").html(str);*/
                
            }
      });
   });
  
})

</script>