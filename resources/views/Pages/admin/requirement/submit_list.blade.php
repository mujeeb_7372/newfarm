@include('include.header')
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Requirement Submite List 
                            </h2>
                            <ul class="header-dropdown m-r--5">
                              
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                           <div class="col-md-10">
                            <div class="row clearfix">
                              @include('include.requirement.requirement_search_blog')
                           </div> 
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="example1">
                                    @include('include.requirement.req_table_header')
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            
            <!-- #END# Exportable Table -->
        </div>
    </section>
@include('include.footer')

<script type="text/javascript">
$(document).ready(function()
{
  $('.mobile-phone-number').inputmask('9999999999', { placeholder: '__________' });
  $('.show-tick').selectpicker();
  user_list();
  $('#user_form_search').submit(function(e) 
  {
      e.preventDefault();
      //var formData = new FormData($(this)[0]);
      user_list();
  })
  $('#req_export_btn').attr('href',"{{url('export_requirement/submit')}}")
}) 
function user_list()
{

  var _token = $('input[name="_token"]').val();
  $('#example1').DataTable(
    {
        initComplete: function() 
        {
            var api = this.api();
            $('#myTable_filter input').off('.DT').on('input.DT', function() 
            {
                api.search(this.value).draw();
            });
        },
        oLanguage: 
        {
            sProcessing: "loading..."
        },
        order: [[ 0, "desc" ]],
        processing: true,
        serverSide: true,
        searching: false,
        bDestroy:true,
        lengthChange: false,
        ajax: {
            "url": "{{url('ajax_list_requirement')}}",
            "type": "POST",
            "headers": 
            {
              'X-CSRF-TOKEN': _token
            },
            "data": function(d) 
            {
               var frm_data = $('#user_form_search').serializeArray();
               $.each(frm_data, function(key, val) {
                 d[val.name] = val.value;
                 d['list_type']='submit';
                 d['user']='admin';
               });
               

             },
            "error": function (xhr, error, code)
            {
                alert(error)
            }
        },
        columns: [
            
            {
              render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            {
                "data": "requirement_id"
            },
            {
                "data": "title"
            },
            {
                "data": "requirement_type"
            },
            {
                "data": "primary_skills"
            },
            {
                "data": "work_location"
            },
            {
                "data": "duration"
            },
            {
                "data": "company_name",
                "render":function( data, type, row, meta ) 
                {
                  return data;
                }
            },
            {
                "data": "created"
            },
            {
                "render": function(data, type, row, meta)
                {
                     var str= `<div class="btn-group">
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">`;
                          
                          str+=`
                          <li><a href="{{url('requirement_change_status/publish')}}/`+row.id+`">Publish</a></li>
                          <li><a href="{{url('view_requirement_page')}}/`+row.id+`">View</a></li>
                          <li><a href="{{url('requirement/page/edit')}}/`+row.id+`">Edit</a></li>
                        </ul>
                      </div>`;
                      return str;
                }
            },
        ],
        
    });
}
</script>