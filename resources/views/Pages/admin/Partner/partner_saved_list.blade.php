@include('include.header')
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Saved Partner List 
                            </h2>
                            <ul class="header-dropdown m-r--5">
                              
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                           <div class="col-md-10">
                            <div class="row clearfix" style="padding: 0px 0px 0px 15px;">
                              <form id="user_form_search">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                               <div class="col-xs-3">
                                    <h2 class="card-inside-title">Name</h2>
                                    <div class="input-group date" >
                                      
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="u_name" id="u_name">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <h2 class="card-inside-title">Email</h2>
                                    <div class="input-group date" >
                                      <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="u_email" id="u_email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <h2 class="card-inside-title">Mobile</h2>
                                    <div class="input-group date" >
                                      <span class="input-group-addon">
                                                <i class="material-icons">phone_iphone</i>
                                            </span>
                                        <div class="form-line">
                                            
                                            <input type="text" id="u_phone" name="u_phone" class="form-control mobile-phone-number" placeholder="Ex: +00 (000) 000-00-00">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <h2 class="card-inside-title">Company</h2>
                                    <div class="input-group date" >
                                      <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                             <select class="form-control show-tick" id="comp_id" name="comp_id">
                                              <option value="">Select</option>
                                               @if(!empty($company))
                                                 @foreach($company as $row)
                                                  <option value="{{$row->company_id}}">{{$row->company_name}}</option>
                                                 @endforeach
                                               @endif
                                          </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-xs-12 text-center">
                                <div class="row clearfix">
                                    <div class="col-xs-12">
                                      <button type="submit" class="btn bg-purple waves-effect">
                                            <i class="material-icons">search</i>
                                            <span>SEARCH</span>
                                       </button>
                                       <a class="btn bg-indigo waves-effect" href="{{url('export_partner/save')}}">
                                          <i class="material-icons">import_export</i>
                                          <span>Export</span>
                                       </a>
                                       <a class="btn btn-success waves-effect" href="{{url('add_partner')}}">
                                            <i class="material-icons">person_add</i>
                                            <span>Add</span>
                                        </a>
                                    </div>  
                               </div>
                             </div>
                            </div>
                          </form>
                           </div> 
                        </div>

                        <div class="body">
                            <div class="table-responsive" style="height:500px;">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sr. No</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Company</th>
                                            <th>Contact</th>
                                            <th>Created at</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            
            <!-- #END# Exportable Table -->
        </div>
    </section>
@include('include.footer')

<script type="text/javascript">
$(document).ready(function()
{
  $('[data-toggle="tooltip"]').tooltip();
  $('.mobile-phone-number').inputmask('9999999999', { placeholder: '__________' });

   $('.show-tick').selectpicker();
    partner_list();

    $('#user_form_search').submit(function(e) 
     {
        e.preventDefault();
        //var formData = new FormData($(this)[0]);
        partner_list();
     })
}) 
function partner_list()
{

  var _token = $('input[name="_token"]').val();
  $('#example1').DataTable(
    {
        initComplete: function() 
        {
            var api = this.api();
            $('#myTable_filter input').off('.DT').on('input.DT', function() 
            {
                api.search(this.value).draw();
            });
        },
        oLanguage: 
        {
            sProcessing: "loading..."
        },
        order: [[0, "desc" ]],
        processing: true,
        serverSide: true,
        searching: false,
        bDestroy:true,
        lengthChange: false,
        ajax: {
            "url": "{{url('getPartner')}}",
            "type": "POST",
            "headers": 
            {
              'X-CSRF-TOKEN': _token
            },
            "data": function(d) 
            {
               var frm_data = $('#user_form_search').serializeArray();
               $.each(frm_data, function(key, val) {
                 d[val.name] = val.value;
                 d['isSaved']=1;
               });
               

             },
            "error": function (xhr, error, code)
            {
                alert(error)
            }
        },
        columns: [
            {
                render: function (data, type, row, meta) 
                {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                "data": "name"
            },
            {
                "data": "email"
            },
            {
                "data": "company_name"
            },
            {
                "data": "mobile"
            },
            {
                "data": "created_at"
            },
            {
                "data": "comp_isActive",
                "render":function( data, type, row, meta ) 
                {

                   if(data=='1')
                   {
                      return 'Active';
                   }
                   if(data=='0')
                   {
                      return 'Deactive';
                   }
                }
            },
            {
                "render": function ( data, type, row, meta ) 
                {
                  var strs='';
                  var btn='';
                   if(row.status=='1')
                   {
                      btn+='<a class="btn btn-danger btn-sm btn-flat" href="{{url("useraction/false")}}/'+row.id+'">Deactive It</a>';
                      
                   }
                   if(row.status=='0')
                   {
                      btn+='<a class="btn btn-success btn-sm btn-flat" href="{{url("useraction/true")}}/'+row.id+'">Active It</a>';

                      
                   }
                   
                   /*strs+='  <a class="btn btn-info btn-sm btn-flat" href="{{url("edit_partner_page")}}/'+row.company_id+'">Edit</a>';*/

                   strs+=`<div class="btn-group">
                                    <button type="button" class="btn bg-pink dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">build</i> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <!-- <li>`+btn+`</li> -->
                                        <li>
                                          <a href="{{url("edit_partner_page")}}/`+row.company_id+`" class=" waves-effect waves-block">Edit</a>
                                        </li>
                                        <li>
                                          <a href="{{url('partner_view_page')}}/`+row.company_id+`" class=" waves-effect waves-block">View</a>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                          <!-- <a href="{{url('partner_graph_page')}}/`+row.company_id+`" class=" waves-effect waves-block">Graph Report</a> -->
                                        </li>
                                        
                                    </ul>
                                </div>`;
                   return strs;
                }
            },
        ],
        
    });
}
</script>