@include('include.header')

<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
     @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Partner Details 
                            </h2>
                            
                        </div>
                        <div class="row">
                           
                          <div class="body">
                             <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion_10" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="headingOne_10">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseOne_10" aria-expanded="true" aria-controls="collapseOne_10">
                                                        Company Details
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_10" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_10">
                                                <div class="panel-body">
                                                    <table class="table table-condensed">
                                                       <thead>
                                                         <tr>
                                                           <th>Company Name</th>
                                                           <th>Business</th>
                                                           <th>Website</th>
                                                           <th>Contact Name</th>
                                                           <th>Contact Number</th>
                                                           <th>Email ID</th>
                                                         </tr>
                                                       </thead> 
                                                       <tbody>
                                                         <tr>
                                                           <td>@if(!empty($company_details)){{$company_details->company_name }}@endif</td>
                                                           <td>@if(!empty($company_details)){{$company_details->business }}@endif</td>
                                                           <td>@if(!empty($company_details)){{$company_details->website }}@endif</td>
                                                           <td>@if(!empty($company_details)){{$company_details->contact_name }}@endif</td>
                                                           <td>@if(!empty($company_details)){{$company_details->email }}@endif</td>
                                                         </tr>
                                                       </tbody>
                                                    </table>
                                                </div>
                                                <div class="panel-body">
                                                   <table class="table table-condensed">
                                                       <thead>
                                                         <tr>
                                                           <th>Address</th>
                                                           <th>City</th>
                                                           <th>State</th>
                                                           <th>Pin code</th>
                                                           <th>Country</th>
                                                         </tr>
                                                       </thead> 
                                                       <tbody>
                                                         @if(!empty($company_address))
                                                          @foreach($company_address as $row)
                                                           <tr>
                                                             <td>
                                                              <button type="button" class="btn btn-primary btn-block waves-effect" data-trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="" data-content="{{$row->cadd_address}}" data-original-title="Address">
                                                                 Show
                                                              </button>
                                                            </td>
                                                             <td>{{$row->cadd_city}}</td>
                                                             <td>{{$row->cadd_state}}</td>
                                                             <td>{{$row->cadd_zip}}</td>
                                                             <td>{{$row->cadd_country}}</td>
                                                           </tr>
                                                          @endforeach
                                                         @endif
                                                       </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="headingTwo_10">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseTwo_10" aria-expanded="false" aria-controls="collapseTwo_10">
                                                        Partner Creditionals
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_10">
                                                <div class="panel-body">
                                                    <table class="table table-condensed">
                                                       <thead>
                                                         <tr>
                                                           <th>Name</th>
                                                           <th>Email</th>
                                                           <th>Mobile</th>
                                                           
                                                         </tr>
                                                       </thead> 
                                                       <tbody>
                                                         @if(!empty($users))
                                                            <tr>
                                                              <td>{{$users->name}}</td>
                                                              <td>{{$users->email}}</td>
                                                              <td>{{$users->mobile}}</td>
                                                            </tr>
                                                         @endif
                                                       </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="headingThree_10">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseThree_10" aria-expanded="false" aria-controls="collapseThree_10">
                                                        Contact Person
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_10">
                                                <div class="panel-body">
                                                    <table class="table table-condensed">
                                                       <thead>
                                                         <tr>
                                                           <th>Contact person</th>
                                                           <th>Designation</th>
                                                           <th>Mobile</th>
                                                           <th>Email ID</th>
                                                           <th>Location</th>
                                                         </tr>
                                                       </thead> 
                                                       <tbody>
                                                         @if(!empty($company_contact))
                                                           @foreach($company_contact as $row)
                                                            <tr>
                                                              <td>{{$row->cc_name}}</td>
                                                              <td>{{$row->cc_designation}}</td>
                                                              <td>{{$row->cc_mobile}}</td>
                                                              <td>{{$row->cc_email}}</td>
                                                              <td>{{$row->cc_location}}</td>
                                                            </tr>
                                                           @endforeach
                                                         @endif
                                                       </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="headingThree_10">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapsefour_10" aria-expanded="false" aria-controls="collapsefour_10">
                                                        Financial Details
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_10">
                                                <div class="panel-body">
                                                    <table class="table table-condensed">
                                                       <thead>
                                                         <tr>
                                                           <th>PAN No.</th>
                                                           <th>Pan File</th>
                                                           <th>GST No.</th>
                                                           <th>GST File</th>
                                                           <th>Buisness Account No.</th>
                                                           <th>Bank Name</th>
                                                         </tr>
                                                       </thead> 
                                                       <tbody>
                                                         <tr>
                                                           <td>{{$company_details->pan}}</td>
                                                           <td>
                                                            @if($company_details->pan_image!='')
                                                           <img class="upld_img" style="width:40px; height:40px;" src="https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/Documents_files_pictogram_symbol_icon_folder.png" srcc="{{$company_details->pan_image}}">
                                                           @endif
                                                            </td>
                                                           <td>{{$company_details->gst}}</td>
                                                           <td>
                                                           <td>
                                                            @if($company_details->gst_image!='')
                                                           <img class="upld_img" style="width:40px; height:40px;" src="https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/Documents_files_pictogram_symbol_icon_folder.png" srcc="{{$company_details->gst_image}}">
                                                           @endif
                                                            </td>
                                                           <td>{{$company_details->business}}</td>
                                                           <td>{{$company_details->bank_name}}</td>
                                                         </tr>
                                                       </tbody>
                                                    </table>
                                                </div>
                                                <div class="panel-body">
                                                  <table class="table table-condensed">
                                                       <thead>
                                                         <tr>
                                                           <th>IFSC Code</th>
                                                           <th>Bank Address</th>
                                                           <th>Swift Code</th>
                                                           <th>IBAN Number</th>
                                                           <th>Cheque File</th>
                                                         </tr>
                                                       </thead> 
                                                       <tbody>
                                                        @if(!empty($company_details))
                                                         <tr>
                                                           <td>{{$company_details->ifsc_code}}</td>
                                                           <td>
                                                            
                                                            <button type="button" class="btn btn-primary btn-block waves-effect" data-trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="" data-content="{{$company_details->bank_address}}" data-original-title="Address">
                                                                 Show
                                                              </button>
                                                           </td>
                                                           <td>{{$company_details->swift_code}}</td>
                                                           <td>{{$company_details->iban_no}}</td>
                                                           <td>
                                                            @if($company_details->cheque_image!='')
                                                             <img class="upld_img" style="width:40px; height:40px;" src="https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/Documents_files_pictogram_symbol_icon_folder.png" srcc="{{$company_details->cheque_image}}">
                                                             @endif
                                                           </td>
                                                         </tr>
                                                         @endif
                                                       </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="headingThree_10">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapsefive_10" aria-expanded="false" aria-controls="collapsefive_10">
                                                        Technical Details
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefive_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_10">
                                                <div class="panel-body">
                                                    <table class="table table-condensed">
                                                       <thead>
                                                         <tr>
                                                           <th>Core Technology</th>
                                                           <th>Other Expertise</th>
                                                           <th>Technology</th>
                                                         </tr>
                                                       </thead> 
                                                       <tbody>
                                                         @if(!empty($company_tech))
                                                          @foreach($company_tech as $row)
                                                           <tr>
                                                             <td>{{$row->ctg_core}}</td>
                                                             <td>{{$row->ctg_other}}</td>
                                                             <td>{{$row->ctg_technolog}}</td>
                                                           </tr>
                                                          @endforeach
                                                         @endif
                                                       </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="headingThree_10">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapsesix_10" aria-expanded="false" aria-controls="collapsesix_10">
                                                        NDA
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsesix_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_10">
                                                <div class="panel-body">
                                                    <table class="table table-condensed">
                                                       <thead>
                                                         <tr>
                                                           <th>NDA Status</th>
                                                           <th>Date of NDA</th>
                                                           <th>End Date</th>
                                                           <th>NDA File</th>
                                                         </tr>
                                                       </thead> 
                                                       <tbody>
                                                         <tr>
                                                           <td>{{$company_details->nda_status}}</td>
                                                           <td>{{$company_details->date_of_nda}}</td>
                                                           <td>{{$company_details->date_of_nda_end}}</td>
                                                           <td>
                                                             @if($company_details->nda_image!='')
                                                             <img class="upld_img" style="width:40px; height:40px;" src="https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/Documents_files_pictogram_symbol_icon_folder.png" srcc="{{$company_details->nda_image}}">
                                                             @endif
                                                           </td>
                                                         </tr>
                                                       </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="headingThree_10">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseseven_10" aria-expanded="false" aria-controls="collapseseven_10">
                                                        Evulation
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseseven_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_10">
                                                <div class="panel-body">
                                                    <table class="table table-condensed">
                                                       <thead>
                                                         <tr>
                                                           <th>Category</th>
                                                           <th>Strength</th>
                                                           <th>Weakness</th>
                                                           <th>No. of Employee</th>
                                                           <th>Turn Over</th>
                                                         </tr>
                                                       </thead> 
                                                       <tbody>
                                                         <tr>
                                                           <td>{{$requriter_details->category}}</td>
                                                           <td>{{$requriter_details->strength}}</td>
                                                           <td>{{$requriter_details->weakness}}</td>
                                                           <td>{{$requriter_details->noemployee}}</td>
                                                           <td>{{$requriter_details->trunover}}</td>
                                                         </tr>
                                                       </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="text-center">
                                    <a href="{{url()->previous()}}" class="btn btn-danger">Close</a>    
                                  </div>
                                </div>
                                
                          </div>

                      </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            
            <!-- #END# Exportable Table -->
        </div>
     </div>   
    </section>
@include('include.footer')

<script type="text/javascript">
$(document).ready(function()
{
  $('[data-toggle="popover"]').popover();
}) 

</script>