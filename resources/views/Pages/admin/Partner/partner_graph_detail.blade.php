@include('include.header')

<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Partner Details 
                            </h2>
                            
                        </div>
                        <div class="row">
                           
                          <div class="body">
                             <canvas id="bar_chart" height="150"></canvas>

                                <div class="row">
                                  <div class="text-center">
                                    <a href="{{url()->previous()}}" class="btn btn-danger">Close</a>    
                                  </div>
                                </div>
                                
                          </div>

                      </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            
            <!-- #END# Exportable Table -->
        </div>
     </div>   
    </section>
@include('include.footer')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $('[data-toggle="popover"]').popover();
 
 set_chart('bar_chart',[{{$create_capacity->qty}},{{$create_requirement->qty}},{{$shortlist_capacity->qty}},{{$shortlist_requirement->qty}},{{$checkout_capacity->qty}},{{$checkout_requirement->qty}},100]);
}) 


function set_chart(element,qty)
{
   var ctx = $('#'+element).get(0).getContext('2d')

    var myChart = new Chart(ctx, 
    {
      type: 'bar',
      data: {
          labels: ['Capacity','Requirement','Short list Capacity','Short list Requirement','Checkout Capacity','Checkout Requirement'],
          datasets: [{
              label: 'Qty',

              data: qty,
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          }]
    },
    options: 
    {
      scales: 
      {
        yAxes: [{
          ticks: 
          {
            beginAtZero: true
          }
        }]
      }
    }
  });
  
}
</script>