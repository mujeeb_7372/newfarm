@include('include.header')
<style type="text/css">
.farm_custome_div
{
    width: 282px;
    margin: 0px 0px 0px 382px;
    border: 3px solid #DD4B39;
    padding: 10px;
}
</style>
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
         <div class="row clearfix">
           <div class="col-lg-12 d-flex ">
                    <div class="card">
                        <div class="header">
                            <h2>
                               @if($data!=NULL)
                                  Update Partner
                                @else
                                  Add Partner
                               @endif
                            </h2>
                           
                        </div>
                        <div class="body">
                          <div class="">
                            <form id="user_frm">
                              <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                              @if(!empty($data))
                              <input type="hidden" name="cid" id="cid" value="{{$data->company_id}}" 
                              >
                              @endif

                              <input type="hidden" name="isSaved" id="isSaved" value="false">

                                 <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion_17" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-col-pink">
                                            <div class="panel-heading" role="tab" id="headingOne_17">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseOne_17" aria-expanded="true" aria-controls="collapseOne_17">
                                                        <i class="material-icons">perm_contact_calendar</i> Company Details
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_17" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_17">
                                                <div class="panel-body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Company Name</label>
                                                                  <span style="color:red;">*
                                                                    <input type="text" name="comp_name" class="form-control" id="comp_name"  @if(!empty($data)) value="{{$data->company_name}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Business</label>
                                                                  <span style="color:red;">*
                                                                     <input type="text" name="comp_buisness" id="comp_buisness" class="form-control"  @if(!empty($data)) value="{{$data->business}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Website</label>
                                                                  <span style="color:red;">*
                                                                     <input type="text" name="comp_webiste" class="form-control" id="comp_webiste" @if(!empty($data)) value="{{$data->website}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Contact Name</label>

                                                                  <span style="color:red;">*
                                                                     <input type="text" class="form-control" id="comp_contact_name" name="comp_contact_name" @if(!empty($data)) value="{{$data->contact_name}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="password">Contact Number</label>
                                                                <span style="color:red;">*
                                                                  <input type="text" class="form-control mobile-phone-number"  name="comp_contact_number" id="comp_contact_number" @if(!empty($data)) value="{{$data->contact_number}}" @endif>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                                          <div class="form-group">
                                                              <div class="form-line">
                                                                <label for="password">Email ID</label>
                                                                <span style="color:red;">*
                                                                  <input type="email" class="form-control"  name="comp_contact_email" id="comp_contact_email" @if(!empty($data)) value="{{$data->email}}" @endif>
                                                              </div>
                                                          </div>
                                                      </div>
                                                    </div>
                                                   
                                                    <div class="row clearfix">
                                                      <div class="col-lg-12 col-md-12 col-sm-3 col-xs-6">
                                                       <table class="table table-bordered" id="dynamic_field1">  
                                                   @if(isset($comp_address[0]))
                                                      <?php $i=0; ?>
                                                       @foreach($comp_address as $val)
                                                         <tr id="row{{$i}}">  
                                                          <td>
                                                            <label for="exampleInputPassword1">Address</label>
                                                            <input type="text" id="cadd1" name="cadd1[]" placeholder="Enter Address"  class="form-control name_list" value="{{$val->cadd_address}}" />
                                                          </td>
                                                          <td>
                                                             <label for="exampleInputPassword1">City</label>
                                                            <input type="text" id="ccity1" name="ccity1[]" placeholder="Enter City Name"  class="form-control name_list" value="{{$val->cadd_city}}" />
                                                          </td> 
                                                          <td>
                                                            <label for="exampleInputPassword1">State</label>
                                                            <input type="text" id="cstate1" name="cstate1[]" placeholder="Enter State Name"  class="form-control name_list" value="{{$val->cadd_state}}" />
                                                          </td> 
                                                          <td>
                                                            <label for="exampleInputPassword1">Pin code</label>
                                                            <input type="text" id="czip1" name="czip1[]" placeholder="Enter Zip Code"  class="form-control name_list" value="{{$val->cadd_zip}}" />
                                                          </td>
                                                          <td>
                                                            <label for="exampleInputPassword1">Country</label>
                                                            <input type="text" id="ccountry1" name="ccountry1[]" placeholder="Enter Country Name"  class="form-control name_list" value="{{$val->cadd_country}}" />
                                                          </td> 
                                                        @if($i==0)  
                                                          <td>
                                                            <label for="exampleInputPassword1"></label>
                                                            <button type="button" name="add" id="add1" class="btn btn-success">+</button></td>  
                                                        </tr>
                                                         @else
                                                         <td>
                                                           <label for="exampleInputPassword1"></label>
                                                            <button type="button" name="remove" id="<?php echo $i;?>" class="btn btn-danger btn_remove1">X</button></td>
                                                         </td> 
                                                        @endif
                                                        <?php $i++;?>
                                                       @endforeach
                                                     @else
                                                      <tr>  
                                                      <td>
                                                        <label for="exampleInputPassword1">Address</label>
                                                        <input type="text" d1" name="cadd1[]" placeholder="Enter Address"  class="form-control name_list" />
                                                      </td>
                                                      <td>
                                                         <label for="exampleInputPassword1">City</label>
                                                        <input type="text" ty1" name="ccity1[]" placeholder="Enter City Name"  class="form-control name_list" />
                                                      </td> 
                                                      <td>
                                                        <label for="exampleInputPassword1">State</label>
                                                        <input type="text" ate1" name="cstate1[]" placeholder="Enter State Name"  class="form-control name_list" />
                                                      </td> 
                                                      <td>
                                                        <label for="exampleInputPassword1">Pin code</label>
                                                        <input type="text" p1" name="czip1[]" placeholder="Enter Zip Code"  class="form-control name_list" />
                                                      </td>
                                                      <td>
                                                        <label for="exampleInputPassword1">Country</label>
                                                        <input type="text" id="ccountry1" name="ccountry1[]" placeholder="Enter Country Name"  class="form-control name_list" />
                                                      </td> 
                                                      <td>
                                                        <label for="exampleInputPassword1"></label>
                                                        <button type="button" name="add" id="add1" class="btn btn-success">+</button></td>  
                                                    </tr>
                                                   @endif 
                                                      
                                                   </table> 
                                                    </div>  
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="headingTwo_17">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseTwo_17" aria-expanded="false" aria-controls="collapseTwo_17">
                                                        <i class="material-icons">cloud_download</i> Partner Credentials
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_17">
                                                <div class="panel-body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Name</label>
                                                                   <span style="color:red;">*
                                                                     <input type="text" name="usr_name" class="form-control" id="usr_name"  @if(!empty($user_detail)) value="{{$user_detail->name}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Email</label>
                                                                  <span style="color:red;">*
                                                                     <input name="usr_email" type="email" class="form-control" id="usr_email" @if(!empty($user_detail)) value="{{$user_detail->email}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Mobile</label>
                                                                  <span style="color:red;">*
                                                                     <input id="usr_mobile" name="usr_mobile" type="text" class="form-control mobile-phone-number"  @if(!empty($user_detail)) value="{{$user_detail->mobile}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Designation</label>
                                                                  <span style="color:red;">*
                                                                     <input type="text" class="form-control" name="usr_designation" id="usr_designation" @if(!empty($reqruiter)) value="{{$reqruiter->designation}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Password</label>
                                                                  <span style="color:red;">*
                                                                     <input type="password" class="form-control" name="usr_pwd" id="usr_pwd"  @if(!empty($user_detail)) value="{{$user_detail->pwd_text}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Confirm Password</label>
                                                                  <span style="color:red;">*
                                                                     <input type="password" class="form-control" name="usr_conf_pwd" id="usr_conf_pwd"  @if(!empty($user_detail)) value="{{$user_detail->pwd_text}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-teal">
                                            <div class="panel-heading" role="tab" id="headingThree_17">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseThree_17" aria-expanded="false" aria-controls="collapseThree_17">
                                                        <i class="material-icons">contact_phone</i> Contact Person Info
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_17">
                                                <div class="panel-body">
                                                    <div class="row clearfix">
                                                      <div class="col-lg-12 col-md-12 col-sm-3 col-xs-6">
                                                        <table class="table table-bordered" id="dynamic_field2"> 
                                             @if(isset($comp_contact[0]))
                                                <?php $io=0;?>
                                                  @foreach($comp_contact as $val)
                                                    <tr id="row{{$io}}">  
                                                    <td>
                                                      <label for="exampleInputPassword1">Contact person</label>
                                                      <input type="text" id="contact_name_1" name="ccontname1[]" placeholder="Contact person" value="{{$val->cc_name}}" class="form-control name_list" />
                                                    </td>
                                                    
                                                    <td>
                                                      <label for="exampleInputPassword1">Designation</label>
                                                      <input type="text" id="designation_1" name="ccontdesig1[]" placeholder="Enter Designation" value="{{$val->cc_designation}}"  class="form-control name_list" />
                                                    </td> 
                                                    <td>
                                                      <label for="exampleInputPassword1">Mobile</label>
                                                      <input type="text" id="mobile_1" value="{{$val->cc_mobile}}" name="ccontmob1[]" placeholder="Enter Mobile"  class="form-control name_list" />
                                                    </td>
                                                    <td>
                                                      <label for="exampleInputPassword1">Email ID</label>
                                                      <input type="text" id="ccontemail1" name="ccontemail1[]" placeholder="Enter Email ID" value="{{$val->cc_email}}"  class="form-control name_list" />
                                                    </td>
                                                    <td>
                                                      <label for="exampleInputPassword1">Location</label>
                                                      <input type="text"  id="ccontloc1" value="{{$val->cc_location}}" name="ccontloc1[]" placeholder="Enter Location"  class="form-control name_list" />
                                                    </td> 
                                                    @if($io==0)
                                                        <td>
                                                          <label for="exampleInputPassword1"></label>
                                                          <button type="button" name="add" id="add2" class="btn btn-success">+</button>
                                                        </td>  
                                                      @else
                                                      <td><label for="exampleInputPassword1"></label>
                                                        <button type="button" name="remove" id="{{$io}}" class="btn btn-danger btn_remove2">X</button>
                                                      </td>

                                                    @endif
                                                    
                                                  </tr>  
                                                  <?php $io++;?>
                                                  @endforeach
                                               @else
                                                  <tr>  
                                                  <td>
                                                    <label for="exampleInputPassword1">Contact person</label>
                                                    <input type="text" id="contact_name_1" name="ccontname1[]" placeholder="Contact person"  class="form-control name_list" />
                                                  </td>
                                                  <td>
                                                    <label for="exampleInputPassword1">Designation</label>
                                                    <input type="text" id="designation_1" name="ccontdesig1[]" placeholder="Enter Designation"  class="form-control name_list" />
                                                  </td> 
                                                  <td>
                                                    <label for="exampleInputPassword1">Mobile</label>
                                                    <input type="text" id="mobile_1" name="ccontmob1[]" placeholder="Enter Mobile"  class="form-control name_list" />
                                                  </td>
                                                  <td>
                                                    <label for="exampleInputPassword1">Email ID</label>
                                                    <input type="text" id="ccontemail1" name="ccontemail1[]" placeholder="Enter Email ID"  class="form-control name_list" />
                                                  </td>
                                                  <td>
                                                    <label for="exampleInputPassword1">Location</label>
                                                    <input type="text" id="ccontloc1" name="ccontloc1[]" placeholder="Enter Location"  class="form-control name_list" />
                                                  </td> 
                                                  <td>
                                                    <label for="exampleInputPassword1"></label>
                                                    <button type="button" name="add" id="add2" class="btn btn-success">+</button></td>  
                                                </tr>  
                                             @endif 
                                              
                                             </table>
                                                      </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-orange">
                                            <div class="panel-heading" role="tab" id="headingFour_17">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseFour_17" aria-expanded="false" aria-controls="collapseFour_17">
                                                        <i class="material-icons">folder_shared</i> Financial Details
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseFour_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_17">
                                                <div class="panel-body">
                                                   <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">PAN No.</label>
                                                                  
                                                                    <input type="text" class="form-control" name="comp_pan_no" id="comp_pan_no" @if(!empty($data)) value="{{$data->pan}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Pan File (<small>PDF/Photo</small>)</label>
                                                                     <input type="file" class="form-control-file" name="comp_pan_file" id="comp_pan_file" accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps">
                                                                     @if(!empty($data)) @if($data->pan_image!='') 
                                                                       <img class="upld_img" style="width:40px; height:40px;" src="https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/Documents_files_pictogram_symbol_icon_folder.png" srcc="{{url($data->pan_image)}}"> @endif @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">GST No.</label>
                                                                     <input type="text" class="form-control" name="comp_gst_no" id="comp_gst_no" @if(!empty($data)) value="{{$data->gst}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">GST File (<small>PDF/Photo</small>)</label>
                                                                  
                                                                    <input type="file" class="form-control-file" id="comp_gst_file" name="comp_gst_file" accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps">

                                                                    @if(!empty($data)) @if($data->gst_image!='') 
                                                                       <img class="upld_img" style="width:40px; height:40px;" src="https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/Documents_files_pictogram_symbol_icon_folder.png" srcc="{{url($data->gst_image)}}"> @endif @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Buisness Account No.</label>
                                                                  <span style="color:red;">*
                                                                     <input type="text" class="form-control" name="comp_acc_no" id="comp_acc_no" @if(!empty($data)) value="{{$data->bank_account_no}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Bank Name</label>
                                                                  <span style="color:red;">*
                                                                     <input type="text" class="form-control" name="comp_bank_name" id="comp_bank_name" @if(!empty($data)) value="{{$data->bank_name}}" @endif >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">IFSC Code</label>
                                                                  <span style="color:red;">*
                                                                  
                                                                    <input type="text" class="form-control" id="comp_ifsc_code" name="comp_ifsc_code" @if(!empty($data)) value="{{$data->ifsc_code}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Bank Address</label>
                                                                  <span style="color:red;">*
                                                                     <input type="text" class="form-control" name="comp_bank_add" id="comp_bank_add"  @if(!empty($data)) value="{{$data->bank_address}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Swift Code</label>
                                                                     <input type="text" class="form-control" name="comp_swift_code" id="comp_swift_code" @if(!empty($data)) value="{{$data->swift_code}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">IBAN Number</label>
                                                                  <span style="color:red;">*
                                                                    <input type="text" class="form-control" name="comp_iban_no" id="comp_iban_no" @if(!empty($data)) value="{{$data->iban_no}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                  <label for="password">Cheque File(<small>PDF/Photo</small>)</label>
                                                                     <input type="file" class="form-control-file" id="comp_cheque_file" name="comp_cheque_file" accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps">
                                                                     @if(!empty($data)) @if($data->cheque_image!='') 
                                                                       <img class="upld_img" style="width:40px; height:40px;" src="https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/Documents_files_pictogram_symbol_icon_folder.png" srcc="{{url($data->cheque_image)}}"> @endif @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-pink">
                                            <div class="panel-heading" role="tab" id="headingFour_17">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseFour_18" aria-expanded="false" aria-controls="collapseFour_18">
                                                        <i class="material-icons">folder_shared</i> Technical Details
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseFour_18" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_17">
                                                <div class="panel-body">
                                                   <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-3 col-sm-3 col-xs-6">
                                                          <table class="table table-bordered" id="dynamic_field3"> 
                                                         @if(isset($comp_tech[0]))
                                                           <?php $ii=0;?>
                                                          @foreach($comp_tech as $val)
                                                            <tr id="row{{$ii}}">  
                                                              <td>
                                                                <label for="exampleInputPassword1">Core Technology</label>
                                                                <input type="text" id="ccoretech" name="ccoretech[]" value="{{$val->ctg_core}}" class="form-control name_list" />
                                                              </td>
                                                              <td>
                                                                 <label for="exampleInputPassword1">Other Expertise</label>
                                                                <input type="text" id="cotherexper" name="cotherexper[]"  value="{{$val->ctg_other}}"  class="form-control name_list" />
                                                              </td> 
                                                              <td>
                                                                <label for="exampleInputPassword1">Technology</label>
                                                                <input type="text"  id="ctechology1" name="ctechology1[]" value="{{$val->ctg_technolog}}" class="form-control name_list" />
                                                              </td> 
                                                              @if($ii==0)
                                                                <td>
                                                                <label for="exampleInputPassword1"></label>
                                                                <button type="button" name="add" id="add3" class="btn btn-success">+</button>
                                                              </td>  
                                                               @else
                                                                 <td>
                                                                  <label for="exampleInputPassword1"></label>
                                                                  <button type="button" name="remove" id="{{$ii}}" class="btn btn-danger btn_remove3">X</button></td>
                                                              @endif
                                                            </tr>
                                                            <?php $ii++;?>
                                                          @endforeach
                                                         @else
                                                            <tr>  
                                                            <td>
                                                              <label for="exampleInputPassword1">Core Technology</label>
                                                              <input type="text" id="ccoretech" name="ccoretech[]"  class="form-control name_list" />
                                                            </td>
                                                            <td>
                                                               <label for="exampleInputPassword1">Other Expertise</label>
                                                              <input type="text" id="cotherexper" name="cotherexper[]"   class="form-control name_list" />
                                                            </td> 
                                                            <td>
                                                              <label for="exampleInputPassword1">Technology</label>
                                                              <input type="text" id="ctechology1" name="ctechology1[]"  class="form-control name_list" />
                                                            </td> 
                                                            <td>
                                                              <label for="exampleInputPassword1"></label>
                                                              <button type="button" name="add" id="add3" class="btn btn-success">+</button>
                                                            </td>  
                                                          </tr>
                                                         @endif
                                                            
                                                         </table>
                                                        </div>
                                                        
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-blue">
                                            <div class="panel-heading" role="tab" id="headingFour_17">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseFour_19" aria-expanded="false" aria-controls="collapseFour_19">
                                                        <i class="material-icons">folder_shared</i> NDA
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseFour_19" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_17">
                                                <div class="panel-body">
                                                   <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-3 col-sm-3 col-xs-6">
                                                           <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="password">NDA Status</label>
                                                                      <span style="color:red;">*
                                                                        <select class="form-control show-tick" name="comp_nda_status" id="comp_nda_status">
                                                                            <option value="">-- Please select --</option>
                                                                            <option value="NDA Draft Shared Partner">NDA Draft Shared Partner</option>
                                                                            <option value="Received Partner Signed Copy">Received Partner Signed Copy</option>
                                                                            <option value="Sent Digitohub Signed Copy">Sent Digitohub Signed Copy</option>
                                                                            <option>NDA Fully Signed</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="password">Date of NDA</label>
                                                                        <input type="text" class="datepicker form-control" name="comp_date_nda" id="comp_date_nda
                                                                        " >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="password">End Date</label>
                                                                         <input type="text" class="datepicker form-control" name="comp_end_nda_date" id="comp_end_nda_date" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="password">NDA File</label>
                                                                         <input type="file" class="form-control-file" id="comp_nda_file" name="comp_nda_file" accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps">
                                                                         @if(!empty($data)) @if($data->nda_image!='') 
                                                                       <img class="upld_img" style="width:40px; height:40px;" src="https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/Documents_files_pictogram_symbol_icon_folder.png" srcc="{{url($data->nda_image)}}"> @endif @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-orange">
                                            <div class="panel-heading" role="tab" id="headingFour_17">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseFour_20" aria-expanded="false" aria-controls="collapseFour_20">
                                                        <i class="material-icons">folder_shared</i> 
                                                        Evulation
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseFour_20" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_17">
                                                <div class="panel-body">
                                                   <div class="row clearfix">
                                                      <div class="col-lg-12 col-md-3 col-sm-3 col-xs-6">
                                                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                              <div class="form-group">
                                                                  <div class="form-line">
                                                                    <label for="password">Category</label>
                                                                        <select class="form-control show-tick" id="comp_ev_categeory" name="comp_ev_categeory">
                                                                            <option value="">-- Please select --</option>
                                                                            <option value="Buy">Buy</option>
                                                                            <option value="Sell">Sell</option>
                                                                            <option value="Both">Both</option>
                                                                            <option value="Dont know">Dont know</option>
                                                                        </select>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="password">Strength</label>
                                                                          <input type="text" class="form-control" name="comp_ev_strength" id="comp_ev_strength" @if(!empty($reqruiter)) value="{{$reqruiter->category}}" @endif>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="password">Weakness</label>
                                                                          <input type="text" class="form-control" id="comp_ev_weakness" name="comp_ev_weakness" @if(!empty($reqruiter)) value="{{$reqruiter->weakness}}" @endif>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="password">No. of Employee</label>
                                                                          <input type="number" class="form-control" name="comp_ev_employee" id="comp_ev_employee" @if(!empty($reqruiter)) value="{{$reqruiter->noemployee}}" @endif>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                      <label for="password">Turn Over</label>
                                                                          <input type="number" class="form-control" name="comp_ev_trunover" id="comp_ev_trunover" @if(!empty($reqruiter)) value="{{$reqruiter->trunover}}" @endif>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="text-center">
                                  <button type="button" id="company_save"  class="btn btn-primary m-t-15 waves-effect">Save</button>
                                  <button type="submit" class="btn btn-success m-t-15 waves-effect">Submit</button>
                                  <a href="{{url()->previous() }}" class="btn btn-danger m-t-15 waves-effect">Cancel</a>
                                </div>
                            </form>
                          </div>
                        </div>
                    </div>
                </div>
         </div>
       </div> 
    </section>

@include('include.footer')

<script type="text/javascript">
  @if(!empty($data))
    $('#comp_nda_status').val('{{$data->nda_status}}')
    $('#comp_ev_categeory').val('{{$reqruiter->category}}')
  @endif
$(document).ready(function()
{

   var i=1;
   var q=1;
   var r=1;

      $('#add1').click(function(){  
           i++;  
            if(i>=10)
            {
              
              return false;
            }
           $('#dynamic_field1').append(`<tr id="row`+i+`">  
              <td>
                <label for="exampleInputPassword1"></label>
                <input type="text" id="cadd1" name="cadd1[]" placeholder="Enter Address"  class="form-control name_list" />
              </td>
              <td>
                 <label for="exampleInputPassword1"></label>
                <input type="text" id="ccity1" name="ccity1[]" placeholder="Enter City Name"  class="form-control name_list" />
              </td> 
              <td>
                <label for="exampleInputPassword1"></label>
                <input type="text" id="cstate1" name="cstate1[]" placeholder="Enter State Name"  class="form-control name_list" />
              </td> 
              <td>
                <label for="exampleInputPassword1"></label>
                <input type="text" id="czip1" name="czip1[]" placeholder="Enter Zip Code"  class="form-control name_list" />
              </td>
              <td>
                <label for="exampleInputPassword1"></label>
                <input type="text" id="ccountry1" name="ccountry1[]" placeholder="Enter Country Name"  class="form-control name_list" />
              </td> 
              <td>
                <label for="exampleInputPassword1"></label>
                <button type="button" name="remove" id="`+i+`" class="btn btn-danger btn_remove1">X</button></td>
            </tr>`);
      });  
      $(document).on('click', '.btn_remove1', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });
   /*=====================================*/
   $('#add2').click(function(){  
           q++;  
           if(q>=12)
            {
              return false;
            }
           $('#dynamic_field2').append(` 
              <tr id="row`+q+`">  
                <td>
                  <label for="exampleInputPassword1">Contact person</label>
                  <input type="text" id="contact_name_1" name="ccontname1[]" placeholder="Contact Person"  class="form-control name_list" />
                </td>
                <td>
                  <label for="exampleInputPassword1"></label>
                  <input type="text" id="designation_1" name="ccontdesig1[]" placeholder="Enter Designation"  class="form-control name_list" />
                </td> 
                <td>
                  <label for="exampleInputPassword1"></label>
                  <input type="text" id="mobile_1" name="ccontmob1[]" placeholder="Enter Mobile"  class="form-control name_list mobile-phone-number" />
                </td>
                <td>
                  <label for="exampleInputPassword1"></label>
                  <input type="text" id="ccontemail1" name="ccontemail1[]" placeholder="Enter Email ID"  class="form-control name_list" />
                </td>
                <td>
                  <label for="exampleInputPassword1"></label>
                  <input type="text" id="ccontloc1" name="ccontloc1[]" placeholder="Enter Location"  class="form-control name_list" />
                </td> 
                <td>
                  <label for="exampleInputPassword1"></label>
                  <button type="button" name="remove" id="`+q+`" class="btn btn-danger btn_remove2">X</button></td>
              </tr>  
             `);  
      });  
      $(document).on('click', '.btn_remove2', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });

      /*************************************************/
  /*===================================*/
      $('#add3').click(function(){  
           r++;  
           if(r>=12)
            {
              return false;
            }
           $('#dynamic_field3').append(`<tr id="row`+r+`">  
              <td>
                <label for="exampleInputPassword1"></label>
                <input type="text" id="ccoretech" name="ccoretech[]"  class="form-control name_list" />
              </td>
              <td>
                 <label for="exampleInputPassword1"></label>
                <input type="text" id="cotherexper" name="cotherexper[]"   class="form-control name_list" />
              </td> 
              <td>
                <label for="exampleInputPassword1"></label>
                <input type="text" id="ctechology1" name="ctechology1[]"  class="form-control name_list" />
              </td> 
              <td>
                <label for="exampleInputPassword1"></label>
                <button type="button" name="remove" id="`+r+`" class="btn btn-danger btn_remove2">X</button></td> 
            </tr>`);  
      });  
      $(document).on('click', '.btn_remove3', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });
      /*==========================================================*/
  $('.mobile-phone-number').inputmask('9999999999', { placeholder: '__________' });
   $('.show-tick').selectpicker();
    
    $('#user_frm').submit(function(e) 
     {
       $("#accordion_17").loading();

        e.preventDefault();
        var url="";
        var formData = new FormData($(this)[0]);
        
        var url;
         @if(!empty($data))
            url="{{url('ajax_update_company')}}"
          @else
             url="{{url('ajax_save_company')}}"
          @endif
         $.ajax({
           type: 'POST',
           cache: false,
           headers: 
           {
             'X-CSRF-TOKEN': $('#_token').val()
           },
           url: url,
           data:formData,
           processData : false,
           contentType: false,
           dataType:'json',
           success: function(data)
           {
             $('#accordion_17').loading('stop');
               if(data.type=='success')
               {
                    swal({
                        title:"Response",
                        text:data.msg,
                        type:data.type,
                        imageUrl: "https://gurayyarar.github.io/AdminBSBMaterialDesign/images/thumbs-up.padding",
                        showCancelButton:false,
                         showConfirmButton: true
                     },function()
                     {
                       if(data.isSaved=='true')
                       {
                         window.location.href="{{url('saved_partner')}}";
                       }
                       if(data.isSaved=='false')
                       {
                         window.location.href="{{url('partner')}}";
                       }
                       
                     })
               }
               else
               {
                 $('#accordion_17').loading('stop');
                 var str="<ul style='color:red;'>";
                 $.each(data, function(key, value)
                 {
                    if(key!='type')
                    {
                      $('#'+key).css({ "border": '#FF0000 1px solid'});
                      str+='<li>'+value+'</li>';                        
                    }
                    
                 });
                 str+='</ul>';

                 swal({
                 title:"The following fields were filled incorrectly",
                  html:true,
                  type:'error',
                  text:str,
                 })
               }
           },
           error: function (request, status, error) 
           {
              $('#accordion_17').loading('stop');
                 json = $.parseJSON(request.responseText);
                  //printErrorMsg(json.error);
                 //$('.splerror').show();
                 var str="<ul style='color:red;'>";
                 $.each(request.responseJSON, function(key, value)
                 {
                    if(key!='type')
                    {
                      $('#'+key).css({ "border": '#FF0000 1px solid'});
                      str+='<li>'+value+'</li>';                        
                    }                
                 });
                 str+='</ul>';
                 
                 swal({
                 title:"The following fields were filled incorrectly",
                  html:true,
                  type:'error',
                  text:str,
                 })
                 //$(".splerror").html(str);
                 
             }
       });
        
     })
   $('#company_save').click(function()
   {
    $('#isSaved').val('true');
    $('#user_frm').submit();
   }) 
}) 

</script>