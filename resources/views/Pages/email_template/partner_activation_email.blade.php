    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<style type="text/css">
    .card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #e9ecef;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
.btn-info {
    color: #fff;
    background-color: #17a2b8;
    border-color: #17a2b8;
}
.btn {
    display: inline-block;
    font-weight: 400;
    color: #212529;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.btn-primary {
    color: #fff;
    background-color: #007bff;
    border-color: #007bff;
}
p {
    margin-top: 0;
    margin-bottom: 1rem;
}
.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
}
</style>
</head>

<body style="margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tr>
            <td style="padding: 10px 0 30px 0;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                    <tr>
                        <td align="center" bgcolor="black" style="padding: 17px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                            <img src="https://digitohub.com/img/logo.png" alt="Creating Email Magic" width="250" height="84" style="display: block;" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    
                                    <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px; text-align: center;">
                                        <p style="text-align: left; font-size: 17px;">Dear {{$data->name}},</p>
                                        
                                        <p style="text-align: center; font-size: 17px;">Welcome to DigitoHub's <b><u>FARM</u></b>.</p>
                                        <p style="text-align: left; font-size: 17px;">A Flexible & Agile Resource Model for all your Requirements & Capacities, Powered By out 300+ Partner Newtwork</p>
                                        <b>Flexible & Agile Resource Model (FARM)</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                        Digitohub FARM - <b>CAPACITY</b> provides access to Verified talent available within our partner network for on time ramp up for your engagements. 
                                        <img src="http://nanoelectronics.in/digitohub/img/products/digitohub.jpg" alt="" style="width: 250px; height: 200px; margin-left: 156px; display: block;" />
                                        Digitohub FARM - <b>REQUIREMENTS</b> helps to publish your resource requirements to be immediately serviced through our partner network across length & breadth of INDIA.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="card" style="width:600px; text-align: center;">
                                        
                                        <div class="card-body">
                                         @if($status=='1')
                                          <p style="text-align: center; margin-left: 2px;" >Congratulations!! Your Farm account has been activated.</p>
                                           @else
                                             <p style="text-align: center; margin-left: 2px;" >Your Farm account has been De-activated.</p>
                                          @endif
                                          <p class="btn btn-info" style="margin-left: -62px;">Email : {{$data->email}}</p><br>
                                          <p style="margin-left: 9px;">Please Clcik on below button to reset your pre-activated password</p>
                                          <a href="{{url('partner_reset_page/'.base64_encode($data->recruiter_id).'/'.base64_encode($data->email))}}" class="btn btn-primary" style="color:white; margin-left:-28px;">Click</a>
                                        </div>
                                      </div>
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
                                        © 2020 DigitoHub. All Rights Reserved.
                                    
                                    </td>
                                    <td align="right" width="25%">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
                                                    <a href="http://www.twitter.com/" style="color: #ffffff;">
                                                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/tw.gif" alt="Twitter" width="38" height="38" style="display: block;" border="0" />
                                                    </a>
                                                </td>
                                                <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                                <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
                                                    <a href="http://www.twitter.com/" style="color: #ffffff;">
                                                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/fb.gif" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>