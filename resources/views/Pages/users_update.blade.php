@include('include.header')
<style type="text/css">
.farm_custome_div
{
    width: 282px;
    margin: 0px 0px 0px 382px;
    border: 3px solid #DD4B39;
    padding: 10px;
}
</style>
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
    @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
   
        <div class="container-fluid">
         <div class="row clearfix">
           <div class="col-lg-12 d-flex ">
                    <div class="card">
                        <div class="header">
                            <h2>
                               @if($data!=NULL)
                                  Update User
                                @else
                                  Add User
                               @endif
                            </h2>
                           
                        </div>
                        <div class="body">
                          <div class="farm_custome_div">
                            <form id="user_frm">
                              <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                              <input type="hidden" name="isEdit" value="@if($data!=NULL) {{$data->id}} @else false @endif" >
                                <label for="password">Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" required id="u_name" name="u_name" class="form-control" @if($data!=NULL)value="{{$data->name}}"  @endif>
                                    </div>
                                </div>
                                
                                <label for="password">Email</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" required id="u_email" name="u_email" class="form-control"@if($data!=NULL)value="{{$data->email}}"  @endif>
                                    </div>
                                </div>
                                <label for="password">Password</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" required id="u_pwd" name="u_pwd" class="form-control" @if($data!=NULL)value="{{$data->pwd_text}}"  @endif>
                                    </div>
                                </div>
                                <label for="password">Confirm Passwwrd</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" required id="u_conf_pwd" name="u_conf_pwd" class="form-control" @if($data!=NULL)value="{{$data->pwd_text}}"  @endif>
                                    </div>
                                </div>
                                <label for="password">Mobile</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" required id="u_mobile" name="u_mobile" class="form-control mobile-phone-number" @if($data!=NULL)value="{{$data->mobile}}"  @endif>
                                    </div>
                                </div>
                                <label for="email_address">Status</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" id="u_status" name="u_status" required>
                                            <option value="">Select</option>
                                            <option value="1">Active</option>
                                            <option value="0">Deactive</option>
                                        </select>
                                    </div>
                                </div>

                                <br>
                                <div class="text-center">
                                  <button type="submit" class="btn btn-primary m-t-15 waves-effect">@if($data!="")Update @else Save @endif</button>
                                  <a href="{{ url()->previous()}}" class="btn btn-danger m-t-15 waves-effect">Cancel</a>
                                </div>
                                
                            </form>
                          </div>
                        </div>
                    </div>
                </div>
         </div>
       </div> 
    </section>
@include('include.footer')

<script type="text/javascript">
$(document).ready(function()
{
   @if($data!=NULL)
   $('#u_status').val('{{$data->status}}')
   $('#u_status').selectpicker('refresh')
   @endif
  $('.mobile-phone-number').inputmask('9999999999', { placeholder: '__________' });
   $('.show-tick').selectpicker();
    
    $('#user_frm').submit(function(e) 
     {
        $(".farm_custome_div").loading();

        e.preventDefault();
        var url="";
        var formData = new FormData($(this)[0]);
        url="{{url('ajax_save_user')}}"
         $.ajax({
           type: 'POST',
           cache: false,
           headers: 
           {
             'X-CSRF-TOKEN': $('#_token').val()
           },
           url: url,
           data:formData,
           processData : false,
           contentType: false,
           dataType:'json',
           success: function(data)
           {
             $(".farm_custome_div").loading('stop');
               if(data.type=='success')
               {
                    swal({
                        title:"Response",
                        text:data.msg,
                        type:data.type,
                        imageUrl: "https://gurayyarar.github.io/AdminBSBMaterialDesign/images/thumbs-up.padding",
                        showCancelButton:false,
                         showConfirmButton: true
                     },function()
                     {
                       window.location.href="{{url('users')}}";
                     })
               }
               else
               {
                
                  $(".farm_custome_div").loading('stop');
                 var str="<ul style='color:red;'>";
                 $.each(data, function(key, value)
                 {
                    if(key!='type')
                    {
                      $('#'+key).css({ "border": '#FF0000 1px solid'});
                      str+='<li>'+value+'</li>';                        
                    }
                    
                 });
                 str+='</ul>';

                 swal({
                 title:"The following fields were filled incorrectly",
                  html:true,
                  type:'error',
                  text:str,
                 })
               }
           },
           error: function (request, status, error) 
           {
            $(".farm_custome_div").loading('stop');
                 json = $.parseJSON(request.responseText);
                  //printErrorMsg(json.error);
                 //$('.splerror').show();
                 var str="<ul style='color:red;'>";
                 $.each(request.responseJSON, function(key, value)
                 {
                    if(key!='type')
                    {
                      $('#'+key).css({ "border": '#FF0000 1px solid'});
                      str+='<li>'+value+'</li>';                        
                    }                
                 });
                 str+='</ul>';
                 
                 swal({
                 title:"The following fields were filled incorrectly",
                  html:true,
                  type:'error',
                  text:str,
                 })
                 //$(".splerror").html(str);
                 
             }
       });
        
     })
}) 

</script>