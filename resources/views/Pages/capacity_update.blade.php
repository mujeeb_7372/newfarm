@include('include.header')

<style type="text/css">
   .farm_custome_div
   {
   border: 3px solid #DD4B39;
   padding: 10px;
   }
</style>
<body class="theme-red">
   <!-- Page Loader -->
   <div class="page-loader-wrapper">
      <div class="loader">
         <div class="preloader">
            <div class="spinner-layer pl-red">
               <div class="circle-clipper left">
                  <div class="circle"></div>
               </div>
               <div class="circle-clipper right">
                  <div class="circle"></div>
               </div>
            </div>
         </div>
         <p>Please wait...</p>
      </div>
   </div>
   <!-- #END# Page Loader -->
   <!-- Overlay For Sidebars -->
   <div class="overlay"></div>
   <!-- #END# Overlay For Sidebars -->
   <!-- Search Bar -->
   <div class="search-bar">
      <div class="search-icon">
         <i class="material-icons">search</i>
      </div>
      <input type="text" placeholder="START TYPING...">
      <div class="close-search">
         <i class="material-icons">close</i>
      </div>
   </div>
   <!-- #END# Search Bar -->
   <!-- Top Bar -->
   <nav class="navbar">
      @include('include.leaft_head')
   </nav>
   <!-- #Top Bar -->
   <section>
      <!-- Left Sidebar -->
      @include('include.left_sidebar')
      <!-- #END# Left Sidebar -->
      <!-- Right Sidebar -->
      @include('include.right_sidebar')
      <!-- #END# Right Sidebar -->
   </section>
   <section class="content custome-content" style="margin:100px 15px 0 188px">
      <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 d-flex ">
               <div class="card">
                  <div class="header">
                     <h2>
                        @if($data!=NULL)
                        Edit Capacity
                        @else
                        Add Capacity
                        @endif
                     </h2>
                  </div>
                  <div class="body">
                     <div class="farm_custome_div">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist" id="itabs">
                           <li role="presentation"  class="active">
                              <a href="#home_with_icon_title" id="exp_tb" data-toggle="tab" aria-expanded="false">
                              <i class="material-icons">home</i> Experience
                              </a>
                           </li>
                           <li role="presentation" >
                              <a href="#profile_with_icon_title" id="fr_tb" data-toggle="tab" aria-expanded="true">
                              <i class="material-icons">face</i> Frehser
                              </a>
                           </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                           <div role="tabpanel" class="tab-pane fade active in" id="home_with_icon_title">
                              <form id="experience_frm" class="comm_frm">
                                 <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                                 <input type="hidden" name="exp_isSaved" id="exp_isSaved" value="false" >
                                 <input type="hidden" name="isExperinece" id="isExperinece" value="true">
                                 <input type="hidden" name="exp_isUpdate" id="exp_isUpdate" value="@if(isset($data))  true @else false @endif">
                                 @if(isset($data))
                                   @if(!empty($data))
                                     <input type="hidden" name="capid" id="capid" value="{{$data->id}}"> 
                                   @endif
                                 @endif
                                 <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion_10" role="tablist" aria-multiselectable="true">
                                       <div class="panel panel-col-cyan">
                                          <div class="panel-heading" role="tab" id="headingOne_10">
                                             <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseOne_10" aria-expanded="true" aria-controls="collapseOne_10">
                                                General Details
                                                </a>
                                             </h4>
                                          </div>
                                          <div id="collapseOne_10" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_10">
                                             <div class="panel-body">
                                                <div class="form-group">
                                                   <label for="inputEmail">Title</label>
                                                   <span style="color:red;">
                                                      *
                                                      <div class="form-line">  
                                                         <input type="text" class="form-control" id="title" name="title" required>
                                                      </div>
                                                   </span>
                                                </div>
                                                <div class="form-group">
                                                   <label for="inputEmail">Description</label>
                                                   <span style="color:red;">
                                                      *
                                                      <div class="form-line">  
                                                         <textarea class="form-control textarea validate[required]" id="description" name="description" >
                                                            @if(!empty($data)) 
                                                            {!!base64_decode($data->description)!!} @endif
                                                         </textarea >
                                                      </div>
                                                   </span>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Capacity Type</label>
                                                         <span style="color:red;">
                                                            *
                                                            <select class="form-control  cap_type  show-tick" name="cap_type" id="cap_type" required>
                                                               <option  value="">Please select</option>
                                                               <option value="Functional">Functional</option>
                                                               <option value="Technical">Technical</option>
                                                               <option value="Techno-Functional">Techno-Functional</option>
                                                            </select>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Industry</label>
                                                         <span style="color:red;">*
                                                            <select class="form-control  show-tick" name="industry" id="industry" required>
                                                               <option  value="">Please select</option>
                                                               <option value="IT Service">IT Service</option>
                                                               <option value="Telecome">Telecome</option>
                                                               <option value="IT Product Development">IT Product Development</option>
                                                               <option value="IT Engineering Services">IT Engineering Services</option>
                                                               <option value="IT Embedded Services">IT Embedded Services</option>
                                                            </select>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Preferred Location 1</label>
                                                         <span style="color:red;">*
                                                         <input type="text" name="location" class="form-control" id="location" required="">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Preferred Location 2</label>
                                                         <input type="text" name="location_1" class="form-control" id="location_1">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Ready to onboard in</label>
                                                         <span style="color:red;">
                                                            *
                                                            <select class="form-control  show-tick" name="onboard" id="onboard" required="">
                                                               <option selected value="">Please select</option>
                                                               <option value="Immediate">Immediate</option>
                                                               <option value="15">15 Days</option>
                                                               <option value="16-30">16-30 Days</option>
                                                            </select>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Duration of Availability (months)</label>
                                                         <span style="color:red;">*
                                                         <input type="number" name="duration" class="form-control" id="duration" required="">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Comments</label>
                                                         <input type="text" name="comments" class="form-control" id="comments" >
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Source</label>
                                                         
                                                         <select class="form-control show-tic sourcename show-tick" frm="experience_frm" name="sourcename" id="sourcename" required="">
                                                            <option value="">-- Please select --</option>
                                                            <option value="Registered Partner">Registered Partner</option>
                                                            <option value="Referral">Referral</option>
                                                            <option value="Job Portal">Job Portal</option>
                                                            <option value="Walkins">Walkins</option>
                                                            <option value="Digitohub Website">Digitohub Website</option>
                                                            <option value="Non Registered Partner">Non Registered Partner</option>
                                                         </select>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                   <div id="gendiv" style="display: none;">
                                                      <label class="form-label">Source Description</label>
                                                      <input type="text" placeholder="Enter Source" name="resource_val" id="resource_val"
                                                      @if(isset($data))
                                                      value="{{$data->source_desc}}"  
                                                      @endif class="form-control">
                                                   </div>
                                                   <div id="gendiv1" style="display: none;">
                                                      <label class="form-label">Partner</label>
                                                      <select class="form-control show-tick" name="partner" id="partner">
                                                         <option value="">Select</option>
                                                         @foreach($partner as $val)
                                                         <option value="{{$val->recruiter_id}}">{{$val->name}}</option>
                                                         @endforeach
                                                      </select>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel panel-col-cyan">
                                          <div class="panel-heading" role="tab" id="headingTwo_10">
                                             <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseTwo_10" aria-expanded="false" aria-controls="collapseTwo_10">
                                                Personal Details
                                                </a>
                                             </h4>
                                          </div>
                                          <div id="collapseTwo_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_10">
                                             <div class="panel-body">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Name</label>
                                                         <span style="color:red;">*
                                                         <input type="text" name="name" class="form-control" id="name" required>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Phone No.</label>
                                                         <span style="color:red;">*
                                                         <input type="text" name="mobile" class="form-control mobile-phone-number" id="mobile" required>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Email</label>
                                                         <span style="color:red;">*
                                                         <input type="email" name="email" class="form-control" id="email" required>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">DOB</label>
                                                         <input type="text" name="dob" class="form-control datepicker" id="dob">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Address</label>
                                                         <input type="text" name="address" class="form-control" id="address">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel panel-col-cyan">
                                          <div class="panel-heading" role="tab" id="headingThree_10">
                                             <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseThree_10" aria-expanded="false" aria-controls="collapseThree_10">
                                                Education Details
                                                </a>
                                             </h4>
                                          </div>
                                          <div id="collapseThree_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_10">
                                             <div class="panel-body">
                                                <div class="col-lg-12 col-md-12 col-sm-3 col-xs-6">
                                                   <table class="table table-bordered" id="dynamic_field1">
                                                      @if(isset($education))
                                                        @if($data->isFresher=='1')
                                                          <?php $i=0; ?>
                                                          @foreach($education as $row)
                                                            <tr id="row{{$i}}" tid="{{$row->ce_id}}">
                                                               <td>
                                                                  <label for="password">Qualification</label>
                                                                  <input type="text" id="qualification" name="qualification[]" class="form-control name_list" value="{{$row->ce_qualification}}" />
                                                               </td>
                                                               <td>
                                                                  <label for="exampleInputPassword1">Course</label>
                                                                  <input type="text" id="course" name="course[]" class="form-control name_list" value="{{$row->ce_course}}" />
                                                               </td>
                                                               <td>
                                                                  <label for="exampleInputPassword1">Specialization</label>
                                                                  <input type="text" id="specilization" name="specilization[]" class="form-control name_list" value="{{$row->ce_specilization}}" />
                                                               </td>
                                                               <td>
                                                                  <label for="exampleInputPassword1">University/College</label>
                                                                  <input type="text" id="univerisity" name="univerisity[]" class="form-control name_list" value="{{$row->ce_university}}" />
                                                               </td>
                                                            </tr>
                                                            <tr id="row{{$i}}">
                                                               <td>
                                                                  <label for="exampleInputPassword1"></label>
                                                                  <select class="form-control " name="coursetype[]" id="coursetype" sr='{{$row->ce_id}}'>
                                                                     <option value="" selected>-- Course Type --</option>
                                                                     <option value="Full time">Full time</option>
                                                                     <option value="Part time">Part time</option>
                                                                     <option value="Correspondence">Correspondence</option>
                                                                  </select>
                                                               </td>
                                                               <td>
                                                                  <label for="exampleInputPassword1">Passing Year</label>
                                                                  <input type="text" id="passingyear" name="passingyear[]" class="form-control" value="{{$row->ce_passyear}}" />
                                                               </td>
                                                               <td>
                                                                  <label for="exampleInputPassword1"></label>
                                                                  <select class="form-control" id="marktype" name="marktype[]" sr="{{$row->ce_id}}">
                                                                     <option selected value="">Marks / Grade</option>
                                                                     <option value="Grade">Grade</option>
                                                                     <option value="Percentage">Percentage(%)</option>
                                                                  </select>
                                                               </td>
                                                               <td>
                                                                  <div class="col-lg-9 col-md-3 col-sm-3 col-xs-6">
                                                                     <label for="exampleInputPassword1"></label>
                                                                     <input type="text" id="mark_desc" name="mark_desc[]" class="form-control name_list" placeholder="Mark Description" value="{{$row->ce_markdesc}}" />
                                                                  </div>

                                                                 

                                                                   @if($i==0)  
                                                                      <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                                        <label for="exampleInputPassword1"></label>
                                                                        <button type="button" name="add" frm="experience_frm" tbl="dynamic_field1" id="add1" class="btn btn-success add_row">+</button>
                                                                      </div>
                                                                     @else
                                                                     <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                                        <label for="exampleInputPassword1"></label>
                                                                        <button type="button" name="remove" tbl="dynamic_field1" frm="fresher_frm" rowid="{{$i}}" id="<?php echo $i;?>" class="btn btn-danger btn_remove1">X</button>
                                                                     </div>
                                                                   @endif
                                                               </td>
                                                              
                                                               <?php $i++;?>
                                                            </tr>
                                                          @endforeach
                                                        @endif
                                                      @else
                                                       <tr>
                                                         <td>
                                                            <label for="password">Qualification</label>
                                                            <input type="text" id="qualification" name="qualification[]" class="form-control name_list" />
                                                         </td>
                                                         <td>
                                                            <label for="exampleInputPassword1">Course</label>
                                                            <input type="text" id="course" name="course[]" class="form-control name_list" />
                                                         </td>
                                                         <td>
                                                            <label for="exampleInputPassword1">Specialization</label>
                                                            <input type="text" id="specilization" name="specilization[]" class="form-control name_list" />
                                                         </td>
                                                         <td>
                                                            <label for="exampleInputPassword1">University/College</label>
                                                            <input type="text" id="univerisity" name="univerisity[]" class="form-control name_list" />
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>
                                                            <label for="exampleInputPassword1"></label>
                                                            <select class="form-control " name="coursetype[]" id="coursetype">
                                                               <option value="" selected>-- Course Type --</option>
                                                               <option value="Full time">Full time</option>
                                                               <option value="Part time">Part time</option>
                                                               <option value="Correspondence">Correspondence</option>
                                                            </select>
                                                         </td>
                                                         <td>
                                                            <label for="exampleInputPassword1">Passing Year</label>
                                                            <input type="text" id="passingyear" name="passingyear[]" class="form-control" />
                                                         </td>
                                                         <td>
                                                            <label for="exampleInputPassword1"></label>
                                                            <select class="form-control" id="marktype" name="marktype[]">
                                                               <option selected value="">Marks / Grade</option>
                                                               <option value="Grade">Grade</option>
                                                               <option value="Percentage">Percentage(%)</option>
                                                            </select>
                                                         </td>
                                                         <td>
                                                            <div class="col-lg-9 col-md-8 col-sm-3 col-xs-6">
                                                               <label for="exampleInputPassword1"></label>
                                                               <input type="text" id="mark_desc" name="mark_desc[]" class="form-control name_list" placeholder="Mark Description" />
                                                            </div>
                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                               <label for="exampleInputPassword1"></label>
                                                               <button type="button" name="add" frm="experience_frm" tbl="dynamic_field1" id="add1" class="btn btn-success add_row">+</button>
                                                            </div>
                                                         </td>
                                                      </tr>
                                                      @endif 
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel panel-col-cyan">
                                          <div class="panel-heading" role="tab" id="headingThree_10">
                                             <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseFour_10" aria-expanded="false" aria-controls="collapseFour_10">
                                                Experience Details
                                                </a>
                                             </h4>
                                          </div>
                                          <div id="collapseFour_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_10">
                                             <div class="panel-body">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Total Experience</label>
                                                         <span style="color:red;">*
                                                         <input type="number" name="totalexp" class="form-control" id="totalexp" required>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Company Name</label>
                                                         <input type="text" name="company_name" class="form-control" id="company_name">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Current Designation</label>
                                                         <input type="text" name="designation" class="form-control" id="designation">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Annaul Salary</label>
                                                         <input type="number" name="salary" class="form-control" id="salary">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Working Since</label>
                                                         <input type="text" name="worksince" class="form-control" id="worksince">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Current Location</label>
                                                         <input type="text" name="curr_location" class="form-control" id="curr_location">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Primary Skills</label>
                                                         <span style="color:red;">*
                                                         <input type="text" class="form-control" data-role="tagsinput"  style="display: none;" id="pskill" name="pskill">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Secondry Skill</label>
                                                         <input type="text" class="form-control" data-role="tagsinput"  style="display: none;" name="sskill" id="sskill">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Functional Area</label>
                                                         <input type="text" name="function" class="form-control" id="function">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Onsite Experience</label>
                                                         <input type="text" name="onsite_exp" class="form-control" id="onsite_exp">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Technology Track</label>
                                                         <select class="form-control  show-tick" name="technology" id="technology">
                                                            <option selected value="">-- Please select --</option>
                                                            <option value="Package">Package</option>
                                                            <option value="ADM">ADM</option>
                                                         </select>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Type of Engagement</label>
                                                         <span style="color:red;">*
                                                         <select class="form-control  show-tick" name="engagement" id="engagement" required="">
                                                            <option selected value="">-- Please select --</option>
                                                            <option value="Implimentation">Implimentation</option>
                                                            <option value="Roleout">Roleout</option>
                                                            <option value="Support">Support</option>
                                                            <option value="Designing">Designing</option>
                                                            <option value="Integration">Integration</option>
                                                            <option value="Testing">Testing</option>
                                                            <option value="Other">Other</option>
                                                         </select>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="text-center">
                                    <a id="save_btn1" class="btn btn-primary m-t-15 waves-effect">Save</a>
                                    <button type="submit" class="btn btn-success m-t-15 waves-effect">Submit</button>
                                    <a href="{{ url()->previous()}}" class="btn btn-danger m-t-15 waves-effect">Cancel</a>
                                 </div>
                              </form>
                           </div>
                           <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                              <form id="fresher_frm" class="comm_frm">
                                 <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                                 <input type="hidden" name="exp_isSaved" id="exp_isSaved" value="false">
                                 <input type="hidden" name="isExperinece" id="isExperinece" value="false">
                                 <input type="hidden" name="fresher_isUpdate" value="@if(isset($data))  true @else false @endif">
                                 @if(isset($data))
                                   @if(!empty($data))
                                     <input type="hidden" name="capid" id="capid" value="{{$data->id}}"> 
                                   @endif
                                 @endif

                                 <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion_10" role="tablist" aria-multiselectable="true">
                                       <div class="panel panel-col-cyan">
                                          <div class="panel-heading" role="tab" id="headingFive_10">
                                             <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseFive_10" aria-expanded="true" aria-controls="collapseFive_10">
                                                General Details
                                                </a>
                                             </h4>
                                          </div>
                                          <div id="collapseFive_10" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFive_10">
                                             <div class="panel-body">
                                                <div class="form-group">
                                                   <label for="inputEmail">Title</label>
                                                   <span style="color:red;">
                                                      *
                                                      <div class="form-line">  
                                                         <input type="text" class="form-control" id="title" name="title" required>
                                                      </div>
                                                   </span>
                                                </div>
                                                <div class="form-group">
                                                   <label for="inputEmail">Description</label>
                                                   <span style="color:red;">
                                                      *
                                                      <div class="form-line">  
                                                         <textarea class="form-control textarea" id="description" name="description">
                                                             @if(!empty($data)) 
                                                              {!!base64_decode($data->description)!!} 
                                                             @endif
                                                         </textarea >
                                                      </div>
                                                   </span>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Capacity Type</label>
                                                         <span style="color:red;">
                                                            *
                                                            <select class="form-control cap_type show-tick" name="cap_type" id="cap_type" required>
                                                               <option  value="">Please select</option>
                                                               <option value="Functional">Functional</option>
                                                               <option value="Technical">Technical</option>
                                                               <option value="Techno-Functional">Techno-Functional</option>
                                                            </select>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Industry</label>
                                                         <span style="color:red;">*
                                                            <select class="form-control  show-tick" name="industry" id="industry" required>
                                                               <option  value="">Please select</option>
                                                               <option value="IT Service">IT Service</option>
                                                               <option value="Telecome">Telecome</option>
                                                               <option value="IT Product Development">IT Product Development</option>
                                                               <option value="IT Engineering Services">IT Engineering Services</option>
                                                               <option value="IT Embedded Services">IT Embedded Services</option>
                                                            </select>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Preferred Location 1</label>
                                                         <span style="color:red;">*
                                                         <input type="text" name="location" class="form-control" id="location" required="">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Preferred Location 2</label>
                                                         <input type="text" name="location_1" class="form-control" id="location_1">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Ready to onboard in</label>
                                                         <span style="color:red;">*
                                                            <select class="form-control  show-tick" name="onboard" id="onboard" required="">
                                                               <option selected value="">Please select</option>
                                                               <option value="Immediate">Immediate</option>
                                                               <option value="15">15 Days</option>
                                                               <option value="16-30">16-30 Days</option>
                                                            </select>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Duration of Availability (months)</label>
                                                         <span style="color:red;">*
                                                         <input type="number" name="duration" class="form-control" id="duration" required="">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Comments</label>
                                                         <input type="text" name="comments" class="form-control" id="comments">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Source</label>
                                                         <span style="color:red;">*
                                                         <select class="form-control  sourcename show-tick" frm="fresher_frm" name="sourcename" id="sourcename" required="">
                                                            <option value="">-- Please select --</option>
                                                            <option value="Registered Partner">Registered Partner</option>
                                                            <option value="Referral">Referral</option>
                                                            <option value="Job Portal">Job Portal</option>
                                                            <option value="Walkins">Walkins</option>
                                                            <option value="Digitohub Website">Digitohub Website</option>
                                                            <option value="Non Registered Partner">Non Registered Partner</option>
                                                         </select>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                   <div id="gendiv" style="display: none;">
                                                      <label class="form-label">Source Description</label>
                                                      <input type="text" placeholder="Enter Source" name="resource_val" id="resource_val"
                                                      @if(isset($data))
                                                      value="{{$data->source_desc}}"  
                                                      @endif class="form-control">
                                                   </div>
                                                   <div id="gendiv1" style="display: none;">
                                                      <label class="form-label">Partner</label>
                                                      <select class="form-control show-tick" name="partner" id="partner">
                                                         <option value="">Select</option>
                                                         @foreach($partner as $val)
                                                         <option value="{{$val->recruiter_id}}">{{$val->name}}</option>
                                                         @endforeach
                                                      </select>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel panel-col-cyan">
                                          <div class="panel-heading" role="tab" id="headingSix_10">
                                             <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseSix_10" aria-expanded="false" aria-controls="collapseSix_10">
                                                Personal Details
                                                </a>
                                             </h4>
                                          </div>
                                          <div id="collapseSix_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix_10">
                                             <div class="panel-body">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Name</label>
                                                         <span style="color:red;">*
                                                         <input type="text" name="name" class="form-control" id="name" required>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Phone No.</label>
                                                         <span style="color:red;">*
                                                         <input type="text" name="mobile" class="form-control mobile-phone-number" id="mobile" required>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Email</label>
                                                         <span style="color:red;">*
                                                         <input type="email" name="email" class="form-control" id="email" required>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">DOB</label>
                                                         <input type="text" name="dob" class="form-control datepicker" id="dob">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-6">
                                                   <div class="form-group">
                                                      <div class="form-line">
                                                         <label for="password">Address</label>
                                                         <input type="text" name="address" class="form-control" id="address">
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="panel panel-col-cyan">
                                          <div class="panel-heading" role="tab" id="headingThree_10">
                                             <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_10" href="#collapseSeven_10" aria-expanded="false" aria-controls="collapseSeven_10">
                                                Education Details
                                                </a>
                                             </h4>
                                          </div>
                                          <div id="collapseSeven_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven_10">
                                             <div class="panel-body">
                                                <div class="col-lg-12 col-md-12 col-sm-3 col-xs-6">
                                                   <table class="table table-bordered" id="dynamic_field1">
                                                       @if(isset($education))
                                                        @if($data->isFresher=='0')
                                                          <?php $i=0; ?>
                                                          @foreach($education as $row)
                                                            <tr id="row{{$i}}" tid="{{$row->ce_id}}">
                                                               <td>
                                                                  <label for="password">Qualification</label>
                                                                  <input type="text" id="qualification" name="qualification[]" class="form-control name_list" value="{{$row->ce_qualification}}" />
                                                               </td>
                                                               <td>
                                                                  <label for="exampleInputPassword1">Course</label>
                                                                  <input type="text" id="course" name="course[]" class="form-control name_list" value="{{$row->ce_course}}" />
                                                               </td>
                                                               <td>
                                                                  <label for="exampleInputPassword1">Specialization</label>
                                                                  <input type="text" id="specilization" name="specilization[]" class="form-control name_list" value="{{$row->ce_specilization}}" />
                                                               </td>
                                                               <td>
                                                                  <label for="exampleInputPassword1">University/College</label>
                                                                  <input type="text" id="univerisity" name="univerisity[]" class="form-control name_list" value="{{$row->ce_university}}" />
                                                               </td>
                                                            </tr>
                                                            <tr id="row{{$i}}">
                                                               <td>
                                                                  <label for="exampleInputPassword1"></label>
                                                                  <select class="form-control " name="coursetype[]" id="coursetype" sr="{{$row->ce_id}}">
                                                                     <option value="" selected>-- Course Type --</option>
                                                                     <option value="Full time">Full time</option>
                                                                     <option value="Part time">Part time</option>
                                                                     <option value="Correspondence">Correspondence</option>
                                                                  </select>
                                                               </td>
                                                               <td>
                                                                  <label for="exampleInputPassword1">Passing Year</label>
                                                                  <input type="text" id="passingyear" name="passingyear[]" class="form-control" value="{{$row->ce_passyear}}" />
                                                               </td>
                                                               <td>
                                                                  <label for="exampleInputPassword1"></label>
                                                                  <select class="form-control" id="marktype" name="marktype[]" sr="{{$row->ce_id}}">
                                                                     <option selected value="">Marks / Grade</option>
                                                                     <option value="Grade">Grade</option>
                                                                     <option value="Percentage">Percentage(%)</option>
                                                                  </select>
                                                               </td>
                                                               <td>
                                                                  <div class="col-lg-9 col-md-8 col-sm-3 col-xs-6">
                                                                     <label for="exampleInputPassword1"></label>
                                                                     <input type="text" id="mark_desc" name="mark_desc[]" class="form-control name_list" placeholder="Mark Description" value="{{$row->ce_markdesc}}" />
                                                                  </div>

                                                                 

                                                                   @if($i==0)  
                                                                      <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                                        <label for="exampleInputPassword1"></label>
                                                                        <button type="button" name="add" frm="fresher_frm" tbl="dynamic_field1" id="add1" class="btn btn-success add_row">+</button>
                                                                      </div>
                                                                     @else
                                                                     <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                                        <label for="exampleInputPassword1"></label>
                                                                        <button type="button" name="remove" tbl="dynamic_field1" frm="fresher_frm" rowid="{{$i}}" id="<?php echo $i;?>" class="btn btn-danger btn_remove1">X</button>
                                                                     </div>
                                                                   @endif
                                                               </td>
                                                              
                                                               <?php $i++;?>
                                                            </tr>
                                                          @endforeach
                                                        @endif
                                                      @else
                                                      <tr>
                                                         <td>
                                                            <label for="password">Qualification</label>
                                                            <input type="text" id="qualification" name="qualification[]" class="form-control name_list" />
                                                         </td>
                                                         <td>
                                                            <label for="exampleInputPassword1">Course</label>
                                                            <input type="text" id="course" name="course[]" class="form-control name_list" />
                                                         </td>
                                                         <td>
                                                            <label for="exampleInputPassword1">Specialization</label>
                                                            <input type="text" id="specilization" name="specilization[]" class="form-control name_list" />
                                                         </td>
                                                         <td>
                                                            <label for="exampleInputPassword1">University/College</label>
                                                            <input type="text" id="univerisity" name="univerisity[]" class="form-control name_list" />
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>
                                                            <label for="exampleInputPassword1"></label>
                                                            <select class="form-control " name="coursetype[]" id="coursetype">
                                                               <option value="" selected>-- Course Type --</option>
                                                               <option value="Full time">Full time</option>
                                                               <option value="Part time">Part time</option>
                                                               <option value="Correspondence">Correspondence</option>
                                                            </select>
                                                         </td>
                                                         <td>
                                                            <label for="exampleInputPassword1">Passing Year</label>
                                                            <input type="text" id="passingyear" name="passingyear[]" class="form-control" />
                                                         </td>
                                                         <td>
                                                            <label for="exampleInputPassword1"></label>
                                                            <select class="form-control" id="marktype" name="marktype[]">
                                                               <option selected value="">Marks / Grade</option>
                                                               <option value="Grade">Grade</option>
                                                               <option value="Percentage">Percentage(%)</option>
                                                            </select>
                                                         </td>
                                                         <td>
                                                            <div class="col-lg-9 col-md-8 col-sm-3 col-xs-6">
                                                               <label for="exampleInputPassword1"></label>
                                                               <input type="text" id="mark_desc" name="mark_desc[]" class="form-control name_list" placeholder="Mark Description" />
                                                            </div>
                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                                                               <label for="exampleInputPassword1"></label>
                                                               <button type="button" tbl="dynamic_field1" frm="fresher_frm" name="add" id="add1" class="btn btn-success add_row">+</button>
                                                            </div>
                                                         </td>
                                                      </tr>
                                                      @endif 
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="text-center">
                                    <a id="save_btn2" class="btn btn-primary m-t-15 waves-effect">Save</a>
                                    <button type="submit" class="btn btn-success m-t-15 waves-effect">Submit</button>
                                    <a href="{{ url()->previous()}}" class="btn btn-danger m-t-15 waves-effect">Cancel</a>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   @include('include.footer')

 <script src="{{URL::asset('public/')}}/js/capacity.js"></script>
 <script type="text/javascript">
 $(document).ready(function()
 { 
      @if(isset($data))
       var frm_id="";
        @if(!empty($data))
          @if($data->isFresher=='0')
            $('#experience_frm').hide()
            var active_tab_selector = $('.nav-tabs > li > #exp_tb').attr('href');
            frm_id='#fresher_frm';
              $(active_tab_selector).removeClass('active');
              $(active_tab_selector).addClass('hide');
              $('#fr_tb').tab('show')
              $('#exp_tb').hide()
              
              $('#home_with_icon_title').collapse('show')


           @else
            $('#fresher_frm').hide()
             var active_tab_selector = $('.nav-tabs > li > #fr_tb').attr('href');
              frm_id='#experience_frm'; 
              $(active_tab_selector).removeClass('active');
              $(active_tab_selector).addClass('hide');
              $('#exp_tb').tab('show')
              $('#fr_tb').hide()
              
              $('#profile_with_icon_title').collapse('show')

              $(frm_id+' #totalexp').val('{{$data->experience}}');
              $(frm_id+' #company_name').val('{{$data->comapany_name}}');
              $(frm_id+' #designation').val('{{$data->designation}}');
              $(frm_id+' #salary').val('{{$data->annaulsalary}}');
              $(frm_id+' #worksince').val('{{$data->workingsince}}');
              $(frm_id+' #curr_location').val('{{$data->curr_location}}');
              $(frm_id+' #pskill').tagsinput('add','{{$data->primary_skills}}');
              $(frm_id+' #sskill').tagsinput('add','{{$data->secondary_skills}}');
              $(frm_id+' #function').val('{{$data->func_area}}');
              $(frm_id+' #onsite_exp').val('{{$data->onsite_experience}}');
              $(frm_id+' #technology').val('{{$data->tech_track}}');
              $(frm_id+' #engagement').val('{{$data->typeofengae}}');

          @endif

           $(frm_id+' #title').val('{{$data->title}}');
           $(frm_id+' #description').val(`{!!base64_decode($data->description)!!}`);

           $(frm_id+' #location').val('{{$data->preferred_location}}');
           $(frm_id+' #location_1').val('{{$data->preferred_location1}}');
           $(frm_id+' #duration').val('{{$data->duration_of_availability}}');
           $(frm_id+' #comments').val('{{$data->comments}}');
           

           $(frm_id+' #cap_type').val('{{$data->capacity_type}}');
           $(frm_id+' #industry').val('{{$data->industry}}');
           $(frm_id+' #onboard').val('{{$data->onboard}}');
           $(frm_id+' #sourcename').val('{{$data->source}}');
           

           @if($data->source=='Registered Partner')
              $(frm_id+' #partner').val('{{$data->source_part}}');
              $(frm_id+' #gendiv').hide()
              $(frm_id+' #gendiv1').show()
             @else
              $(frm_id+' #resource_val').val('{{$data->source_desc}}');
              $(frm_id+' #gendiv').show()
              $(frm_id+' #gendiv1').hide()
           @endif
        

           $(frm_id+' #dynamic_field1 tbody tr #coursetype').each(function() 
           {   
              @foreach($education as $row)
                if($(this).attr('sr')=='{{$row->ce_id}}')
                {
                  $(this).val('{{$row->ce_course_type}}');
                }
              @endforeach
           });

           $(frm_id+' #dynamic_field1 tbody tr #marktype').each(function() 
           {   
              @foreach($education as $row)
                if($(this).attr('sr')=='{{$row->ce_id}}')
                {
                  $(this).val('{{$row->ce_marktype}}');
                }
              @endforeach
           });
           
           $(frm_id+' #name').val('{{$data->name}}');
           $(frm_id+' #mobile').val('{{$data->mobile}}');
           $(frm_id+' #email').val('{{$data->email}}');
           $(frm_id+' #dob').val('{{$data->dob}}');
           $(frm_id+' #address').val('{{$data->address}}');
           
            

        @endif
      @endif

     $('.mobile-phone-number').inputmask('9999999999', { placeholder: '__________' });
     $('.show-tick').selectpicker('refresh');
     tinymce.init({
        selector: "textarea.textarea",
        theme: "modern",
        height: 140,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
    });
    tinymce.suffix = ".min";
    tinyMCE.baseURL = '{{URL::asset("public/src/")}}/plugins/tinymce';

     $('.comm_frm').submit(function(e) 
     {
        
      //$(".tab-content").loading();
        e.preventDefault();

        var url="";

        var editor='description';
        var content =tinyMCE.activeEditor.getContent();
        
        var formData = new FormData($(this)[0]);
        formData.append('description',content);
        //formData.append('description',$('textarea#description').val());
        url="{{url('ajax_add_capacity')}}";

         $.ajax({
           type: 'POST',
           cache: false,
           headers: 
           {
             'X-CSRF-TOKEN': $('#_token').val()
           },
           url: url,
           data:formData,
           processData : false,
           contentType: false,
           dataType:'json',
           success: function(data)
           {
              $('.tab-content').loading('stop');
               if(data.type=='success')
               {
                    swal({
                        title:"Response",
                        text:data.msg,
                        type:data.type,
                        imageUrl: "https://gurayyarar.github.io/AdminBSBMaterialDesign/images/thumbs-up.padding",
                        showCancelButton:false,
                         showConfirmButton: true
                     },function()
                     {
                        if(data.list_type=='saved')
                        {
                           window.location.href="{{url('capacity/page/saved/0')}}";
                        }
                        if(data.list_type=='submit')
                        {
                          window.location.href="{{url('capacity/page/submit/0')}}";    
                        }
                       
                     })
               }
               
           },
           error: function (request, status, error) 
           {
                $('.tab-content').loading('stop');
                 
                 var str="<ul style='color:red;'>";
                 $.each(request.responseJSON.errors, function(key, value)
                 {
                    if(key!='type')
                    {
                      $('#'+key).css({ "border": '#FF0000 1px solid'});
                      str+='<li>'+value+'</li>';                        
                    }                
                 });
                 str+='</ul>';
                 
                 swal({
                 title:"The following fields were filled incorrectly",
                  html:true,
                  type:'error',
                  text:str,
                 })
                 //$(".splerror").html(str);
                 
             }
       });
        
     })

     $('.sourcename').change(function() 
     {

         var val = $(this).val();
         var frm=$(this).attr('frm');

         if (val == '') {
             $('#'+frm+' #gendiv').hide()
             $('#'+frm+' #gendiv1').hide()
             return false;
         }
         if (val == 'Registered Partner') {
             $('#'+frm+' #gendiv').hide()
             $('#'+frm+' #gendiv1').show()

         } else { 

             $('#'+frm+' #gendiv').show()
             $('#'+frm+' #gendiv1').hide()
         }
     })
     $('#save_btn1').click(function()
     {
           
        $('#experience_frm').find('#exp_isSaved').val('true');
        $('#experience_frm').submit();
      }) 
      $('#save_btn2').click(function()
      {
        $('#fresher_frm').find('#exp_isSaved').val('true');
        $('#fresher_frm').submit();
      })


    var q=1;
    var i=1;
    $('.add_row').click(function()
    {
        
       i++;
       var tbl=$(this).attr('tbl');  
       var frm=$(this).attr('frm'); 
       add_table_rows(tbl,frm,i)

    });  
    $(document).on('click', '.btn_remove1', function()
    {  
      var tbl=$(this).attr('tbl');  
      var frm=$(this).attr('frm');
      var button_id = $(this).attr("id");   

      $('#'+frm+' #row'+button_id+'').remove();  
      $('#'+frm+' #row'+button_id+'').remove();  
    });
  /*===================================*/
});
 

      
      
   </script>