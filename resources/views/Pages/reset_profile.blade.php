<!DOCTYPE>
<html>
   <head>
      <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   </head>
   <style type="text/css">
      body{
      background: -webkit-linear-gradient(left, #0072ff, #00c6ff);
      }
      .contact-form{
      background: #fff;
      margin-top: 10%;
      margin-bottom: 5%;
      width: 70%;
      }
      .contact-form .form-control{
      border-radius:1rem;
      }
      .contact-image{
      text-align: center;
      }
      .contact-image img{
      border-radius: 6rem;
      width: 11%;
      margin-top: -3%;
      transform: rotate(29deg);
      }
      .contact-form form{
      padding: 14%;
      }
      .contact-form form .row{
      margin-bottom: -7%;
      }
      .contact-form h3{
      margin-bottom: 8%;
      margin-top: -10%;
      text-align: center;
      color: #0062cc;
      }
      .contact-form .btnContact {
      width: 50%;
      border: none;
      border-radius: 1rem;
      padding: 1.5%;
      background: #dc3545;
      font-weight: 600;
      color: #fff;
      cursor: pointer;
      }
      .btnContactSubmit
      {
      width: 50%;
      border-radius: 1rem;
      padding: 1.5%;
      color: #fff;
      background-color: #0062cc;
      border: none;
      cursor: pointer;
      }
   </style>
   <body>
      <div class="container contact-form" style="background: #0062cc;">
         <div class="contact-images" style="text-align: center;">
            <img src="https://farm.digitohub.com/web/images/logo_new.png" alt="rocket_contact"/>
         </div>
         @if($errors->any())
         <h4>{{$errors->first()}}</h4>
         @endif
         <form id="login-form" class="text-left" action="{{url('update_partner_pwd')}}" style="margin-left: 105px;">
            {{ csrf_field() }} 
            <input type="hidden" name="rid" value="{{$id}}" >     
            <input type="hidden" name="remail" value="{{$email}}" >
            <h3>Digito<b>HUB</b></h3>
            <div class="row">
               <div class="col-md-9">
                  <div class="form-group">
                     <label class="form-control">Email : {{base64_decode($email)}}</label>
                  </div>
                  <div class="form-group">
                     <input required="" type="password" name="password1" class="form-control" placeholder="Enter New Password" />
                  </div>
                  <div class="form-group text-center" >
                     <input type="submit" name="btnSubmit" class="btnContact" value="Update" />
                  </div>
               </div>
            </div>
         </form>
      </div>
   </body>
</html>