<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    /*public function handle($request, Closure $next)
    {
        if($request->user()->status==0)
        {
            return redirect('logout');
        }
        return $next($request);
    }*/
    public function handle($request, Closure $next)
    {

        $response = $next($request);

        //If the status is not approved redirect to login 

        if(Auth::check() && Auth::user()->status== '0'){

            Auth::logout();

            $request->session()->flash('alert-danger', 'Your Account is not activated yet.');

            return redirect('/login')->with('erro_login', 'Your error text');

        }

        return $response;

    }
}
