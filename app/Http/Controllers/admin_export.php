<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use League\Csv\Reader;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use General;
use Exporter;
use DB;
use File;
use Zip;
class admin_export extends Controller
{
    
    public function downloadUser_Data()
    {
        $query = DB::table('users')->select('name as Name','email as Email','pwd_text as Password','mobile as Mobile','usertype as User_Type','partner_id as Partner_ID','company_id as Comapny_ID');
        /*if(login_details('usertype')=='Admin' || login_details('usertype')=='admin_u')
        {
 		   
        }*/
        
        $query->where('company_id',login_details('company'));	 
        if(empty($query->get()->toArray()))
	    {
	      	return redirect()->back();
	    }
	      return (new FastExcel($query->get()))->download('User_List.xlsx');

         /* $folder=rand();	
	      Storage::disk('local')->makeDirectory('ExportData/'.$folder);
	      $path=(new FastExcel($query))->export('storage/app/ExportData/'.$folder.'/file.csv');*/
	     	
	     /*=================================================*/	
	       /*$zip = Zip::create('your_zipfile.zip');
	       $zip->add('storage/app/ExportData/'.$folder.'/');

	       $zip->add('storage/app/ExportData/'.$folder);

			// declaring path
			$zip->setPath('storage/app/ExportData/'.$folder)->add('file');

			// add directory
			$zip->add('storage/app/ExportData/');

			// add directory (only its content)
			$zip->add('storage/app/ExportData/', true);*/
		/*=================================================*/		

	       //return response()->download($path);
	       

			
			
	}      
    public function download_Partner($status)
    {
    	 $query = DB::table('company_profiles as a')
    	 ->select('a.company_name as Comapny_Name','a.business as Business','a.address as Company_address','a.contact_name as Contact_Person','a.contact_number as Contact_Number','a.website as Website','a.email as Comapny_Email','pan as PAN_No','gst as GST','bank_account_no as Bank_Account','a.bank_name as Bank_Name','a.bank_address as Bank_Address','a.ifsc_code as IFSC_Code','a.swift_code as Swift Code','a.iban_no as IBAN_NO','a.nda_status as NDA_STATUS','a.date_of_nda as Date_of_NDA','b.name as Recruiter_Name','b.designation as Designation',
    	 	'b.mobile as Recruiter_Mobile','b.email as Recruiter_Email','b.category as Category',
    	 	'b.strength as Strenght','b.weakness as Weakness','b.noemployee as NO_of_Employee',
    	 	'b.trunover as Turnover')->
			join('recruiter_profiles as b','a.company_id','=','b.company_id')
			->where('a.company_id','!=',login_details('company'));

	      	if($status=='save')
	      	{
	      		$query->where('b.saved_status','1');
	      	}
	      	if($status=='submit')
	      	{
	      		$query->where('b.saved_status','0');	
	      	}
	      if(empty($query->get()->toArray()))
	      {
	      	return redirect()->back();
	      }
		  return (new FastExcel($query->get()))->download('Partner_List.xlsx');
    }
    public function downloadRequirement_data($status)
    {
    	$usertype=login_details('usertype');
    	 $query = DB::table('requirment')
    	 ->select('requirement_id as No','title as Title',DB::raw('FROM_BASE64(description) as Description'),'requirement_type as Requirement_Type','primary_skills as Primary Skills','secondary_skills as Secondry_skills','min_experience as Minimum_Experience','max_experience as Maximum_Experience','work_location as Work_Location','place as Place','project_type as Project_Type','industry as Industry','duration as Duration','start_date as Start_Date','posted_by as Posted_By_Id',
    	 	'partner_id as Recruiter_Id','company_id as Company_Id','isSaved as Save_Status','source as Source','source_desc as Source_Description','source_partner as Recruiter_Id','comment as Comment');
    	 if($usertype=='Partner' || $usertype=='partner_user')
    	 {
    	 	$query=NULL;
    	 	 $query=DB::table('requirment')->select('requirement_id as No','title as Title',DB::raw('FROM_BASE64(description) as Description'),'requirement_type as Requirement_Type','primary_skills as Primary Skills','secondary_skills as Secondry_skills','min_experience as Minimum_Experience','max_experience as Maximum_Experience','work_location as Work_Location','place as Place','project_type as Project_Type','industry as Industry','duration as Duration','start_date as Start_Date','isSaved as Save_Status','comment as Comment');
    	 }
    	 if($status=='save')
    	 {
    	 	$query->where('company_id',login_details('company'))->where('isSaved','1');
    	 	if($usertype=='Partner' || $usertype=='partner_user')
    	 	{
    	 		$query->where('partner_status','pending');
    	 	}
    	 	else
    	 	{
    	 		$query->where('admin_status','pending');	
    	 	}
    	 	
    	 }
    	 if($status=='submit')
    	 {
    	 	$query->where('company_id',login_details('company'))->where('isSaved','0');
    	 	if($usertype=='Partner' || $usertype=='partner_user')
    	 	{
    	 		$query->where('partner_status','submitted');
    	 	}
    	 	else
    	 	{
    	 		$query->where('admin_status','submitted');	
    	 	}
    	 	
    	 }
    	 if($status=='shortlist')
    	 {
    	 	$query->join('requirement_shortlist as b','requirment.id','=','b.short_id')
    	 	->where('short_company_id',login_details('company'))->where('b.short_status','0');
    	 	
    	 }
    	 if($status=='deactive')
    	 {
    	 	$query->where('company_id',login_details('company'))->where('admin_status','inactive');
    	 }
    	 if($status=='checkout')
    	 {
    	 	$query->join('requirement_shortlist as b','requirment.id','=','b.short_id')
    	 	->where('short_company_id',login_details('company'))->where('b.short_status','1');
    	 }
    	 if($status=='publish')
    	 {
    	 	$query->where('company_id',login_details('company'));
    	 	if($usertype=='Partner' || $usertype=='partner_user')
    	 	{
    	 		$query->where('partner_status','publish');
    	 	}
    	 	else
    	 	{
    	 		$query->where('admin_status','publish');	
    	 	}
    	 	
    	 }
    	 if($status=='part_submit')
    	 {
    	 	$query->where('company_id','!=',login_details('company'))
    	 	->where('partner_status','submitted');
    	 }
    	 if($status=='part_checkout')
    	 {
    	 	$query->join('requirement_shortlist as b','requirment.id','=','b.short_id')
    	 	->where('short_company_id','!=',login_details('company'))
    	 	->where('b.short_status','1');
    	 }
    	 //return $query->toSql();
    	 if(empty($query->get()->toArray()))
	     {
	      	return redirect()->back();
	     }	
		  return (new FastExcel($query->get()))->download($status.'.xlsx');
    }
    public function downloadCapacity_data($status)
    {
    	$usertype=login_details('usertype');
    	 $query = DB::table('capacity as a')->select('a.capacity_no as Capacity_No','a.title as Title',DB::raw('FROM_BASE64(a.description) as Description'),'a.onboard as On_Board','a.industry as Industry','a.capacity_type as Capacity_Type','a.primary_skills as Primary_Skills','a.secondary_skills as Secondary_Skills','a.experience as Experience','a.preferred_location as Preferred_Location','a.preferred_location1 as Preferred_Location_1','a.projects_worked as Project_Wored','a.availability as Avaiblity','a.duration_of_availability as Duration_of_Availability','a.comments as Comments','a.posted_by as Posted_User_Id',
    	 	'a.partner_id as Posted_Partner_Id','a.cap_companyid as Posted_Company_Id','a.name as Candidate_Name','a.email as Candidate_Email','a.mobile as Candidate_Mobile','a.dob as Candidate_DOB','a.address as Candidate_Address','a.status_admin as Admin_Status','a.status_partner as Partner_Status','a.source as Source','a.source_desc as Source_Description','a.source_part as Source_Partner_Id','a.comapany_name as Previouse_Company','a.designation as Candidate_Position','a.annaulsalary as Annual_Salary','a.workingsince as Working_Since','a.curr_location as Current_Location','a.func_area as Functional_Area','a.onsite_experience as On_Site_Experience','a.tech_track as Tech_Track','a.isSaved as Save_Status');
    	 if($usertype=='Partner' || $usertype=='partner_user')
    	 {
    	 	$query=NULL;
    	 	 $query = DB::table('capacity as a')->select('a.capacity_no as Capacity_No','a.title as Title',DB::raw('FROM_BASE64(a.description) as Description'),'a.onboard as On_Board','a.industry as Industry','a.capacity_type as Capacity_Type','a.primary_skills as Primary_Skills','a.secondary_skills as Secondary_Skills','a.experience as Experience','a.preferred_location as Preferred_Location','a.preferred_location1 as Preferred_Location_1','a.projects_worked as Project_Wored','a.availability as Avaiblity','a.duration_of_availability as Duration_of_Availability','a.comments as Comments','a.posted_by as Posted_User_Id','a.partner_id as Posted_Partner_Id','a.cap_companyid as Posted_Company_Id','a.name as Candidate_Name','a.email as Candidate_Email','a.mobile as Candidate_Mobile','a.dob as Candidate_DOB','a.address as Candidate_Address','a.status_partner as Partner_Status','a.comapany_name as Previouse_Company','a.designation as Candidate_Position','a.annaulsalary as Annual_Salary','a.workingsince as Working_Since','a.curr_location as Current_Location','a.func_area as Functional_Area','a.onsite_experience as On_Site_Experience','a.tech_track as Tech_Track','a.isSaved as Save_Status');
    	 }
    	 if($status=='save')
    	 {
    	 	$query->where('a.cap_companyid',login_details('company'))->where('a.isSaved','1');
    	 	if($usertype=='Partner' || $usertype=='partner_user')
    	 	{
    	 		$query->where('status_partner','saved');
    	 	}
    	 	else
    	 	{
    	 		$query->where('status_admin','saved');	
    	 	}
    	 	
    	 }
    	 if($status=='submit')
    	 {
    	 	$query->where('a.cap_companyid',login_details('company'))->where('a.isSaved','0');
    	 	if($usertype=='Partner' || $usertype=='partner_user')
    	 	{
    	 		$query->where('status_partner','submit');
    	 	}
    	 	else
    	 	{
    	 		$query->where('status_admin','submit');	
    	 	}
    	 	
    	 }
    	 if($status=='shortlist')
    	 {
    	 	$query->join('capacity_shortlist as b','a.id','=','b.cs_capid')
    	 	->where('cs_company_id',login_details('company'))
    	 	->where('b.cs_short_status','0');
    	 	
    	 }
    	 if($status=='deactive')
    	 {
    	 	$query->where('a.cap_companyid',login_details('company'))
    	 	->where('status_admin','inactive');
    	 }
    	 if($status=='checkout')
    	 {
    	 	$query->join('capacity_shortlist as b','a.id','=','b.cs_capid')
    	 	->where('cs_company_id',login_details('company'))
    	 	->where('b.cs_short_status','1');
    	 }
    	 if($status=='publish')
    	 {
    	 	$query->where('a.cap_companyid',login_details('company'));
    	 	if($usertype=='Partner' || $usertype=='partner_user')
    	 	{
    	 		$query->where('status_admin','publish');
    	 	}
    	 	else
    	 	{
    	 		$query->where('status_partner','publish');	
    	 	}
    	 	
    	 }
    	 if($status=='part_submit')
    	 {
    	 	$query->where('a.cap_companyid','!=',login_details('company'))
    	 	->where('status_partner','submit');
    	 }
    	 if($status=='part_checkout')
    	 {
    	 	$query->join('capacity_shortlist as b','a.id','=','b.cs_capid')
    	 	->where('cs_company_id','!=',login_details('company'))
    	 	->where('b.cs_short_status','1');
    	 }
    	 
    	 if(empty($query->get()->toArray()))
	     {
	      	return redirect()->back();
	     }

	      
		  return (new FastExcel($query->get()))->download($status.'.xlsx');
    }
}
