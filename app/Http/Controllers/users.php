<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use General;

class users extends Controller
{
	public $utype;
    public $uid;
    public $reqruiter_id;
    public $permission;
    public $company_id;

    public function __construct()
    {
        $this->middleware('auth');
        $data=$this->middleware(function ($request, $next) 
        {
            $this->utype=Auth::user()->usertype;
            $this->uid=Auth::user()->id;
            $this->company_id=Auth::user()->company_id;
            $this->reqruiter_id=Auth::user()->partner_id;
            return $next($request);
        });
    }

    public function get_user_list(Request $request)
    {
    	$user=$this->utype;
        
    	$data=DB::table('users')->select('*',DB::raw('DATE(created_at) as created_at'));
    	if($user=='Admin' || $user=='admin_user')
    	{
    		$data->where('usertype','admin_user');
    	}
    	if($user=='Partner' || $user=='partner_user')
    	{
    		$data->where('usertype','partner_user');
    	}

    	$name=$request->input('u_name');
    	$email=$request->input('u_email');
    	$mobile=$request->input('u_phone');
    	$status=$request->input('u_status');


    	if($name!='' && isset($name))
    	{
    		$data->where('name',$name);
    	}
    	if($email!='' && isset($email))
    	{
    		$data->where('email',$email);
    	}
    	if($mobile!='' && isset($mobile))
    	{
    		$data->where('mobile',$mobile);
    	}
    	if($status!='' && isset($status))
    	{
    		$data->where('status',$status);
    	}
        $data->orderBy('id','desc');
    	$data->where('company_id',$this->company_id);
    	return datatables()->of($data->get())->toJson();
    }
    public function add_users_page($id=NULL)
    {
    	$usertype=$this->utype;
        $data=NULL;

       if($id!=NULL)
       {
         $data=DB::table('users')->where('id',$id)->first();
       } 
       return view('Pages.users_update',compact('usertype','data'));
    }
    public function add_users(Request $request)
    {
        $usertype=$this->utype;
        $user_type="admin_user";
        if($usertype=='Partner')
        {
          $user_type="partner_user";
        }
        $request->validate([

                'u_name' => 'required',
                'u_pwd' => 'min:6|required_with:u_conf_pwd|same:u_conf_pwd',
                'u_conf_pwd'=>'required',
                'u_email' => 'required|email',
                'u_status' => 'required'

            ], [

                'u_name.required' => 'Name is Required',
                'u_pwd.required' => 'Password is Required',
                
                'u_email.required' => 'Email ID is Required',
                'u_status.required' => 'Status is Required',

            ]);

        $name=$request->input('u_name');
        $email=$request->input('u_email');
        $password=$request->input('u_pwd');
        $mobile=$request->input('u_mobile');
        $status=$request->input('u_status');
        $isEdit=$request->input('isEdit');
        $check_user=$this->exist_user($email);
        if($isEdit=='false')
        {
            if(!empty($check_user))
            {
                echo json_encode(array('type'=>'warning','msg'=>'User already exist'));
                exit;
            }    
            
        }
        
        
        $data=array('name'=>$name,'email'=>$email,'pwd_text'=>$password,'password'=>bcrypt($password),'mobile'=>$mobile,
            'usertype'=>$user_type,'status'=>$status,'partner_id'=>$this->reqruiter_id,'company_id'=>$this->company_id);

        DB::beginTransaction();
        $msg="";
        try 
        {
            if($isEdit!='false')
            {
              DB::table('users')->where('id',$isEdit)->update($data);
              $msg=json_encode(array('type'=>'success','msg'=>'User Update Successful'));
            }
            else
            {
              DB::table('users')->insert($data);    
              $msg=json_encode(array('type'=>'success','msg'=>'User Add Successful'));
            }
            
        
            DB::commit();
            echo $msg;
        } 
        catch (\Exception $e) 
        {
            DB::rollback();
            // something went wrong
            echo json_encode(array('type'=>'error','msg'=>$e->getMessage()));
        }
    }
   public function exist_user($email)
   {
     $result=DB::table('users')->select('email')->where('email',$email)->first();
     return $result;
   }  
   public function user_action($status,$id)
   {
      
         DB::table('users')->where('id',$id)->update(array('status'=>$status));
         return redirect()->back()->with('success','Data Update Successful');
      
   }  
   
}
