<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Product;
use Validator;
use DB;

class taskController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('tb_testtask');


        return $this->sendResponse($products->toArray(), 'Products retrieved successfully.');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'title' => 'required',
            'start_date' => 'required',
            'due_date' => 'required',
            'priority' => 'required',
            'description' => 'required',
            'status' => 'required',
        ]);

        $title=$request->input('title');
        $start_date=$request->input('start_date');
        $due_date=$request->input('due_date');
        $status=$request->input('status');
        $priority=$request->input('priority');
        $description=$request->input('description');


        if($validator->fails())
        {
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $data=array('title'=>$title,'start_date'=>$start_date,'due_date'=>$due_date,'status'=>$status,'priority'=>$priority,
            'description'=>$description);
        $product =DB::table('tb_testtask')->insert($data);


        return $this->sendResponse($product->toArray(), 'Product created successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = DB::table('tb_testtask')->where('task_id',$id)->first();


        if (is_null($product)) {
            return $this->sendError('Product not found.');
        }


        return $this->sendResponse($product->toArray(),'Product retrieved successfully.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();


       $validator = Validator::make($input, [
            'title' => 'required',
            'start_date' => 'required',
            'due_date' => 'required',
            'priority' => 'required',
            'description' => 'required',
            'status' => 'required',
        ]);

        $title=$request->input('title');
        $start_date=$request->input('start_date');
        $due_date=$request->input('due_date');
        $status=$request->input('status');
        $priority=$request->input('priority');
        $description=$request->input('description');


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }


         $data=array('title'=>$title,'start_date'=>$start_date,'due_date'=>$due_date,'status'=>$status,'priority'=>$priority,
            'description'=>$description);
        $product =DB::table('tb_testtask')->where('taks_id')->update($data);


        return $this->sendResponse($product->toArray(), 'Product updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=DB::table('tb_testtask')->delete($id);
        return $this->sendResponse($product->toArray(), 'Product deleted successfully.');
    }
}