<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use DB;
use File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use General;

class partner extends Controller
{
   public $utype;
   public $uid;
   public $reqruiter_id;
   public $permission;
   public $company_id;
   public function __construct()
   {
	    $this->middleware('auth');
	    $data=$this->middleware(function ($request, $next) 
	    {
	        $this->utype=Auth::user()->usertype;
	        $this->uid=Auth::user()->id;
	        $this->company_id=Auth::user()->company_id;
	        $this->reqruiter_id=Auth::user()->partner_id;
	        return $next($request);
	    });
   }
   public function add_partner_page(Request $reqest,$id=NULL)
   {
   	  $usertype=$this->utype;
      if($usertype=='Admin' || $usertype=='admin_user')
      {
        $data=NULL;
     	  if($id!=NULL)
     	  {
     	  	$data=DB::table('company_profiles')->select('*')->where('company_id',$id)->first();
     	  	$reqruiter=DB::table('recruiter_profiles')->select('*')->where('company_id',$id)->first();
     	  	
     	  	
     	  	$comp_address=DB::table('company_address')->select('*')->where('cadd_companyid',$id)->get();

     	  	$user_detail=DB::table('users')->select('*')->where('partner_id',$reqruiter->recruiter_id)
     	  	->where('company_id',$id)->where('usertype','Partner')->first();

     	  	$comp_contact=DB::table('company_contact')->select('*')->where('cc_companyid',$id)->get();
     	  	$comp_tech=DB::table('company_technology')->select('*')->where('ctg_companyid',$id)->get();

     	  	return view('Pages.admin.Partner.partner_update',compact('usertype','data','reqruiter','comp_address','user_detail','comp_contact','comp_tech'));
     	  }
     }
     else
     {
       return redirect()->back();
     }
      return view('Pages.admin.Partner.partner_update',compact('usertype','data'));
   }
   public function partner_list_page(Request $request)
   {
   	 $usertype=$this->utype;
     if($usertype=='Admin' || $usertype=='admin_user')
     {
     	 $company=DB::table('company_profiles')->select('*')->where('comp_isActive',1)->get();
     	 return view('Pages.admin.Partner.partner_list',compact('usertype','company'));
     }
     else
     {
       return redirect()->back();
     }
   }
   public function partner_saved_page(Request $request)
   {
     $usertype=$this->utype;
     if($usertype=='Admin' || $usertype=='admin_user')
     {
       $company=DB::table('company_profiles')->select('*')->where('comp_isActive',1)->get();
       return view('Pages.admin.Partner.partner_saved_list',compact('usertype','company'));
     } 
     else
     {
       return redirect()->back();
     }  
   }
   public function get_partner_list(Request $request)
   {
   	    $user=$this->utype;

    	$data=DB::table('recruiter_profiles as a')
      ->select('a.company_id','a.name as name','b.email as email','b.contact_number as mobile','b.company_name as company_name',DB::raw('DATE(b.created_at) as created_at'),'b.comp_isActive as comp_isActive')
    	->join('company_profiles as b','a.company_id','=','b.company_id');
    	
    	$name=$request->input('u_name');
    	$email=$request->input('u_email');
    	$mobile=$request->input('u_phone');
    	$comp_id=$request->input('comp_id');
      $is_saved=$request->input('isSaved');

      if(isset($is_saved))
      {
       $data->where('a.saved_status',$is_saved); 
      }
    	if($name!='' && isset($name))
    	{
    		$data->where('a.name','like','%'.$name.'%');
    	}
    	if($email!='' && isset($email))
    	{
    		$data->where('b.email','like','%'.$email.'%');
    	}
    	if($mobile!='' && isset($mobile))
    	{
    		$data->where('b.contact_number','like','%'.$mobile.'%');
    	}
    	if($comp_id!='' && isset($comp_id))
    	{
    		$data->where('b.company_id',$comp_id);
    	}
      if(isset($this->company_id))
      {
        $data->where('a.company_id','!=',$this->company_id);  
      }
      
    	$data->orderBy('b.company_id', 'desc');
    	return datatables()->of($data->get())->toJson();
   }
   public function add_company(Request $request)
   {
   	 $utype=$this->utype;
   	 	$this->validate($request, 
        [
          'comp_name' => 'required',
          'comp_buisness' => 'required',
          'comp_webiste' => 'required',
          'comp_contact_name' => 'required',
          'comp_contact_number' => 'required',
          'comp_contact_email' => 'required',

          'usr_name' => 'required',
          'usr_email' => 'required',
          'usr_mobile' => 'required',
          'usr_designation' => 'required',
          'usr_pwd' => 'min:6|required_with:usr_conf_pwd|same:usr_conf_pwd',
          'usr_conf_pwd' => 'required',

          'comp_acc_no' => 'required',
          'comp_bank_name' => 'required',
          'comp_ifsc_code' => 'required',
          'comp_bank_add' => 'required',

          'comp_nda_status' => 'required',
        ],
        [
          'comp_name.requiured' => 'Company Name is Required',
          'comp_buisness.requiured' => 'Buisness is Required',
          'comp_webiste.requiured' => 'Webiste is Required',
          'comp_contact_name.requiured' => 'Company Contact Name is Required',
          'comp_contact_number.requiured' => 'Company Contact Number is Required',
          'comp_contact_email.requiured' => 'Company Email is Required',

          'usr_name.requiured' => 'User Name is Required',
          'usr_email.requiured' => 'User Email is Required',
          'usr_mobile.requiured' => 'User Mobile is Required',
          'usr_designation.requiured' => 'User Designation is Required',
          
          'comp_acc_no.requiured' => 'Bank Account No. is Required',
          'comp_bank_name.requiured' => 'Bank Name is Required',
          'comp_ifsc_code.requiured' => 'Bank IFSC is Required',
          'comp_bank_add.requiured' => 'Bank Address is Required',

          'comp_nda_status.requiured' => 'NDA Status is Required',
        ]);

        $company_name=$request->input('comp_name');
        $company_buisness=$request->input('comp_buisness');
        $company_website=$request->input('comp_webiste');
        $comapany_cont_persone=$request->input('comp_contact_name');
        $company_cont_no=$request->input('comp_contact_number');
        $company_email=$request->input('comp_contact_email');

        $comp_add1=$request->input('cadd1');
        $comp_state1=$request->input('cstate1');
        $comp_city1=$request->input('ccity1');
        $comp_zip1=$request->input('czip1');
        $comp_country1=$request->input('ccountry1');

        $comp_location=array();
            
        $cont_name1=$request->input('ccontname1');
        $cont_desig1=$request->input('ccontdesig1');
        $cont_mobile1=$request->input('ccontmob1');
        $cont_email=$request->input('ccontemail1');
        $cont_location=$request->input('ccontloc1');

        $cont_details=array();

        $core_tech=$request->input('ccoretech');
        $other_experties=$request->input('cotherexper');
        $tech1=$request->input('ctechology1');

        $comp_tech=array();

        $user_name=$request->input('usr_name');
        $user_email=$request->input('usr_email');
        $user_mobile=$request->input('usr_mobile');
        $user_designation=$request->input('usr_designation');
        $user_pwd=$request->input('usr_pwd');

        $pan_no=$request->input('comp_pan_no');
        $pan_file=$request->input('comp_pan_file');
        $gst_no=$request->input('comp_gst_no');
        $gst_file=$request->input('comp_gst_file');
        $bank_account=$request->input('comp_acc_no');
        $bank_name=$request->input('comp_bank_name');
        $ifsc_code=$request->input('comp_ifsc_code');
        $bank_address=$request->input('comp_bank_add');
        $swift_code=$request->input('comp_swift_code');
        $ibn_no=$request->input('comp_iban_no');
        $cheque_file=$request->input('comp_cheque_file');


        $nda_status=$request->input('comp_nda_status');
        $nda_start_date=$request->input('comp_date_nda');
        $nda_end_date=$request->input('comp_end_nda_date');
        $nda_file=$request->input('comp_nda_file');


        $evl_cate=$request->input('comp_ev_categeory');
        $evl_strength=$request->input('comp_ev_strength');
        $evl_weakness=$request->input('comp_ev_weakness');
        $evl_employee=$request->input('comp_ev_employee');
        $evl_trunover=$request->input('comp_ev_trunover');

        $isSaved=$request->input('isSaved');
        $saved_status=0;
        if($isSaved=='false')
        {
        	$msg="Record Add Successfully in Saved List";
        }
        if($isSaved=='true')
        {
        	$saved_status=1;
          	$msg='Record Successfully Submitted';
        }

        $nda_path="";
        $gst_path="";
        $pan_path="";
        $bcheque_path="";

       if($request->file('comp_nda_file'))
       {
          
            $uploadedFile = $request->file('comp_nda_file');
              $filename = time().$uploadedFile->getClientOriginalName();

              $nda_path='storage/app/'.Storage::disk('local')->putFileAs('NDA_Image/'.$filename,$uploadedFile,$filename);
       }
       if($request->file('comp_pan_file'))
       {
          
            $uploadedFile = $request->file('comp_pan_file');
              $filename = time().$uploadedFile->getClientOriginalName();

              $pan_path='storage/app/'.Storage::disk('local')->putFileAs('Pan_Image/'.$filename,$uploadedFile,$filename);

       }
       if($request->file('comp_gst_file'))
       {
          
            $uploadedFile = $request->file('comp_gst_file');
              $filename = time().$uploadedFile->getClientOriginalName();

              $gst_path='storage/app/'.Storage::disk('local')->putFileAs('GST_Image/'.$filename,$uploadedFile,$filename);
       }
       if($request->file('comp_cheque_file'))
       {
          
            $uploadedFile = $request->file('comp_cheque_file');
              $filename = time().$uploadedFile->getClientOriginalName();

              $bcheque_path='storage/app/'.Storage::disk('local')->putFileAs('Bank_cheque_Image/'.$filename,$uploadedFile,$filename);
       }

       $data=array('company_name'=>$company_name,'business'=>$company_buisness,'contact_number'=>$company_cont_no,'website'=>$company_website,'email'=>$company_email,'pan'=>$pan_no,'pan_image'=>$pan_path,'gst'=>$gst_no,'gst_image'=>$gst_path,'bank_account_no'=>$bank_account,
       	'bank_name'=>$bank_name,'bank_address'=>$bank_address,'cheque_image'=>$bcheque_path,
       	'ifsc_code'=>$ifsc_code,'swift_code'=>$swift_code,'iban_no'=>$ibn_no,'nda_status'=>$nda_status,
       	'date_of_nda'=>$nda_start_date,'date_of_nda_end'=>$nda_end_date,'nda_image'=>$nda_path,
       	'comp_isActive'=>0,'contact_name'=>$comapany_cont_persone,'expiry_type'=>'limited',
       	'expiry_date'=>$nda_end_date,'uid'=>$this->uid);


       DB::beginTransaction();

		try 
		{
			$comp_id=DB::table('company_profiles')->insertGetId($data);

			$data1=array('company_id'=>$comp_id,'name'=>$comapany_cont_persone,'designation'=>$user_designation,'mobile'=>$company_cont_no,'email'=>$company_email,'saved_status'=>$saved_status,'status'=>0,'category'=>$evl_cate,'strength'=>$evl_strength,
				'weakness'=>$evl_weakness,'noemployee'=>$evl_employee,'trunover'=>$evl_trunover);

			$id=DB::table('recruiter_profiles')->insertGetId($data1);

			$data2=array('name'=>$user_name,'email'=>$user_email,'mobile'=>$user_mobile,
                'password'=>bcrypt($user_pwd),'pwd_text'=>$user_pwd,'usertype'=>'Partner','status'=>'0',
                'partner_id'=>$id,'company_id'=>$comp_id);

			DB::table('users')->insert($data2);

			for($i=0; $i<=count($comp_add1)-1; $i++)
             {
              $comp_location[]=array('cadd_companyid'=>$comp_id,'cadd_address'=>$comp_add1[$i],
                'cadd_state'=>$comp_state1[$i],'cadd_city'=>$comp_city1[$i],'cadd_zip'=>$comp_zip1[$i]
                ,'cadd_country'=>$comp_country1[$i]);
             }
             for($i=0; $i<=count($cont_name1)-1; $i++)
             {
                $cont_details[]=array('cc_companyid'=>$comp_id,'cc_name'=>$cont_name1[$i],
                'cc_designation'=>$cont_desig1[$i],'cc_mobile'=>$cont_mobile1[$i],'cc_email'=>$cont_email[$i],'cc_location'=>$cont_location[$i]);
             }
             for($i=0; $i<=count($core_tech)-1; $i++)
             {
             	 $comp_tech[]=array('ctg_companyid'=>$comp_id,'ctg_core'=>$core_tech[$i],
                'ctg_other'=>$other_experties[$i],'ctg_technolog'=>$tech1[$i]);
             }   

             DB::table('company_address')->insert($comp_location);
             DB::table('company_contact')->insert($cont_details);
             DB::table('company_technology')->insert($comp_tech); 

		    DB::commit();
		    echo json_encode(array('type'=>'success','msg'=>$msg,'isSaved'=>$isSaved));
		
		} 
		catch (\Exception $e) 
		{
		    DB::rollback();
			echo json_encode(array('type'=>'error','msg'=>$e->getMessage()));
		}
   }
   public function update_partner_page(Request $request)
   {
      $id=$request->input('cid');

      $utype=$this->utype;
      $this->validate($request, 
        [
          'comp_name' => 'required',
          'comp_buisness' => 'required',
          'comp_webiste' => 'required',
          'comp_contact_name' => 'required',
          'comp_contact_number' => 'required',
          'comp_contact_email' => 'required',

          'usr_name' => 'required',
          'usr_email' => 'required',
          'usr_mobile' => 'required',
          'usr_designation' => 'required',
          'usr_pwd' => 'min:6|required_with:usr_conf_pwd|same:usr_conf_pwd',
          'usr_conf_pwd' => 'required',

          'comp_acc_no' => 'required',
          'comp_bank_name' => 'required',
          'comp_ifsc_code' => 'required',
          'comp_bank_add' => 'required',

          'comp_nda_status' => 'required',


        ],
        [
           'comp_name' => 'required',
          'comp_buisness' => 'required',
          'comp_webiste' => 'required',
          'comp_contact_name' => 'required',
          'comp_contact_number' => 'required',
          'comp_contact_email' => 'required',

          'usr_name' => 'required',
          'usr_email' => 'required',
          'usr_mobile' => 'required',
          'usr_designation' => 'required',
          'usr_pwd' => 'min:6|required_with:usr_conf_pwd|same:usr_conf_pwd',
          'usr_conf_pwd' => 'required',

          'comp_acc_no' => 'required',
          'comp_bank_name' => 'required',
          'comp_ifsc_code' => 'required',
          'comp_bank_add' => 'required',

          'comp_nda_status' => 'required',
        ]);

        $company_name=$request->input('comp_name');
        $company_buisness=$request->input('comp_buisness');
        $company_website=$request->input('comp_webiste');
        $comapany_cont_persone=$request->input('comp_contact_name');
        $company_cont_no=$request->input('comp_contact_number');
        $company_email=$request->input('comp_contact_email');

        $comp_add1=$request->input('cadd1');
        $comp_state1=$request->input('cstate1');
        $comp_city1=$request->input('ccity1');
        $comp_zip1=$request->input('czip1');
        $comp_country1=$request->input('ccountry1');

        $comp_location=array();
            
        $cont_name1=$request->input('ccontname1');
        $cont_desig1=$request->input('ccontdesig1');
        $cont_mobile1=$request->input('ccontmob1');
        $cont_email=$request->input('ccontemail1');
        $cont_location=$request->input('ccontloc1');

        $cont_details=array();

        $core_tech=$request->input('ccoretech');
        $other_experties=$request->input('cotherexper');
        $tech1=$request->input('ctechology1');

        $comp_tech=array();

        $user_name=$request->input('usr_name');
        $user_email=$request->input('usr_email');
        $user_mobile=$request->input('usr_mobile');
        $user_designation=$request->input('usr_designation');
        $user_pwd=$request->input('usr_pwd');

        $pan_no=$request->input('comp_pan_no');
        $pan_file=$request->input('comp_pan_file');
        $gst_no=$request->input('comp_gst_no');
        $gst_file=$request->input('comp_gst_file');
        $bank_account=$request->input('comp_acc_no');
        $bank_name=$request->input('comp_bank_name');
        $ifsc_code=$request->input('comp_ifsc_code');
        $bank_address=$request->input('comp_bank_add');
        $swift_code=$request->input('comp_swift_code');
        $ibn_no=$request->input('comp_iban_no');
        $cheque_file=$request->input('comp_cheque_file');


        $nda_status=$request->input('comp_nda_status');
        $nda_start_date=$request->input('comp_date_nda');
        $nda_end_date=$request->input('comp_end_nda_date');
        $nda_file=$request->input('comp_nda_file');


        $evl_cate=$request->input('comp_ev_categeory');
        $evl_strength=$request->input('comp_ev_strength');
        $evl_weakness=$request->input('comp_ev_weakness');
        $evl_employee=$request->input('comp_ev_employee');
        $evl_trunover=$request->input('comp_ev_trunover');

        $isSaved=$request->input('isSaved');
        $saved_status=0;
        if($isSaved=='false')
        {
          $msg="Record Add Successfully in Saved List";
        }
        if($isSaved=='true')
        {
          $saved_status=1;
            $msg='Record Successfully Submitted';
        }

        $nda_path="";
        $gst_path="";
        $pan_path="";
        $bcheque_path="";

       

       $data=array('company_name'=>$company_name,'business'=>$company_buisness,'contact_number'=>$company_cont_no,'website'=>$company_website,'email'=>$company_email,'pan'=>$pan_no,'gst'=>$gst_no,'bank_account_no'=>$bank_account,'bank_name'=>$bank_name,'bank_address'=>$bank_address,
        'ifsc_code'=>$ifsc_code,'swift_code'=>$swift_code,'iban_no'=>$ibn_no,'nda_status'=>$nda_status,
        'date_of_nda'=>$nda_start_date,'date_of_nda_end'=>$nda_end_date,'contact_name'=>$comapany_cont_persone,'expiry_type'=>'limited','expiry_date'=>$nda_end_date,'updated_by_uid'=>$this->uid,);
        $old=DB::table('company_profiles')->where('company_id',$id)->get()->toArray();
        if($request->file('comp_nda_file'))
       {
          
            $uploadedFile = $request->file('comp_nda_file');
              $filename = time().$uploadedFile->getClientOriginalName();

              $nda_path='storage/app/'.Storage::disk('local')->putFileAs('NDA_Image/'.$filename,$uploadedFile,$filename);
              $data['nda_image']=$nda_path;
              if (File::delete(public_path($old[0]->nda_image)))
              {
                
              }
       }
       if($request->file('comp_pan_file'))
       {
          
            $uploadedFile = $request->file('comp_pan_file');
              $filename = time().$uploadedFile->getClientOriginalName();

              $pan_path='storage/app/'.Storage::disk('local')->putFileAs('Pan_Image/'.$filename,$uploadedFile,$filename);
              $data['pan_image']=$pan_path;
              if (File::delete(public_path($old[0]->pan_image)))
              {
              
              }
       }
       if($request->file('comp_gst_file'))
       {
          
            $uploadedFile = $request->file('comp_gst_file');
              $filename = time().$uploadedFile->getClientOriginalName();

              $gst_path='storage/app/'.Storage::disk('local')->putFileAs('GST_Image/'.$filename,$uploadedFile,$filename);
              $data['gst_image']=$gst_path;
              if (File::delete(public_path($old[0]->gst_image)))
              {
                
              }
       }
       if($request->file('comp_cheque_file'))
       {
          
            $uploadedFile = $request->file('comp_cheque_file');
              $filename = time().$uploadedFile->getClientOriginalName();

              $bcheque_path='storage/app/'.Storage::disk('local')->putFileAs('Bank_cheque_Image/'.$filename,$uploadedFile,$filename);
            $data['cheque_image']=$bcheque_path;  
            if (File::delete(public_path($old[0]->cheque_image)))
            {
            
            }
       }

       DB::beginTransaction();

    try 
    {
      $comp_id=DB::table('company_profiles')->where('company_id',$id)->update($data);

      $data1=array('name'=>$comapany_cont_persone,'designation'=>$user_designation,'mobile'=>$company_cont_no,'email'=>$company_email,'saved_status'=>$saved_status,'category'=>$evl_cate,'strength'=>$evl_strength,'weakness'=>$evl_weakness,'noemployee'=>$evl_employee,'trunover'=>$evl_trunover);

      DB::table('recruiter_profiles')->where('company_id',$id)->update($data1);

      $data2=array('name'=>$user_name,'email'=>$user_email,'mobile'=>$user_mobile,
                'password'=>bcrypt($user_pwd),'pwd_text'=>$user_pwd);

      DB::table('users')->where('company_id',$id)->where('usertype','Partner')->update($data2);

       
         

         if(!empty($comp_add1))
          {
              DB::table('company_address')->where('cadd_companyid', $id)->delete();
             for($i=0; $i<=count($comp_add1)-1; $i++)
             {
              $comp_location[]=array('cadd_companyid'=>$id,'cadd_address'=>$comp_add1[$i],
                'cadd_state'=>$comp_state1[$i],'cadd_city'=>$comp_city1[$i],'cadd_zip'=>$comp_zip1[$i]
                ,'cadd_country'=>$comp_country1[$i]);
             }
             DB::table('company_address')->insert($comp_location);
          }
          if(!empty($cont_name1))
          {
              DB::table('company_contact')->where('cc_companyid', $id)->delete();
             for($i=0; $i<=count($cont_name1)-1; $i++)
             {
                $cont_details[]=array('cc_companyid'=>$id,'cc_name'=>$cont_name1[$i],
                'cc_designation'=>$cont_desig1[$i],'cc_mobile'=>$cont_mobile1[$i],'cc_email'=>$cont_email[$i],'cc_location'=>$cont_location[$i]);
             }
             DB::table('company_contact')->insert($cont_details);
          }
          if(!empty($core_tech))
          { 
            DB::table('company_technology')->where('ctg_companyid', $id)->delete();
            for($i=0; $i<=count($core_tech)-1; $i++)
            {
              $comp_tech[]=array('ctg_companyid'=>$id,'ctg_core'=>$core_tech[$i],
              'ctg_other'=>$other_experties[$i],'ctg_technolog'=>$tech1[$i]);
            }
            DB::table('company_technology')->insert($comp_tech);
          } 


        DB::commit();
        echo json_encode(array('type'=>'success','msg'=>$msg,'isSaved'=>$isSaved));
    
    } 
    catch (\Exception $e) 
    {
        DB::rollback();
      echo json_encode(array('type'=>'error','msg'=>$e->getMessage()));
    }
   }
   public function view_details_page($id)
   {
     $usertype=$this->utype;

     $company_details=DB::table('company_profiles')->where('company_id',$id)->first();

     $company_address=DB::table('company_address')->where('cadd_companyid',$id)->get();
     $company_contact=DB::table('company_contact')->where('cc_companyid',$id)->get();
     $company_tech=DB::table('company_technology')->where('ctg_companyid',$id)->get();

     $requriter_details=DB::table('recruiter_profiles')->where('company_id',$id)->first();

     $users=DB::table('users')->where('company_id',$id)->where('usertype','Partner')->first();


     return view('Pages.admin.Partner.partner_detail_view',compact('usertype','company_details',
      'company_address','company_contact','company_tech','requriter_details',
      'users'));
   }
   public function view_grap_report(Request $req,$company)
   {
      $usertype=$this->utype;
      
       $create_capacity=0;
       $create_requirement=0;
       $shortlist_capacity=0;
       $shortlist_requirement=0;
       $checkout_capacity=0;
       $checkout_requirement=0;
       if($req->ajax())
         {
              $create_capacity=DB::table('user_history')
              ->select(DB::raw('count(*) as qty'))
               ->where('company_id',$company)
               ->where('actions','Add Capacity')
               ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
               ->first();

               $create_requirement=DB::table('user_history')
               ->select(DB::raw('count(*) as qty'))
               ->where('company_id',$company)
               ->where('actions','Add Requirement')
               ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
               ->first();

               $shortlist_capacity=DB::table('user_history')
               ->select(DB::raw('count(*) as qty'))
               ->where('company_id',$company)
               ->where('actions','Shortlist Capacity')
               ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
               ->first();

               $shortlist_requirement=DB::table('user_history')
               ->select(DB::raw('count(*) as qty'))
               ->where('company_id',$company)
               ->where('actions','Shortlist Requirement')
               ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
               ->first();

               $checkout_capacity=DB::table('user_history')
               ->select(DB::raw('count(*) as qty'))
               ->where('company_id',$company)
               ->where('actions','Checkout Capacity')
               ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
               ->first();

               $checkout_requirement=DB::table('user_history')
               ->select(DB::raw('count(*) as qty'))
               ->where('company_id',$company)
               ->where('actions','Checkout Requirment')
               ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
               ->first();
         }
         else
         {
           $create_capacity=DB::table('user_history')
           ->select(DB::raw('count(*) as qty'))
           ->where('company_id',$company)
           ->where('actions','Add Capacity')
           ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
           ->first();
           

           $create_requirement=DB::table('user_history')
           ->select(DB::raw('count(*) as qty'))
           ->where('company_id',$company)
           ->where('actions','Add Requirement')
           ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
           ->first();

           $shortlist_capacity=DB::table('user_history')
           ->select(DB::raw('count(*) as qty'))
           ->where('company_id',$company)
           ->where('actions','Shortlist Capacity')
           ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
           ->first();

           $shortlist_requirement=DB::table('user_history')
           ->select(DB::raw('count(*) as qty'))
           ->where('company_id',$company)
           ->where('actions','Shortlist Requirement')
           ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
           ->first();

           $checkout_capacity=DB::table('user_history')
           ->select(DB::raw('count(*) as qty'))
           ->where('company_id',$company)
           ->where('actions','Checkout Capacity')
           ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
           ->first();

           $checkout_requirement=DB::table('user_history')
           ->select(DB::raw('count(*) as qty'))
           ->where('company_id',$company)
           ->where('actions','Checkout Requirment')
           ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())')
           ->first();

           return view('Pages.admin.partner.partner_graph_detail',compact('usertype','create_capacity','create_requirement','shortlist_capacity','shortlist_requirement','checkout_capacity','checkout_requirement','company')); 
         }
      
   }
   public function sent_plain_email($tomsg,$html,$arr,$subject)
   {

       $mail = Mail::send($html,$arr, 
       function($message) use($subject,$tomsg)
       {
         $message->from('Mujeeb@digitohub.com', Auth::user()->name);
         $message->to($tomsg)
         ->subject($subject);
         

       });
       return $mail;
   }
   public function sent_attached_email()
   {
       $utype=$this->utype;
       $mail = Mail::send('pages.email_template.home', array('utype' => $utype), 
       function($message)
       {
         $message->from(Auth::user()->email, Auth::user()->name);
         $message->to('Mujeeb@digitohub.com')->subject('abcccc');
         $message->attach('public/uploads/articles/ssss.docx', [
                    'as' => 'name.docx',
                    'mime' => 'application/docx',
                ]);
       });
   }
   public function part_action($status,$id)
   {


      if($this->utype=='Admin' || $this->utype=='admin_user')
      {

         

         DB::beginTransaction();

          try 
          {
              
             DB::table('company_profiles')->where('company_id',$id)->update(array('comp_isActive'=>$status));
             DB::table('users')->where('company_id',$id)->update(array('status'=>$status));
             DB::table('recruiter_profiles')->where('company_id',$id)->update(array('status'=>$status));
             DB::commit();
              // all good
          } 
          catch (\Exception $e) 
          {
              DB::rollback();
              echo $e->getMessage();
          }
          
         $data=DB::table('recruiter_profiles')->select('*')->where('company_id',$id)->first();
         
         if($status=='0')
         {
             $x=view('Pages.email_template.partner_activation_email',compact('utype','data','status'));  
              
              /*$y=$this->sent_plain_email($data->email,'pages.email_template.partner_activation_email',array('utype'=>$utype,'data'=>$data),'Welcome to DigitoHub - Farm');*/
         }
         if($status=='1')
         {
             $x=view('Pages.email_template.partner_activation_email',compact('utype','data','status'));  
              
             /* $y=$this->sent_plain_email($data->email,'pages.email_template.partner_activation_email',array('utype'=>$utype,'data'=>$data),'Welcome to DigitoHub - Farm');*/
         }
         return redirect()->back()->with('success','Data Update Successful');
      }
   }
   function reset_partner_pwd($id,$email)
   {

      $data=DB::table('recruiter_profiles')->where('recruiter_id',base64_decode($id))->first();
      /*if($data->activation_mail==1)
      {
        return redirect()->back();
      }*/
      $utype=$this->utype;
      return view('pages.reset_profile',compact('utype','id','email'));
   }
   function update_partner_pwd(Request $request)
    {
      $id=base64_decode($request->input('rid'));
      $email=base64_decode($request->input('remail'));
      
      $this->validate($request, 
        [
            'password1' => 'min:6|required',
        ],
        [
           'password1.min' => 'Password is Required',
           
        ]);

      $password1=$request->input('password1');


      $data=array('password'=>bcrypt($password1),'pwd_text'=>$password1);

      DB::beginTransaction();

      try 
      {
      
        DB::table('recruiter_profiles')->where('recruiter_id',$id)->update(array('activation_mail'=>1));
        DB::table('users')->where('partner_id',$id)->where('email',$email)->update($data);
        DB::commit();
      
      } catch (\Exception $e) 
      {
        DB::rollback();
        throw $e;
      }
      
      return redirect('login');
    }
}
