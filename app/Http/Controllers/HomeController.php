<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use General;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $utype;
    public $uid;
    public $reqruiter_id;
    public $permission;
    public $company_id;

    public function __construct()
    {
        $this->middleware('auth');
        $data=$this->middleware(function ($request, $next) 
        {
            $this->utype=Auth::user()->usertype;
            $this->uid=Auth::user()->id;
            $this->company_id=Auth::user()->company_id;
            $this->reqruiter_id=Auth::user()->partner_id;
            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       $usertype=$this->utype;
       
       
       if($usertype=='Partner' || $usertype=='admin_user')
       {
           $saved=$this->get_requirement_count('pending','true');
           $submit=$this->get_requirement_count('submitted','true');
           $deactive=$this->get_requirement_count('inactive','true');
           $publish=$this->get_requirement_count('publish','true');

           $short_list=$this->get_requirement_count('shortlisted','true');
           $checkout=$this->get_requirement_count('checout','true');

           $part_submit=0;
           $part_checkout=0;

           $cap_saved=$this->get_capacity_count('saved','true');
           $cap_submit=$this->get_capacity_count('submit','true');
           $cap_deactive=$this->get_capacity_count('inactive','true');
           $cap_publish=$this->get_capacity_count('publish','true');

           $cap_short_list=0;
           $cap_checkout=0;

           $cap_part_submit=$this->get_capacity_count('part_submit','true');
           $cap_part_checkout=$this->get_capacity_count('part_checkout','true');

           $active_part=0;
           $active_user=DB::table('users')->where('company_id',login_details('company'))
           ->where('status',1)->count();
           $active_user=$active_user-1;

           return view('Pages.admin_dashboard',compact('usertype','saved','submit','deactive','publish','short_list','checkout','part_submit','part_checkout','cap_saved','cap_submit','cap_deactive','cap_publish','cap_short_list','cap_checkout','cap_part_submit','cap_part_checkout','active_part','active_user'));
           exit;
       }
       /*============For Admin==================*/
       $saved=$this->get_requirement_count('pending');
       $submit=$this->get_requirement_count('submitted');
       $deactive=$this->get_requirement_count('inactive');
       $publish=$this->get_requirement_count('publish');

       $short_list=$this->get_requirement_count('shortlisted');
       $checkout=$this->get_requirement_count('checout');

       $part_submit=$this->get_requirement_count('part_submit');
       $part_checkout=$this->get_requirement_count('part_checkout');

       $cap_saved=$this->get_capacity_count('saved');
       $cap_submit=$this->get_capacity_count('submit');
       $cap_deactive=$this->get_capacity_count('inactive');
       $cap_publish=$this->get_capacity_count('publish');

       $cap_short_list=$this->get_capacity_count('shortlisted');
       $cap_checkout=$this->get_capacity_count('checout');

       $cap_part_submit=$this->get_capacity_count('part_submit');
       $cap_part_checkout=$this->get_capacity_count('part_checkout');

       $active_part=DB::table('company_profiles')
       ->where('company_id','!=',login_details('company'))
       ->where('comp_isActive',1)
       ->count();
       $active_user=DB::table('users')
       ->where('company_id',login_details('company'))
       ->where('status',1)
       ->count();
       $active_user=$active_user-1;

       return view('Pages.admin_dashboard',compact('usertype','saved','submit','deactive','publish','short_list','checkout','part_submit','part_checkout','cap_saved','cap_submit','cap_deactive','cap_publish','cap_short_list','cap_checkout','cap_part_submit','cap_part_checkout','active_part','active_user'));
    }
    public function get_capacity_count($type,$clause=NULL)
    {
        $company_id=login_details('company');

        if($type=='shortlisted')
        {
            $count=DB::table('capacity as a')
            ->join('capacity_shortlist as b','b.cs_capid','=','a.id')
            //->where('b.cs_company_id',$company_id)
            ->where('b.cs_short_status','0');
            if($clause!=NULL)
            {
                $count->where('b.cs_company_id',$company_id);
            }
            
            return $count->count();
            exit;
        }
        if($type=='checout')
        {
            $count=DB::table('capacity as a')
            ->join('capacity_shortlist as b','b.cs_capid','=','a.id')
            //->where('b.cs_company_id',$company_id)
            ->where('b.cs_short_status','1');
            if($clause!=NULL)
            {
                $count->where('b.cs_company_id',$company_id);
            }
            return $count->count();
            exit;
        }
        if($type=='part_submit')
        {
            $count=DB::table('capacity')->where('status_partner','submitted')
            ->where('cap_companyid','!=',$company_id);
            
            return $count->count();
            exit;
        }
        if($type=='part_checkout')
        {
            $count=DB::table('capacity as a')
            ->join('capacity_shortlist as b','b.cs_capid','=','a.id')
            ->where('b.cs_company_id','!=',$company_id)
            ->where('b.cs_short_status','1');
           
            return $count->count();
            exit;
        }

        $count=DB::table('capacity')->where('status_admin',$type);
        if($clause!=NULL)
        {
            $count->where('cap_companyid',$company_id);
        }
        return $count->count();
    }
    public function get_requirement_count($type,$clause=NULL)
    {
        $company_id=login_details('company');
        if($type=='shortlisted')
        {
            $count=DB::table('requirment as a')
            ->join('requirement_shortlist as b','b.short_req_id','=','a.id')
            //->where('b.short_company_id',$company_id)
            ->where('b.short_status','0');
            if($clause!=NULL)
            {
              $count->where('b.short_company_id',$company_id);
            }
            
            return $count->count();
            exit;
        }
        if($type=='checout')
        {
            $count=DB::table('requirment as a')
            ->join('requirement_shortlist as b','b.short_req_id','=','a.id')
            //->where('b.short_company_id',$company_id)
            ->where('b.short_status','1');
            if($clause!=NULL)
            {
              $count->where('b.short_company_id',$company_id);
            }
            
            return $count->count();
            exit;
        }
        if($type=='part_submit')
        {
            $count=DB::table('requirment')->where('partner_status','submitted')
            ->where('company_id','!=',$company_id);
            
            return $count->count();
            exit;
        }
        if($type=='part_checkout')
        {
            $count=DB::table('requirment as a')
            ->join('requirement_shortlist as b','b.short_req_id','=','a.id')
            ->where('b.short_company_id','!=',$company_id)
            ->where('b.short_status','1');
            
            return $count->count();
            exit;
        }
        $count=DB::table('requirment')->where('admin_status',$type);
         if($clause!=NULL)
         {
           $count->where('company_id',$company_id);
         }
        
        return $count->count();
    }
    public function users_page(Request $req)
    {
        
        $FmyFunctions1 = new General;
        
       $usertype=$this->utype;
       return view('Pages.admin.users_list',compact('usertype'));
    }
    public function email_page()
    {
       $usertype=$this->utype;
       $partner=DB::table('recruiter_profiles')
       ->where('company_id','!=',login_details('company'))->where('status','1')->get();
       
       $data=DB::table('users as a')->join('company_profiles as b','a.company_id','=','b.company_id')
       ->where('a.id',login_details('uid'))->first();
       return view('Pages.admin.email_page',compact('usertype','partner','data'));
    }
    public function getEmail(Request $request)
    {
       //login_details('uid,company,requriter');
       $usertype=$this->utype;
       $status=$request->input('status');
       
       $data=DB::table('emails as a')
       ->select('a.*','b.*','c.*','sent_comp.company_name','sent_user.name',DB::raw('DATE(a.em_date) as em_date'))
       ->join('email_sent as b','b.es_email_id','=','a.em_id')
       ->join('email_receive as c','c.er_esid','=','b.es_id')
       ->join('company_profiles as sent_comp','b.es_company','=','sent_comp.company_id')
       ->join('users as sent_user','sent_user.id','=','b.es_user_id'); 
       
       if($status=='trash')
       {
         $data->whereRaw(login_details('company')." in(a.em_fromcompany,a.em_tocompany)");
       }
       if($status=='sentmail')
       {
         $data->where('a.em_fromcompany',login_details('company')); 
       }
       if($status=='inbox')
       {
         $data->where('a.em_tocompany',login_details('company')); 
       }
       $data->orderBy('a.em_date','desc');
       return datatables()->of($data->get())->toJson();      
    }
    public function addEmail(Request $request)
    {
      $partner=$request->input('partner');
      $subject=$request->input('subject');
      $message=$request->input('msg');
      $nda_path="";
      $curr_date=date('Y-m-d H:i:s');
      if($request->file('emial_file'))
       {
          
            $uploadedFile = $request->file('emial_file');
              $filename = rand();

              $nda_path='storage/app/'.Storage::disk('local')->put('Mail_file/'.$filename,$uploadedFile);
       }
        $data1=array('em_subject'=>$subject,'em_message'=>$message,'em_file'=>$nda_path,'em_tocompany'=>$partner,
          'em_fromcompany'=>login_details('company'),'em_date'=>$curr_date);

       
         DB::beginTransaction();

          try 
          {
              
             $id=DB::table('emails')->insertGetId($data1);

              $data2=array('es_user_id'=>login_details('uid'),'es_company'=>login_details('company'),
                'es_to_company'=>$partner,'es_email_id'=>$id,'es_date'=>$curr_date);

              $es_id=DB::table('email_sent')->insertGetId($data2);

              DB::table('email_receive')->insert(array('er_esid'=>$es_id));

             DB::commit();
             echo json_encode(array('type'=>'success','msg'=>'Eamil has been Sent'));
          } 
          catch (\Exception $e) 
          {
              DB::rollback();
              echo json_encode(array('type'=>'danger','msg'=>$e->getMessage()));
          }
          
    }
    public function help()
    {

      $usertype=$this->utype;
      return view('Pages.email_page.help',compact('usertype'));
    }
    public function doc_help()
    {

      $usertype=$this->utype;
      return view('Pages.email_page.help_document',compact('usertype'));
    }
    public function alter_query()
    {
      DB::statement('ALTER TABLE `newfarm`.`requirment` ADD COLUMN `qty` INT(10) NULL AFTER `updated_by`');
    }
    function updateRead(Request $request)
    {
      $id=$request->input('id');
      $action=$request->input('action');
      $from_list=$request->input('from_list');
      if($action=='read')
      {
        DB::table('email_receive')->where('er_esid',$id)->update(array('er_isread'=>1));
      }
      /*if($action=='delete')
      {
        if($from_list=='inbox')
        {
          DB::table('email_receive')->where('er_esid',$id)->update(array('er_status'=>2));
        }
        if($from_list=='sent')
        {
          DB::table('email_sent')->where('es_id',$id)->update(array('es_status'=>2));
        }
        if($from_list=='trash')
        {
          DB::table('email_sent')->where('es_id',$id)->update(array('es_status'=>2));
          DB::table('email_receive')->where('er_esid',$id)->update(array('er_status'=>2));
        }
        
      }*/
      
    }
}
