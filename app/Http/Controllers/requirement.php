<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use General;
use File;
use Redirect;

class requirement extends Controller
{
   public $utype;
   public $uid;
   public $reqruiter_id;
   public $permission;
   public $company_id;
   public function __construct()
   {
	    $this->middleware('auth');
	    $data=$this->middleware(function ($request, $next) 
	    {
	        $this->utype=Auth::user()->usertype;
	        $this->uid=Auth::user()->id;
	        $this->company_id=Auth::user()->company_id;
	        $this->reqruiter_id=Auth::user()->partner_id;
	        return $next($request);
	    });

   }
   public function requirement_page($page,$type,$requirement_id=NULL)
   {

   		$usertype=$this->utype;
   		$partner=DB::table('recruiter_profiles')->get();
      if($usertype=='Partner' || $usertype=='partner_user')
      {
          switch ($type)
          {
                case "add":
                     $data=NULL;  
                     return view('Pages.requirment_update',compact('usertype','data','partner'));
                    break;
                case "saved_list":
                    return view('Pages.client.requirement.saved_list',compact('usertype'));
                    break;
                case "saved":
                    return view('Pages.client.requirement.savedcapacity_list',compact('usertype'));
                    break;
                case "edit":
                     $data=DB::table('requirment')->where('id',$requirement_id)->first();
                    return view('Pages.requirment_update',compact('usertype','data','partner'));
                    break;
                case "submit_list":
                     return view('Pages.client.requirement.submit_list',compact('usertype'));
                    break;
                case "shortlist":
                       return view('Pages.client.requirement.short_list',compact('usertype'));
                    break;
                case "global_list":
                     return view('Pages.glob_requirement_list',compact('usertype'));
                    break;
                case "checkout_list":
                    return view('Pages.client.requirement.checkout_list',compact('usertype'));
                    break;
                case "deactive_list":
                    return view('Pages.client.requirement.deactive_list',compact('usertype'));
                    break;
                case "part_submit":
                      return view('Pages.client.requirement.part_submit_list',compact('usertype'));
                    break;
                case "part_checkout":
                      return view('Pages.client.requirement.part_checkout_list',compact('usertype'));
                    break;
                case "publish":
                      return view('Pages.client.requirement.publish_list',compact('usertype'));
                    break;
                default:
                    return view('Pages.client.requirement.submitcapacity_list',compact('usertype'));
          }
        exit;  
      }
   		switch ($type)
	    {
	          case "add":
	          	   $data=NULL;	
	               return view('Pages.requirment_update',compact('usertype','data','partner'));
	              break;
	          case "saved_list":
	              return view('Pages.admin.requirement.saved_list',compact('usertype'));
	              break;
	          case "saved":
	              return view('Pages.admin.requirement.savedcapacity_list',compact('usertype'));
	              break;
	               case "edit":
	               		$data=DB::table('requirment')->where('id',$requirement_id)->first();

	              return view('Pages.requirment_update',compact('usertype','data','partner'));
	              break;
	               case "submit_list":
	               return view('Pages.admin.requirement.submit_list',compact('usertype'));
	              break;
	               case "shortlist":
	                 return view('Pages.admin.requirement.short_list',compact('usertype'));
	              break;
	              case "global_list":
	               return view('Pages.glob_requirement_list',compact('usertype'));
	              break;
	              case "checkout_list":
	              return view('Pages.admin.requirement.checkout_list',compact('usertype'));
	              break;
	              case "deactive_list":
	              return view('Pages.admin.requirement.deactive_list',compact('usertype'));
	              break;
	              case "part_submit":
	                return view('Pages.admin.requirement.part_submit_list',compact('usertype'));
	              break;
                case "part_checkout":
                  return view('Pages.admin.requirement.part_checkout_list',compact('usertype'));
                break;
                case "publish":
                  return view('Pages.admin.requirement.publish_list',compact('usertype'));
                break;
	          default:
	              return view('Pages.admin.requirement.submitcapacity_list',compact('usertype'));
	    }
   }
   function add_requirement(Request $request)
   {
   		//login_details('uid');
   		//login_details('company');
   		$isEdit=$request->input('isEdit');
   		if($isEdit!='false')
   		{
   			$res=$this->update_requirement($request);
   			exit;
   		}
   		$title=$request->input('req_title');
   		$req_type=$request->input('req_type');
   		$primary_skill=$request->input('primary_skill');
   		$secondry_skill=$request->input('secondry_skill');
   		$req_desc=$request->input('requirment_description');

      
   		$req_qty=str_replace("_","",$request->input('requr_qty'));
   		$industry=$request->input('industry');
   		$project_type=$request->input('prj_type');
   		$work_location=$request->input('location');
   		$city_name=$request->input('city_name');
   		$project_duration=str_replace("_","",$request->input('prj_duration'));
      if($project_duration=='')
      {
        $project_duration=0;
      }
      
   		$min_exp=str_replace("_","",$request->input('min_exp'));
   		$max_exp=str_replace("_","",$request->input('max_exp'));
   		if($min_exp=='')
      {
        $min_exp=0;
      }
      if($max_exp=='')
      {
        $max_exp=0;
      }
   		$start_date=$request->input('start_date');
   		$job_file=$request->input('job_doc');
   		$source=$request->input('sourcename');
   		$source_text=$request->input('resource_val');
   		$source_partner=$request->input('partner');
   		$comment=$request->input('comments');

   		$isSaved=$request->input('isSaved');



   		$this->validate($request, 
        [
          'req_title' => 'required',
          'req_type' => 'required',
          'requirment_description' => 'required',
          'industry' => 'required',
          'location' => 'required',
          /*'sourcename' => 'required',*/
          'primary_skill'=>'required',
          /*'comments'=>'required'*/
        ],
        [
          'req_title.required' => 'Title is Required',
          'req_type.required' => 'Requirement Type is Required',
          'requirment_description.required' => 'Description is Required',
          'industry.required' => 'Industry is Required',
          'location.required' => 'Location is Required',
          /*'sourcename.required' => 'Source is Required',*/
          'primary_skill.required' => 'Primary Skills is Required',
          /*'comments.required' => 'Comments is Required'*/
        ]);
       $admin_status='pending';
       $partner_status='pending';

   		if(login_details('usertype')=='Admin' || login_details('usertype')=='admin_user')
   		{
   		  $user_type_no=0;	

        if($isSaved=='true')
        {
          $saved_status=1;
          $msg="Record Add Successfully in Saved List";
          
        }
        else if($isSaved=='false')
        {
          $saved_status=0;
          $msg='Record Successfully Submitted';
          $admin_status='submitted';
          
        }
   		}
   		else if(login_details('usertype')=='Partner' || login_details('usertype')=='partner_user')
   		{
   			$user_type_no=1;

        if($isSaved=='true')
        {
          $saved_status=1;
          $msg="Record Add Successfully in Saved List";
          
        }
        else if($isSaved=='false')
        {
          $saved_status=0;
          
          $msg='Record Successfully Submitted';
          $partner_status='submitted';
        }
   		}


   		$job_doc="";
   		if($request->file('job_doc'))
        {
          	$uploadedFile = $request->file('job_doc');
            $filename = time().$uploadedFile->getClientOriginalName();

            $job_doc='storage/app/'.Storage::disk('local')->putFileAs('Requirement_doc/'.$filename,$uploadedFile,$filename);
        }
      
   		$data=array('title'=>$title,'description'=>base64_encode($req_desc),'requirement_type'=>$req_type,'primary_skills'=>$primary_skill,'secondary_skills'=>$secondry_skill,'min_experience'=>$min_exp,'max_experience'=>$max_exp,'work_location'=>$work_location,'place'=>$city_name,'project_type'=>$project_type,'industry'=>$industry,'duration'=>$project_duration,'start_date'=>$start_date,
   			'admin_status'=>$admin_status,'partner_status'=>$partner_status,'posted_by'=>login_details('uid'),
   			'partner_id'=>login_details('requriter'),'company_id'=>login_details('company'),'saved_by'=>$user_type_no,'isSaved'=>$saved_status,'source'=>$source,'source_desc'=>$source_text,
   			'source_partner'=>$source_partner,'comment'=>$comment,'created_at'=>date('Y-m-d H:i:s'),
   			'document_path'=>$job_doc,'qty'=>$req_qty);

   		 DB::beginTransaction();
   		 try
   		 {
  			 DB::table('requirment')->insert($data);
  			 DB::select("CALL requirement_gen_id()");
  		 	 DB::commit();
		     echo json_encode(array('type'=>'success','msg'=>$msg,'isSaved'=>$isSaved));
   		 }
   		 catch(\Exception $e)
   		 {
   		 	 DB::rollback();
			   echo json_encode(array('type'=>'error','msg'=>$e->getMessage()));
   		 }


   }
   public function update_requirement($request)
   {

   		$id=$request->input('isEdit');

   	  $title=$request->input('req_title');
   		$req_type=$request->input('req_type');
   		$primary_skill=$request->input('primary_skill');
   		$secondry_skill=$request->input('secondry_skill');
   		$req_desc=$request->input('requirment_description');
   		
      
   		$req_qty=str_replace("_","",$request->input('requr_qty'));
   		$industry=$request->input('industry');
   		$project_type=$request->input('prj_type');
   		$work_location=$request->input('location');
   		$city_name=$request->input('city_name');
   		$project_duration=str_replace("_","",$request->input('prj_duration'));
      if($project_duration=='')
      {
        $project_duration=0;
      }
      
   		$min_exp=str_replace("_","",$request->input('min_exp'));
   		$max_exp=str_replace("_","",$request->input('max_exp'));
   		
      if($min_exp=='')
      {
        $min_exp=0;
      }
      if($max_exp=='')
      {
        $max_exp=0;
      }

   		$start_date=$request->input('start_date');
   		$job_file=$request->input('job_doc');
   		$source=$request->input('sourcename');
   		$source_text=$request->input('resource_val');
   		$source_partner=$request->input('partner');
   		$comment=$request->input('comments');

   		$isSaved=$request->input('isSaved');
   		if($isSaved=='true')
   		{
   			$saved_status=1;
   			$msg="Record Add Successfully in Saved List";
   		}
   		else if($isSaved=='false')
   		{
   			$saved_status=0;
   			
        $msg='Record Successfully Submitted';
   		}

   		$this->validate($request, 
        [
          'req_title' => 'required',
          'req_type' => 'required',
          'requirment_description' => 'required',
          'industry' => 'required',
          'location' => 'required',
          /*'sourcename' => 'required',*/
          'primary_skill'=>'required'
        ],
        [
          'req_title.required' => 'Title is Required',
          'req_type.required' => 'Requirement Type is Required',
          'requirment_description.required' => 'Description is Required',
          'industry.required' => 'Industry is Required',
          'location.required' => 'Location is Required',
          /*'sourcename.required' => 'Source is Required',*/
          'primary_skill.required' => 'Primary Skills is Required'
        ]);
      $admin_status="pending";
      $partner_status="pending";
   		if(login_details('usertype')=='Admin' || login_details('usertype')=='admin_user')
   		{
   		  $user_type_no=0;	
        if($isSaved=='false')
        {
          $admin_status='submitted';
        }
        
   		}
   		else if(login_details('usertype')=='Partner' || login_details('usertype')=='partner_user')
   		{
   			$user_type_no=1;
        if($isSaved=='false')
        {
          $partner_status='submitted';
        }
   		}
   		$job_doc="";

   		$old=DB::table('requirment')->where('id',$id)->first();

   		if($request->file('job_doc'))
        {
          	$uploadedFile = $request->file('job_doc');
            $filename = time().$uploadedFile->getClientOriginalName();

            $job_doc='storage/app/'.Storage::disk('local')->putFileAs('Requirement_doc/'.$filename,$uploadedFile,$filename);
            if (File::delete(public_path($old->document_path)))
            {
              
            }
        }
      
   		$data=array('title'=>$title,'description'=>base64_encode($req_desc),'requirement_type'=>$req_type,'primary_skills'=>$primary_skill,'secondary_skills'=>$secondry_skill,'min_experience'=>$min_exp,'max_experience'=>$max_exp,'work_location'=>$work_location,'place'=>$city_name,'project_type'=>$project_type,'industry'=>$industry,'duration'=>$project_duration,'start_date'=>$start_date,
   			'admin_status'=>$admin_status,'partner_status'=>$partner_status,'posted_by'=>login_details('uid'),'partner_id'=>login_details('requriter'),'company_id'=>login_details('company'),'saved_by'=>$user_type_no,'isSaved'=>$saved_status,'source'=>$source,'source_desc'=>$source_text,
   			'source_partner'=>$source_partner,'comment'=>$comment,'created_at'=>date('Y-m-d H:i:s'),
   			'document_path'=>$job_doc,'qty'=>$req_qty);

   		 DB::beginTransaction();
   		 try
   		 {
			DB::table('requirment')->where('id',$id)->update($data);
			//DB::select("CALL requirement_gen_id()");
		 	DB::commit();
		    echo json_encode(array('type'=>'success','msg'=>$msg));
   		 }
   		 catch(\Exception $e)
   		 {
   		 	DB::rollback();
			echo json_encode(array('type'=>'error','msg'=>$e->getMessage()));
   		 }

   }
   public function get_requirement_list(Request $request)
   {
   	  //login_details('uid,company,requriter');
   		$list_type=$request->input('list_type');
      $user=$request->input('user');

   		$data=DB::table('requirment as a')
   		->select('a.*','b.company_name',DB::raw('date_format(DATE(a.created_at),"%Y-%m-%d") as created'))
   		->join('company_profiles as b','a.company_id','=','b.company_id');

   		if(login_details('usertype')=='Partner' || login_details('usertype')=='partner_user' )
   		{
   			//$data->where('a.company_id',login_details('company'));
          if($list_type=='saved' && $user=='partner')
          {
            $data->where('a.isSaved','1');    
            $data->where('a.company_id',login_details('company'));    
          }
          if($list_type=='deactive' && $user=='partner')
          {
            $data->where('a.admin_status','inactive'); 
            $data->where('a.company_id',login_details('company'));      
          }
          if($list_type=='submit' && $user=='partner')
          {
            $data->where('a.isSaved','0');  
            $data->where('a.partner_status','submitted'); 
            $data->where('a.company_id',login_details('company'));     
          }
          if($user=='partner_list')
          {
            $data->where('a.company_id','!=',login_details('company'));
          }
          if($list_type=='publish')
          {
            $data->where('a.admin_status','publish'); 
            $data->where('a.partner_status','publish');  
                  
          }
          if($list_type=='publish' && $user=='partner')
          {
            $data->where('a.admin_status','publish'); 
            $data->where('a.partner_status','publish');  
            $data->where('a.company_id',login_details('company'));       
          }
   		}
      if(login_details('usertype')=='Admin' || login_details('usertype')=='admin_user' )
      {
          if($list_type=='saved' && $user=='admin')
          {
            $data->where('a.isSaved','1');    
            $data->where('a.company_id',login_details('company'));    
          }
          if($list_type=='deactive' && $user=='admin')
          {
            $data->where('a.admin_status','inactive'); 
            $data->where('a.company_id',login_details('company'));      
          }
          if($list_type=='submit' && $user=='admin')
          {
            $data->where('a.isSaved','0');  
            $data->where('a.admin_status','submitted'); 
            $data->where('a.company_id',login_details('company'));     
          }
          if($user=='partner_list')
          {
            $data->where('a.company_id','!=',login_details('company'));
          }
          if($list_type=='publish')
          {
            $data->where('a.admin_status','publish'); 
            $data->where('a.partner_status','publish');        
          }
      }

     

   		$id=$request->input('req_no');
   		$title=$request->input('req_title');
   		$skills=$request->input('req_skill');
   		$location=$request->input('req_location');

   		if(isset($id) && $id!='')
   		{
   			$data->where('requirement_id', 'like', '%' . $id . '%');
   		}
   		if(isset($title) && $title!='')
   		{
   			$data->where('title', 'like', '%' . $title . '%');
   		}
   		if(isset($skills) && $skills!='')
   		{
   			$data->where('primary_skills', 'like', '%' . $skills . '%');
   		}
   		if(isset($location) && $location!='')
   		{
   			$data->where('work_location', 'like', '%' . $location . '%');
   		}
      $data->orderBy('a.id', 'desc');

   		return datatables()->of($data->get())->toJson();
   }
   public function get_short_list(Request $request)
   {
      //login_details('uid,company,requriter');
      $list_type=$request->input('list_type');
      $user=$request->input('user');
      $short_status=$request->input('short_status');

      $data=DB::table('requirment as a')
      ->select('a.*','c.*','b.company_name',DB::raw('DATE(b.created_at) as created_at'),
      'd.name as posted_user','e.company_name as checkout_company','f.name as checkout_user',
      DB::raw('DATE(c.trs_checkout_date) as checkout_date'))

      ->join('company_profiles as b','a.company_id','=','b.company_id')
      ->join('users as d','d.id','=','a.posted_by')
      ->join('requirement_shortlist as c','c.short_req_id','=','a.id')
      ->join('company_profiles as e','e.company_id','=','c.short_company_id')
      ->join('users as f','f.id','=','c.short_uid');
      if(login_details('usertype')=='Admin' || login_details('usertype')=='admin_user' )
      {
          if($short_status==0)
          {
            $data->where('short_status','0');
          }
          else if($short_status==1)
          {
            $data->where('short_status','1');
          }
          
          if(login_details('usertype')=='Partner' || login_details('usertype')=='partner_user')
          {
            $data->where('a.company_id',login_details('company'));
          }
          if( $user=='partner_list')
          {
             $data->where('a.company_id','!=',login_details('company'));
          }
          if($list_type=='shortlist' && $user=='admin')
          {
            $data->where('a.admin_status','publish');      
          }
          else if($list_type=='shortlist' && $user=='partner')
          {
            $data->where('a.partner_status','publish');      
          }
      }
      if(login_details('usertype')=='Partner' || login_details('usertype')=='partner_user' )
      {
          if($short_status==0)
          {
            $data->where('short_status','0');
          }
          else if($short_status==1)
          {
            $data->where('short_status','1');
          }
          $data->where('c.short_company_id',login_details('company'));
          
          if( $user=='partner_list')
          {
             $data->where('a.company_id','!=',login_details('company'));
          }
          if($list_type=='shortlist' && $user=='partner')
          {
            $data->where('a.admin_status','publish');      
          }
          else if($list_type=='shortlist' && $user=='partner')
          {
            $data->where('a.partner_status','publish');      
          }
      }
      

      $id=$request->input('req_no');
      $title=$request->input('req_title');
      $skills=$request->input('req_skill');
      $location=$request->input('req_location');

      if(isset($id) && $id!='')
      {
        $data->where('a.requirement_id', 'like', '%' . $id . '%');
      }
      if(isset($title) && $title!='')
      {
        $data->where('a.title', 'like', '%' . $title . '%');
      }
      if(isset($skills) && $skills!='')
      {
        $data->where('a.primary_skills', 'like', '%' . $skills . '%');
      }
      if(isset($location) && $location!='')
      {
        $data->where('a.work_location', 'like', '%' . $location . '%');
      }

      return datatables()->of($data->get())->toJson();
   }
   public function requirement_detail_page(Request $request,$id)
   {
   	 $usertype=$this->utype;
   	 $data=DB::table('requirment as a')
   	 ->select('a.*','b.company_name','c.name')
   	 ->join('company_profiles as b','a.company_id','=','b.company_id')
   	 ->join('users as c','c.id','=','a.posted_by')
   	 ->where('a.id',$id)->first();
   	 $company=login_details('company');
   	 $isPublish='true';

   	 return view('Pages.requirement_detal_view',compact('usertype','data','company','isPublish'));	
   }
   public function update_requirement_status($action,$id)
   {
   	 
   	   	if($action=='shortlisted')
   	  	{
           $data=DB::table('requirment')->where('id',$id)->whereIn('admin_status',array('inactive','publish'))->first();
           if(!empty($data))
           {
     	  		$arr=array('short_req_id'=>$id,'short_partner_id'=>login_details('requriter'),'short_company_id'=>login_details('company'),'short_uid'=>login_details('uid'),
     	  			'short_date'=>date('Y-m-d H:i:s'),'short_status'=>0);

     	  		$check=DB::table('requirement_shortlist')->where('short_req_id',$id)->where('short_company_id',login_details('company'))->first();

     	  		if(!empty($check))
     	  		{
     	  			
              return redirect()->back()->with('error','This Requirement already shortlisted');
     	  		}
     	  		$result=DB::table('requirement_shortlist')->insert($arr);
     	  		if($result)
     	  		{
     	  			return redirect()->back()->with('success','Requirement Shortlisted Successful');
     	  		}
         }
   	  	}
        else
        {
          $upd_arr=array(); 
          if(login_details('usertype')=='Admin' || login_details('usertype')=='admin_user')
          {
            if($action=='publish')
            {
              $upd_arr['admin_status']=$action;
              $upd_arr['partner_status']=$action;  
            }
            else
            {
              $upd_arr['admin_status']=$action;
            }
            
          }
          if(login_details('usertype')=='Partner' || login_details('usertype')=='partner_user')
          {
            $upd_arr['partner_status']=$action;
          }
          DB::table('requirment')->where('id',$id)->update($upd_arr);   
        }
   	    
   	 
   	  return redirect()->back()->with('success','Requirement Update Successful');
   	  //return Redirect::back();
   }
   function update_shortlist(Request $request)
   {
      $page=$request->input('page');
      if(isset($page))
      {
        $id=$request->input('trs_id');
        $workstatus=$request->input('workstatus');
        $priority=$request->input('priority');
        $closereason=$request->input('closereason');
        $status=$request->input('final_status');

       

        $isClose=0;
        $shortlist_data=DB::table('requirement_shortlist')->select('*')->where('short_id',$id)->first();
        if($closereason=='SOW Signed and Resource Onboarded' && $status=='Close')
        {
          $isClose=1;
          
          $result2=DB::table('requirment')->where('id',$shortlist_data->short_req_id)->update(array('admin_status'=>"close"));


        }
        else
        {
           /*$result2=DB::table('requirment')->where('id',$shortlist_data->short_req_id)->update(array('admin_status'=>"shortlisted"));*/
        }

        $data=array('short_admin_status'=>$workstatus,'short_admin_priority'=>$priority,'short_admin_close_reason'=>$closereason,
          'short_final_status'=>$status,'short_udpate_date'=>date('Y-m-d H:i:s'),'short_final_close'=>$isClose);

        /*$data2=array('trh_trsid'=>$trans_id,'trh_date'=>date('Y-m-d H:i:s'),'trh_workstatus'=>$workstatus,'trh_priority'=>$priority,'trh_close_status'=>$closereason,'trh_uid'=>$this->uid,'trh_company_id'=>$this->company_id);*/

        $result=DB::table('requirement_shortlist')->where('short_id',$id)->update($data);
        //$result1=DB::table('tbl_requirment_shortlist_history')->insert($data2);


        if($result)
        {
          echo json_encode(array('type'=>'success','msg'=>'Data Submitted Successfull'));
        }
        else
        {
          echo json_encode(array('type'=>'error','msg'=>'Data Submitted Unsuccessfull'));
        }
        exit;
      }

      $arr=json_decode($request->getContent());
      foreach($arr as $key=>$val)
      {
        $data=array('short_status'=>1,'trs_checkout_date'=>date('Y-m-d H:i:s'));

        DB::table('requirement_shortlist')->where('short_id',$val)->update($data);
      }
      echo json_encode('Requirment has been checkedout');
   }
   function checkout_details($short_id)
   {

    $data=DB::table('requirment as a')
      ->select('a.*','c.*','b.company_name as posted_company',DB::raw('DATE(b.created_at) as created_at'),'d.name as posted_user','e.company_name as checkout_company','f.name as checkout_user',
      DB::raw('DATE(c.trs_checkout_date) as checkout_date'),'b.contact_number as posted_contact',
      'b.email as posted_email','b.contact_name as posted_contactname','e.contact_number as checkout_contact','e.email as checkout_email','e.contact_name as checkout_contactname')

      ->join('company_profiles as b','a.company_id','=','b.company_id')
      ->join('users as d','d.id','=','a.posted_by')
      ->join('requirement_shortlist as c','c.short_req_id','=','a.id')
      ->join('company_profiles as e','e.company_id','=','c.short_company_id')
      ->join('users as f','f.id','=','c.short_uid')
      ->where('c.short_id',$short_id)->first();

     return view('Pages.admin.requirement.checkout_details',compact('usertype','data'));
   }
}
