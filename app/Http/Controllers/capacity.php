<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use General;
use File;
use Redirect;

class capacity extends Controller
{
   public $utype;
   public $uid;
   public $reqruiter_id;
   public $permission;
   public $company_id;
   public function __construct()
   {
	    $this->middleware('auth');
	    $data=$this->middleware(function ($request, $next) 
	    {
	        $this->utype=Auth::user()->usertype;
	        $this->uid=Auth::user()->id;
	        $this->company_id=Auth::user()->company_id;
	        $this->reqruiter_id=Auth::user()->partner_id;
	        return $next($request);
	    });

   }
    public function capacity_page($page,$type,$cap_id=NULL)
	{

   		$usertype=$this->utype;
   		$company=login_details('company');
   		$partner=DB::table('recruiter_profiles')->where('company_id','!=',login_details('company'))->get();

   		
   		if(login_details('usertype')=='Partner' || login_details('usertype')=='partner_user')
   		{
   			switch ($type)
		    {
		       case "add":
		          $data=NULL;	
		          return view('Pages.capacity_update',compact('usertype','data','partner'));
		       break;

		       case "saved":
		          return view('Pages.client.capacity.saved_list',compact('usertype'));
		       break;

		       case "edit":

		          $data=DB::table('capacity')->where('id',$cap_id)->first();
		          $education=DB::table('cv_educaton')->where('ce_capid',$cap_id)->get();
				  return view('Pages.capacity_update',compact('usertype','data','partner','education'));
				  exit;
		       break;
		       case "viewpage":

		          $data=DB::table('capacity as a')
		          ->select('a.*','b.name as part_name','c.company_name as comp_name','d.name as user_name')
		          ->Leftjoin('recruiter_profiles as b','a.source_part','=','b.recruiter_id')
		          ->Leftjoin('company_profiles as c','a.cap_companyid','=','c.company_id')
		          ->Leftjoin('users as d','a.posted_by','=','d.id')
		          ->where('a.id',$cap_id)->first();

		          $education=DB::table('cv_educaton')->where('ce_capid',$cap_id)->get();
				  return view('Pages.capacity_detail_view',compact('usertype','data','partner','education','company'));
		       break;
		       case "submit":
		          return view('Pages.client.capacity.submit_list',compact('usertype'));
		       break;

		       case "global_list":
		          return view('Pages.glob_capacity_list',compact('usertype'));
		       break;

		       case "deactive":
		          return view('Pages.client.capacity.deactive_list',compact('usertype'));
		       break;

		       case "shortlist":
		          return view('Pages.client.capacity.short_list',compact('usertype'));
		       break;

		       case "checkout":
		          return view('Pages.client.capacity.checkout_list',compact('usertype'));
		       break;

		       case "partner_capacity_publish":
		          return view('Pages.client.capacity.part_submit_list',compact('usertype'));
		       break;
		       case "partner_capacity_checkout":
		          return view('Pages.client.capacity.part_checkout_list',compact('usertype'));
		       break;
		        case "publish":
		          return view('Pages.client.capacity.publish_list',compact('usertype'));
		       break;

		       default:
		          return view('Pages.client.requirement.submitcapacity_list',compact('usertype'));
		    }
   			exit;
   		}
   		switch ($type)
	    {
	       case "add":
	          $data=NULL;	
	          return view('Pages.capacity_update',compact('usertype','data','partner'));
	       break;

	       case "saved":
	          return view('Pages.admin.capacity.saved_list',compact('usertype'));
	       break;

	       case "edit":

	          $data=DB::table('capacity')->where('id',$cap_id)->first();
	          $education=DB::table('cv_educaton')->where('ce_capid',$cap_id)->get();
			  return view('Pages.capacity_update',compact('usertype','data','partner','education'));
			  exit;
	       break;
	       case "viewpage":

	          $data=DB::table('capacity as a')
	          ->select('a.*','b.name as part_name','c.company_name as comp_name','d.name as user_name')
	          ->Leftjoin('recruiter_profiles as b','a.source_part','=','b.recruiter_id')
	          ->Leftjoin('company_profiles as c','a.cap_companyid','=','c.company_id')
	          ->Leftjoin('users as d','a.posted_by','=','d.id')
	          ->where('a.id',$cap_id)->first();

	          $education=DB::table('cv_educaton')->where('ce_capid',$cap_id)->get();
			  return view('Pages.capacity_detail_view',compact('usertype','data','partner','education','company'));
	       break;
	       case "submit":
	          return view('Pages.admin.capacity.submit_list',compact('usertype'));
	       break;

	       case "global_list":
	          return view('Pages.glob_capacity_list',compact('usertype'));
	       break;

	       case "deactive":
	          return view('Pages.admin.capacity.deactive_list',compact('usertype'));
	       break;

	       case "shortlist":
	          return view('Pages.admin.capacity.short_list',compact('usertype'));
	       break;

	       case "checkout":
	          return view('Pages.admin.capacity.checkout_list',compact('usertype'));
	       break;

	       case "partner_capacity_publish":
	          return view('Pages.admin.capacity.part_submit_list',compact('usertype'));
	       break;
	       case "partner_capacity_checkout":
	          return view('Pages.admin.capacity.part_checkout_list',compact('usertype'));
	       break;
	        case "publish":
	          return view('Pages.admin.capacity.publish_list',compact('usertype'));
	       break;

	       default:
	          return view('Pages.admin.requirement.submitcapacity_list',compact('usertype'));
	    }
	}
	public function get_short_list(Request $request)
	{
		//login_details('uid,company,requriter');

	      $list_type=$request->input('list_type');
	      $user=$request->input('user');
	      $short_status=$request->input('short_status');

	      $data=DB::table('capacity as a')
	      ->select('c.*','c.cs_id','a.capacity_no','a.title','a.primary_skills','a.experience'
	      	,DB::raw('DATE(c.cs_short_date) as cs_short_date'),'a.typeofengae','c.cs_short_status',
	      	'c.cs_id','b.company_name','d.name as posted_user','e.company_name as checkout_company',
	      	'f.name as checkout_user',DB::raw('DATE(c.cs_app_checkout) as checkout_date'),'a.experience')

	      ->join('company_profiles as b','a.cap_companyid','=','b.company_id')
	      ->join('users as d','d.id','=','a.posted_by')
	      ->join('capacity_shortlist as c','c.cs_capid','=','a.id')
	      ->join('company_profiles as e','e.company_id','=','c.cs_company_id')
	      ->join('users as f','f.id','=','c.cs_uid');
	      if($short_status==0)
	      {
	        $data->where('cs_short_status','0');
	      }
	      else if($short_status==1)
	      {
	        $data->where('cs_short_status','1');
	      }
	      
	      if(login_details('usertype')=='Partner' || login_details('usertype')=='partner_user')
	      {
	        $data->where('a.cap_companyid',login_details('company'));
	      }
	      if( $user=='partner_list')
	      {
	         $data->where('a.cap_companyid','!=',login_details('company'));
	      }
	      if($list_type=='shortlist' && $user=='admin')
	      {
	        $data->where('a.status_admin','publish');      
	      }
	      else if($list_type=='shortlist' && $user=='partner')
	      {
	        $data->where('a.status_partner','publish');      
	      }

	      $id=$request->input('cap_no');
	      $title=$request->input('cap_title');
	      $skills=$request->input('cap_skill');
	      $type=$request->input('cap_type');
		  if(isset($id) && $id!='')
	      {
	        $data->where('a.capacity_no', $id);
	      }
	      if(isset($title) && $title!='')
	      {
	        $data->where('a.title', 'like', '%' . $title . '%');
	      }
	      if(isset($skills) && $skills!='')
	      {
	        $data->where('a.primary_skills', 'like', '%' . $skills . '%');
	      }
	      if(isset($type) && $type!='')
	      {
	        $data->where('a.isFresher', $type);
	      }
	      
	      $data->orderBy('a.id', 'desc');
	      return datatables()->of($data->get())->toJson();
	}
	function update_shortlist(Request $request)
   {
      $page=$request->input('page');
      if(isset($page))
      {
        $id=$request->input('trs_id');
        $workstatus=$request->input('workstatus');
        $priority=$request->input('priority');
        $status=$request->input('final_status');

       

        $isClose=0;
        $shortlist_data=DB::table('capacity_shortlist')->select('*')->where('cs_id',$id)->first();
        if($closereason=='SOW Signed and Resource Onboarded' && $status=='Close')
        {
          $isClose=1;
          
          $result2=DB::table('capacity')->where('id',$shortlist_data->cs_capid)->update(array('status_admin'=>"close"));


        }
        else
        {
           /*$result2=DB::table('requirment')->where('id',$shortlist_data->short_req_id)->update(array('admin_status'=>"shortlisted"));*/
        }

        $data=array('cs_admin_workstatus'=>$workstatus,'cs_admin_priority'=>$priority,
          'cs_admin_finalstatus'=>$status,'cs_updated_date'=>date('Y-m-d H:i:s'),'cs_updated_uid'=>login_details('uid'));

        /*$data2=array('trh_trsid'=>$trans_id,'trh_date'=>date('Y-m-d H:i:s'),'trh_workstatus'=>$workstatus,'trh_priority'=>$priority,'trh_close_status'=>$closereason,'trh_uid'=>$this->uid,'trh_company_id'=>$this->company_id);*/

        $result=DB::table('capacity_shortlist')->where('cs_id',$id)->update($data);
        //$result1=DB::table('tbl_requirment_shortlist_history')->insert($data2);


        if($result)
        {
          echo json_encode(array('type'=>'success','msg'=>'Data Submitted Successfull'));
        }
        else
        {
          echo json_encode(array('type'=>'error','msg'=>'Data Submitted Unsuccessfull'));
        }
        exit;
      }

      $arr=json_decode($request->getContent());
      foreach($arr as $key=>$val)
      {
        $data=array('cs_short_status'=>1,'cs_app_checkout'=>date('Y-m-d H:i:s'));

        DB::table('capacity_shortlist')->where('cs_id',$val)->update($data);
      }
      echo json_encode('Capacity has been checkedout');
   }
	public function add(Request $request)
	{
		

	  	
	  $isExperience=$request->input('isExperinece');

	  if($isExperience=='true')
	  {
	  	$exp_isUpdate=$request->input('exp_isUpdate');
	  	if($exp_isUpdate=='true')
	  	{
	  		$result=$this->capacity_update($request);
	  		echo $result;
	  		exit;
	  	}
	  	else if($exp_isUpdate=='false')
	  	{
	  		$result=$this->capacity_add($request);
	  		echo $result;
	  		exit;
	  	}

	  }
	  else if($isExperience=='false')
	  {
	  	$fresher_isUpdate=$request->input('fresher_isUpdate');
	  	if($fresher_isUpdate=='true')
	  	{
	  		$result=$this->capacity_update($request);
	  		echo $result;
	  		exit;
	  	}
	  	else if($fresher_isUpdate=='false')
	  	{
	  		$result=$this->capacity_add($request);
	  		echo $result;
	  		exit;
	  	}
	  }
	}
	public function capacity_add($request)
	{
		//login_details('company,usertype,requriter,uid');
		$isExperinece=$request->input('isExperinece');
		$data=array();
		
		    $title=$request->input('title');
			$desc=$request->input('description');
			$cap_type=$request->input('cap_type');
			$industry=$request->input('industry');
			$pref_location=$request->input('location');
			$pref1_location=$request->input('location_1');
			$onboard=$request->input('onboard');
			$duration=$request->input('duration');
			$comment=$request->input('comments');
			
			$source=$request->input('sourcename');
			$soruce_partner=$request->input('partner');
			$source_desc=$request->input('resource_val');

			$name=$request->input('name');
			$mobile=$request->input('mobile');
			$email=$request->input('email');
			$dob=$request->input('dob');
			$address=$request->input('address');

			$qualificatin=$request->input('qualification');
			$course=$request->input('course');
			$specilization=$request->input('specilization');
			$university=$request->input('univerisity');
			$course_type=$request->input('coursetype');
			$passing_year=$request->input('passingyear');
			$mark_type=$request->input('marktype');
			$mark_desc=$request->input('mark_desc');

			$saved_status=0;
			$admin_status='submit';
			$isSaved=$request->input('exp_isSaved');
			$list_type='submit';
			if($isSaved=='true')
			{
			  $saved_status=1;	
			  $admin_status='saved';
			  $list_type='saved';
			}
			

	        $total_exp=$request->input('totalexp');
	        $comp_name=$request->input('company_name');
	        $designation=$request->input('designation');
	        $annualsalary=$request->input('salary');
	        $workin_since=$request->input('worksince');
	        $location=$request->input('curr_location');
	        $primary_skills=$request->input('pskill');
	        $secondary_skills=$request->input('sskill');
	        $function_area=$request->input('function');
	        $onsite_exp=$request->input('onsite_exp');
	        $tech_track=$request->input('technology');
	        $type_engagment=$request->input('engagement');

	        $isFresher=0;
	        
	        if($isExperinece=='true')
	        {
	        	$isFresher=1;
	        }

	        $this->validate($request, 
	        [
	          'title' => 'required',
	          'description' => 'required',
	          'cap_type' => 'required',
	          'industry' => 'required',
	          'onboard' => 'required',
	          /*'pskill'=>'required',*/
	          /*'engagement'=>'required',*/
	          'name' => 'required',
	          'mobile' => 'required',
	          'email' => 'required',
	        
	        ],
	        [
	            'title.required' => 'Title is Required',
		        'description.required' => 'Description is Required',
		        'cap_type.required' => 'Capacity Type is Required',
		        'industry.required' => 'Industry is Required',
		        'onboard.required' => 'On board is Required',
		        /*'pskill.required'=>'Primary Skills is Required',  */
		        /*'engagement.required'=>'Engagement is Required',*/
		        'name.required' => 'Name is Required',
		        'mobile.required' => 'Mobile is Required',
		        'email.required' => 'Email is Required',
		       
	        ]);

		if($isExperinece=='true')
		{
			

	        $data=array('title'=>$title,'description'=>base64_encode($desc),'capacity_type'=>$cap_type,'primary_skills'=>$primary_skills,'industry'=>$industry,'onboard'=>$onboard,
	        	'secondary_skills'=>$secondary_skills,'experience'=>$total_exp,'preferred_location'=>$pref_location,'comapany_name'=>$comp_name,
	        	'preferred_location1'=>$pref1_location,'duration_of_availability'=>$duration,'comments'=>$comment,
	        	'posted_by'=>login_details('uid'),'partner_id'=>login_details('requriter'),'cap_companyid'=>login_details('company'),
	        	'name'=>$name,'email'=>$email,'mobile'=>$mobile,'dob'=>$dob,'address'=>$address,'source'=>$source,'source_desc'=>$source_desc,'source_part'=>$soruce_partner,'designation'=>$designation,'annaulsalary'=>$annualsalary,'workingsince'=>$workin_since,'curr_location'=>$location,'func_area'=>$function_area,'onsite_experience'=>$onsite_exp,'tech_track'=>$tech_track,'typeofengae'=>$type_engagment,'isFresher'=>$isFresher,'isSaved'=>$saved_status);
	    }
		else if($isExperinece=='false')
		{
			$data=array('title'=>$title,'description'=>base64_encode($desc),'capacity_type'=>$cap_type,'preferred_location'=>$pref_location,'onboard'=>$onboard,
	        	'preferred_location1'=>$pref1_location,'duration_of_availability'=>$duration,'comments'=>$comment,'industry'=>$industry,
	        	'posted_by'=>login_details('uid'),'partner_id'=>login_details('requriter'),'cap_companyid'=>login_details('company'),
	        	'name'=>$name,'email'=>$email,'mobile'=>$mobile,'dob'=>$dob,'address'=>$address,'source'=>$source,'source_desc'=>$source_desc,'source_part'=>$soruce_partner,'isFresher'=>$isFresher,'isSaved'=>$saved_status);
	    }
	    
	    if(login_details('usertype')=='Admin' || login_details('usertype')=='admin_user')
	    {
	    	$data['status_admin']=$admin_status;
	    	$data['status_partner']="saved";
	    }
	    else if(login_details('Partner') || login_details('partner_user'))
	    {
	    	$data['status_admin']="saved";
	    	$data['status_partner']=$admin_status;
	    }
	       try
           {
             
              DB::beginTransaction();
              
              $cap_id=DB::table('capacity')->insertGetId($data);
              DB::select("CALL capacity_gen_id()");
              for($i=0; $i<=count($qualificatin)-1; $i++)
		      {
		        $qual_arr=array('ce_capid'=>$cap_id,'ce_qualification'=>$qualificatin[$i],'ce_course'=>$course[$i],'ce_specilization'=>$specilization[$i],'ce_course_type'=>$course_type[$i],'ce_university'=>$university[$i],'ce_marktype'=>$mark_type[$i],'ce_markdesc'=>$mark_desc[$i],'ce_passyear'=>$passing_year[$i],'ce_date'=>date('Y-m-d H:i:s'),'ce_company_id'=>login_details('company'));
		        DB::table('cv_educaton')->insert($qual_arr);
		      }
		        
              DB::commit();
              return json_encode(array('type'=>'success','msg'=>'Data Add Succesfull','list_type'=>$list_type));
           }
           catch(\Exception $e)
           {
              DB::rollback();
              return  json_encode(array('type'=>'error','msg'=>$e->getMessage()));
           }
	}
	public function capacity_update($request)
	{
		//login_details('company,usertype,requriter,uid');
		$isExperinece=$request->input('isExperinece');
		$data=array();
			
			$id=$request->input('capid');

		    $title=$request->input('title');
			$desc=$request->input('description');

			$cap_type=$request->input('cap_type');
			$industry=$request->input('industry');
			$pref_location=$request->input('location');
			$pref1_location=$request->input('location_1');
			$onboard=$request->input('onboard');
			$duration=$request->input('duration');
			$comment=$request->input('comments');

			$source=$request->input('sourcename');
			$soruce_partner=$request->input('partner');
			$source_desc=$request->input('resource_val');

			$name=$request->input('name');
			$mobile=$request->input('mobile');
			$email=$request->input('email');
			$dob=$request->input('dob');
			$address=$request->input('address');

			$qualificatin=$request->input('qualification');
			$course=$request->input('course');
			$specilization=$request->input('specilization');
			$university=$request->input('univerisity');
			$course_type=$request->input('coursetype');
			$passing_year=$request->input('passingyear');
			$mark_type=$request->input('marktype');
			$mark_desc=$request->input('mark_desc');

			$saved_status=0;
			$admin_status='submit';
			$isSaved=$request->input('exp_isSaved');
			
			$list_type='submit';
			if($isSaved=='true')
			{
			  $saved_status=1;	
			  $admin_status='saved';
			  $list_type='saved';
			}
			

	        $total_exp=$request->input('totalexp');
	        $comp_name=$request->input('company_name');
	        $designation=$request->input('designation');
	        $annualsalary=$request->input('salary');
	        $workin_since=$request->input('worksince');
	        $location=$request->input('curr_location');
	        $primary_skills=$request->input('pskill');
	        $secondary_skills=$request->input('sskill');
	        $function_area=$request->input('function');
	        $onsite_exp=$request->input('onsite_exp');
	        $tech_track=$request->input('technology');
	        $type_engagment=$request->input('engagement');

	        $isFresher=0;
	        
	        if($isExperinece=='true')
	        {
	        	$isFresher=1;
	        }

	        $this->validate($request, 
	        [
	          'title' => 'required',
	          'description' => 'required',
	          'cap_type' => 'required',
	          'industry' => 'required',
	          'onboard' => 'required',
	          /*'pskill'=>'required',*/

	          'name' => 'required',
	          'mobile' => 'required',
	          'email' => 'required',
	          /*'engagement'=>'required',*/
	        
	        ],
	        [
	            'title.required' => 'Title is Required',
		        'description.required' => 'Description is Required',
		        'cap_type.required' => 'Capacity Type is Required',
		        'industry.required' => 'Industry is Required',
		        'onboard.required' => 'On board is Required',
		        /*'pskill.required'=>'Primary Skills is Required',  */
		        /*'engagement.required'=>'Engagement is Required',*/
		        'name.required' => 'Name is Required',
		        'mobile.required' => 'Mobile is Required',
		        'email.required' => 'Email is Required',
		       
	        ]);

		if($isExperinece=='true')
		{
			

	        $data=array('title'=>$title,'description'=>base64_encode($desc),'capacity_type'=>$cap_type,'primary_skills'=>$primary_skills,'onboard'=>$onboard,
	        	'secondary_skills'=>$secondary_skills,'experience'=>$total_exp,'preferred_location'=>$pref_location,'industry'=>$industry,
	        	'preferred_location1'=>$pref1_location,'duration_of_availability'=>$duration,'comments'=>$comment,'comapany_name'=>$comp_name,
	        	'posted_by'=>login_details('uid'),'partner_id'=>login_details('requriter'),'cap_companyid'=>login_details('company'),
	        	'name'=>$name,'email'=>$email,'mobile'=>$mobile,'dob'=>$dob,'address'=>$address,'source'=>$source,'source_desc'=>$source_desc,'source_part'=>$soruce_partner,'designation'=>$designation,'annaulsalary'=>$annualsalary,'workingsince'=>$workin_since,'curr_location'=>$location,'func_area'=>$function_area,'onsite_experience'=>$onsite_exp,'tech_track'=>$tech_track,'typeofengae'=>$type_engagment,'isFresher'=>$isFresher,'isSaved'=>$saved_status);
	    }
		else if($isExperinece=='false')
		{
			$data=array('title'=>$title,'description'=>$desc,'capacity_type'=>$cap_type,'preferred_location'=>$pref_location,'onboard'=>$onboard,
	        	'preferred_location1'=>$pref1_location,'duration_of_availability'=>$duration,'comments'=>$comment,'industry'=>$industry,
	        	'posted_by'=>login_details('uid'),'partner_id'=>login_details('requriter'),'cap_companyid'=>login_details('company'),
	        	'name'=>$name,'email'=>$email,'mobile'=>$mobile,'dob'=>$dob,'address'=>$address,'source'=>$source,'source_desc'=>$source_desc,'source_part'=>$soruce_partner,'isFresher'=>$isFresher,'isSaved'=>$saved_status);
	    }

	    if(login_details('usertype')=='Admin' || login_details('usertype')=='admin_user')
	    {
	    	$data['status_admin']=$admin_status;
	    	$data['status_partner']="saved";
	    }
	    else if(login_details('Partner') || login_details('partner_user'))
	    {
	    	$data['status_admin']="saved";
	    	$data['status_partner']=$admin_status;
	    }
	       try
           {
             
              DB::beginTransaction();
              
              DB::table('capacity')->where('id',$id)->update($data);
			  DB::table('cv_educaton')->where('ce_capid',$id)->delete();

              for($i=0; $i<=count($qualificatin)-1; $i++)
		      {
		        $qual_arr=array('ce_capid'=>$id,'ce_qualification'=>$qualificatin[$i],'ce_course'=>$course[$i],'ce_specilization'=>$specilization[$i],'ce_course_type'=>$course_type[$i],'ce_university'=>$university[$i],'ce_marktype'=>$mark_type[$i],'ce_markdesc'=>$mark_desc[$i],'ce_passyear'=>$passing_year[$i],'ce_date'=>date('Y-m-d H:i:s'),'ce_company_id'=>login_details('company'));
		        DB::table('cv_educaton')->insert($qual_arr);
		      }
		        
              DB::commit();
              return json_encode(array('type'=>'success','msg'=>'Data Update Succesfull','list_type'=>$list_type));
           }
           catch(\Exception $e)
           {
              DB::rollback();
              return  json_encode(array('type'=>'error','msg'=>$e->getMessage()));
           }
	}
	public function get_capacity_list(Request $request)
	{
		//login_details('uid,company,requriter');
   		$list_type=$request->input('list_type');
   		$user=$request->input('user');

   		$data=DB::table('capacity as a')
   		->select('a.*','b.company_name',DB::raw('DATE(a.created_at) as created_at'),'c.name as part_name')
   		->join('company_profiles as b','a.cap_companyid','=','b.company_id')
   		->Leftjoin('recruiter_profiles as c','a.source_part','=','c.recruiter_id');

   		if(login_details('usertype')=='Partner' || login_details('usertype')=='partner_user')
   		{
   			
   			if($list_type=='saved' && $user=='partner')
	   		{
	   			$data->where('a.isSaved','1');
	   			$data->where('a.status_admin','saved');
	   			$data->where('a.cap_companyid',login_details('company'));
	   		}

	   		if($list_type=='submit' && $user=='partner')
	   		{
	   			$data->where('a.isSaved','0');
	   			$data->where('a.status_partner','submit');
	   			$data->where('a.cap_companyid',login_details('company'));
	   		}

	   		if($list_type=='deactive' && $user=='partner')
		    {
		        $data->where('a.status_admin','inactive'); 
		        $data->where('a.cap_companyid',login_details('company'));  
		        $data->where('a.cap_companyid',login_details('company'));    
		    }

		    if($list_type=='publish' && $user=='partner')
		    {
		        $data->where('a.status_admin','publish'); 
		        $data->where('a.cap_companyid',login_details('company'));
		    }

	   		if($list_type=='publish' && $user=='global')
	   		{
	   			//$data->where('a.cap_companyid','!=',login_details('company'));
	   			$data->where('a.status_admin','publish'); 
	        	$data->where('a.status_partner','publish');
	   		}
   		}
   		if(login_details('usertype')=='Admin' || login_details('usertype')=='admin_user')
   		{
   			//$data->where('a.cap_companyid',login_details('company'));
   			if($list_type=='saved' && $user=='admin')
	   		{
	   			$data->where('a.isSaved','1');
	   			$data->where('a.status_admin','saved');
	   			$data->where('a.cap_companyid',login_details('company'));
	   		}

	   		if($list_type=='submit' && $user=='admin')
	   		{
	   			$data->where('a.isSaved','0');
	   			$data->where('a.status_admin','submit');
	   			$data->where('a.cap_companyid',login_details('company'));
	   		}

	   		if($list_type=='deactive' && $user=='admin')
		    {
		        $data->where('a.status_admin','inactive'); 
		        $data->where('a.cap_companyid',login_details('company'));      
		    }

		    if($list_type=='submit' && $user=='partner_list')
		    {
		        $data->where('a.status_partner','submit'); 
		        $data->where('a.cap_companyid','!=',login_details('company'));      
		    }
		    if($list_type=='publish' && $user=='admin')
		    {
		        $data->where('a.status_admin','publish'); 
		        $data->where('a.cap_companyid',login_details('company'));      
		    }

	   		if($list_type=='publish' && $user=='global')
	   		{
	   			//$data->where('a.cap_companyid','!=',login_details('company'));
	   			$data->where('a.status_admin','publish'); 
	        	$data->where('a.status_partner','publish');
	   		}
   		}
   		

   		$id=$request->input('cap_no');
   		$title=$request->input('cap_title');
   		$skills=$request->input('cap_skill');
   		$type=$request->input('cap_type');
   		if(isset($id) && $id!='')
   		{
   			$data->where('a.capacity_no', $id);
   		}
   		if(isset($title) && $title!='')
   		{
   			$data->where('a.title', 'like', '%' . $title . '%');
   		}
   		if(isset($skills) && $skills!='')
   		{
   			$data->where('a.primary_skills', 'like', '%' . $skills . '%');
   		}
   		if(isset($type) && $type!='')
   		{
   			$data->where('a.isFresher',$type);
   		}
   		$data->orderBy('a.id', 'desc');
   		return datatables()->of($data->get())->toJson();
	}
	public function update_capacity_status($action,$id)
	{

		
   	  	if($action=='shortlisted')
   	  	{
   	  		$data=DB::table('capacity')->where('id',$id)->whereIn('status_admin',array('inactive','publish'))->first();
	   	  if(!empty($data))
	   	  {
   	  		$arr=array('cs_capid'=>$id,'cs_company_id'=>login_details('company'),'cs_uid'=>login_details('uid'),'cs_short_status'=>0,'cs_short_date'=>date('Y-m-d H:i:s'));

   	  		$check=DB::table('capacity_shortlist')->where('cs_capid',$id)->where('cs_company_id',login_details('company'))->first();

   	  		if(!empty($check))
   	  		{
   	  			return redirect()->back()->with('error','This Requirement already Shortlisted'); 
   	  		}
   	  		$result=DB::table('capacity_shortlist')->insert($arr);
   	  		if($result)
   	  		{
   	  			return redirect()->back()->with('success','Capacity Shortlisted Successful'); 
   	  		}
   	      }	
   	  	}
   	  	else
   	  	{
   	  	   $upd_arr=array();	
   	  	  if(login_details('usertype')=='Admin' || login_details('usertype')=='admin_user')
   	  	  {
   	  	  	$upd_arr['status_admin']=$action;
   	  	  }
   	  	  if(login_details('usertype')=='Partner' || login_details('usertype')=='partner_user')
   	  	  {
   	  	  	$upd_arr['status_partner']=$action;
   	  	  }
   	  	  if($action=='publish')
   	  	  {
   	  	  	$upd_arr['status_partner']=$action;
   	  	  	$upd_arr['status_admin']=$action;
   	  	  }
   	  	  DB::table('capacity')->where('id',$id)->update($upd_arr);		
   	  	}
   	   return redirect()->back()->with('success','Capacity Update Successful');  
   	  //return Redirect::back();
	}
	function checkout_details($cs_id)
	{


	      $data=DB::table('capacity as a')
	      ->select('a.*','c.*','c.cs_id','a.capacity_no','a.title','a.primary_skills','a.experience'
	      	,DB::raw('DATE(c.cs_short_date) as cs_short_date'),'a.typeofengae','c.cs_short_status',
	      	'c.cs_id','b.company_name','d.name as posted_user','e.company_name as checkout_company',
	      	'f.name as checkout_user',DB::raw('DATE(c.cs_app_checkout) as checkout_date'),'a.experience',
	      	'b.contact_name as posted_contactname','b.contact_number as posted_contact','b.email as posted_email','e.contact_name as checkout_contactname','e.contact_number as checkout_contact','e.email as checkout_email','e.company_name as checkout_company',
	      	'a.description')

	      ->join('company_profiles as b','a.cap_companyid','=','b.company_id')
	      ->join('users as d','d.id','=','a.posted_by')
	      ->join('capacity_shortlist as c','c.cs_capid','=','a.id')
	      ->join('company_profiles as e','e.company_id','=','c.cs_company_id')
	      ->join('users as f','f.id','=','c.cs_uid')
	      ->where('cs_id',$cs_id)->first();

	     return view('Pages.admin.capacity.checkout_details',compact('usertype','data'));
	}
}
